# Introdução à Redes

## A Internet

Milhões de dispositivos de computação conectados rodando aplicações de rede. Dispositivos denominados **roteadores** encaminham pacotes (pedaços de dados) através da internet.

## Protocolos

Definições de formato e sequência para a transmissão e recepção de mensagens por meio da rede.

## Borda da rede

Os dispositivos ligados à internet por meio dos quais é feito o contato com um meio exterior a esta.

## Enlace físico

Denominação dada ao meio de transmissão de dados entre transmissor e receptor quaisqueres.

**Meio guiado:** sinais se propagam por um meio sólido, como o cobre ou a fibra ótica.

**Meio não guiado:** sinais se propagam livremente, por exemplo, em sinais de rádio.

## Ethernet

Tecnologia de enlace físico amplamente difundida de conexão à internet via cabeamento.

## Núcleo da rede

O conjunto de roteadores interconectados que, juntos, transmitem dados entre dispositivos na borda da rede. A transferência de dados por meio destes pode se dar de duas formas:

- **comutação de circuitos:** o circuito é dedicado a uma única chamada, por exemplo, como ocorre na rede telefônica. Esta modalidade exige um processo prévio de reserva do circuito para a realização da chamada, à partir de então o desempenho do circuito é garantido. Não obstante, o uso intermitente ou o não uso do recurso encarreta no desperdício deste recurso.
  
  O uso compartilhado deste circuito pode ser feito com os diversos usuários se comunicando em frequências distintas ao mesmo tempo (FDM) ou, senão, em uma mesma frequência em intervalos intercalados e regulares (TDM).

- **comutação de pacotes:** o circuito é dedicado ao repasse intermitente de pacotes de dados de origens e destinações diversas. Quando isto é feito de forma desordenada, chama-se de **mutiplexação estatística**. Esta forma de uso da rede, que se dá por demanda, permite que um número maior de usuários faça uso dela ainda que o desempenho da comunicação possa variar em função do congestionamento de pacotes.

### Estrutura da rede

Roteadores podem ser classificados em função da abrangência de comunicações as quais abarcam:

- **ISP nível 1:** Nacional e internacional;

- **ISP nível 2:** Regional;

- **ISP nível 3:** Local.

### Atraso e perda de pacotes

Em um roteador, conforme pacotes recebidos se acumulam, estes são armazenados em *buffers*. O atraso no envio do pacote em cada nó $d_{nodal}$se dá por:

$$
d_{nodal} = d_{proc} + d_{fila} + d_{trans} + d_{prop}
$$

Sendo,

- $d_{proc}$: atraso de processamento, em função da checagem da integridade do pacote e a determinação do próximo roteador a enviá-lo;

- $d_{fila}$: atraso de enfileiramento;

- $d_{trans}$: atraso de transmissão, dado pela razão entre o tamanho do pacote (usualmente dado em bits) e a largura de banda do enlace (usualmente dada em bits por segundo);

- $d_{prop}$: atraso de propagação, dado pela razão entre a distância do enlace físico e a velocidade de propagação do sinal neste meio;

Não havendo *buffers* disponíveis no roteador a receber o pacote, ou se no processamento deste determina-se que o mesmo foi corrompido, este é descartado (perda).

## Largura de banda

Taxa de transmissão de dados **de um dado enlace**, usualmente dada em bits por segundo. Essa largura de banda pode ser dedicada, entre um par de nós, ou compartilhada, entre um conjunto de nós conectados à um mesmo nó. Neste último caso, o protocolo de conexão necessita gerenciar a conexão afim de evitar colisões ou congestionamento para o envio dos dados.

## Vazão

A taxa de transmissão de dados **entre dados emissor e receptor**, esta pode ser a:

- **instantânea:** em um intervalo de tempo que tende à zero;

- **média:** no intervalo de tempo do envio.

Esta é limitada pelo enlace com menor largura de banda (gargalo) no circuíto. Usualmente, este é um roteador de borda.

## Camadas de protocolo

Em redes, diversos serviços interdependentes se encontram presentes a organização destes cada qual em uma camada de protocolo modulariza a operacionalização da rede, fazendo com que a manutenção ou modificação de um deste serviços não venha a afetar a estrutura do restante do sistema. No modelo ISO/OSI para a internet, vemos a seguintes camadas de serviço:

```mermaid
classDiagram
class Camadas {
    Aplicação
    Transporte
    Rede
    Enlace
    Física
}
```

Sendo,

- **Aplicação:** suporte a aplicações de rede, aqui se encontram os protocolos FTP, SMTP e HTTP, por exemplo.

- **Transporte:** suporte à transferência de dados entre processos, aqui se encontram protocolos como o TCP e UDP.

- **Rede:** suporte a roteamento de datagramas da origem ao destino, aqui se encontra o protocolo IP, dentre outros.

- **Enlace:** suporte a transferência de dados entre computadores vizinhos na rede, aqui se encotnra o PPP, o Ethernet.

- **Física:** suporte a transferência de bits pela rede, por meio de cabos.

Outras camadas de serviço deste modelo que ainda não foram implementadas são:

- **Apresentação:** Permite que as aplicações interpretem os dados dada a presença de criptografia, ou compactação, ou convenções específicas a cada dispositivo. Atualmente estas finalidades são endereçadas por protocolos como HTTPS e SSL na camada de transporte.

- **Session:** Mantém a sincronização, verificação e recuperação da troca de dados. Atualmente estas finalidades são endereçadas pelos *Cookies*, na camada de aplicação.