# Camada de Transporte

Serviços e protocolos de transporte oferecem a **lógica de comunicação** que se dá entre **processos de aplicação** operando conjuntamente, ainda que potencialmente em hospedeiros diferentes. Estes são invocados pelos sistemas finais de uma dada comunicação.

<img src="Imagens/2022-09-29-10-27-51-image.png" title="" alt="" data-align="center">

No uso destes, o lado remetente divide a mensagem da aplicação em segmentos, passados ao destinatário através da camada de rede. O destinatário então recebe estes segmentos e remonta a mensagem, levando-a a sua camada de aplicação. Os principais protocolos utilizados na internet são o **TCP** e o **UDP**.

## Camada de transporte x rede

**Camada de rede:** a lógica de comunicação entre **hospedeiros**

**Camada de transporte:** a lógica de comunicação entre **processos** que, portanto, conta com e amplia os serviços da camada de rede.

### Analogia da "família de crianças"

Considere uma casa com 12 crianças mandando, cada qual, cartas para 12 crianças noutra casa. Determinou-se que duas crianças, Ana e Bill, iriam, respectivamente, enviar e buscar este conjunto de cartas em uma agência de correio. Nesta analogia,

- As **casas** seriam os **hospedeiros**;

- As **crianças** seriam os **processos**;

- As **cartas** seriam as **mensagens**;

- **Ana** e **Bill** seriam o **protocolo de transporte**;

- O **serviço postal** seria o **protocolo de rede**.

## Protocolos da camada de transporte

- **TCP** para a remessa confiável e em ordem, com controle de fluxo e congestionamento mediante o estabelecimento de uma conexão prévia ao envio das mensagens entre os processos.

- **UCP** para a remessa sem garantias de recebimento, pelo método do "melhor esforço".

## Multiplexação/demultiplexação

- **Demultiplexação (no destinatário):** processo pelo qual segmentos de mensagens recebidos são direcionados aos sockets corretos.

- **Multiplexação (no remetente):** processo pelo qual dados enviados por múltiplos sockets são designados cabeçalhos para o envio (que informarão a correta destinação destes no processo de demultiplexação).

<img src="Imagens/2022-09-29-11-14-34-image.png" title="" alt="" data-align="center">

### Como funciona a demultiplexação

O hospedeiro recebe os segmentos (datagramas), sendo que cada um destes tem um endereço IP de origem e um de destino, assim como uma porta de origem e destino. O hospedeiro então usa tais informações para direcionar o segmento ao socket correto.

<img src="Imagens/2022-09-29-11-19-09-image.png" title="" alt="" data-align="center">

### Criação de sockets para demultiplexação *não orientada* a conexão

O seguinte trecho de código Java cria um socket associado à porta `12534`:

```java
DatagramSocket mySocket1 = new DagramSocket(12534);
```

Um socket UDP necessita apenas de dois elementos para ser identificado: **IP** e **porta de destino**. Tais informações são suficientes para que um hospedeiro possa receber um segmento UDP e direcioná-lo ao socket adequado. 

### Demultiplexação *orientada* para conexão

Trata-se de um socket TCP, identificado por 4 elementos:

1. Endereço IP de origem;

2. Número de porta de origem;

3. endereço IP de destino;

4. Número de porta de destino.

Hospedeiros servidores podem admitir muitos sockets TCP simultâneos, de tal sorte que servidores Web têm diferentes sockets para cada cliente conectado e, no mais, conexões não persistentes terão diferentes sockets para *cada requisição*.

## UDP

> User Datagram Protocol [RFC 768]

O protocolo de transporte para a internet mais rudimentar disponível, sua operacionalização se dá por "melhor esforço": pacotes são enviados sem a garantia de que estes serão entregues ou, se chegarem, estarão ordenados (cada segmento UDP é tratado como se fosse independente dos demais). O UDP não necessita estabelecer a conexão entre remetente e destinatário antes de realizar o envio de pacotes. Dentre outras aplicações (como o DNS e o SNMP ‒ Simple Network Management Protocol), o UDP normalmente é usado para streaming de aplicações multimídia, pois estas são mais tolerantes a perdas e mais sensíveis à taxa de transmissão (que o UDP consegue iniciar mais rapidamente quando comparado ao TCP). É possível ser feito o tratamento de falhas de envio por UDP fazendo uso da sua soma de verificação (*checksum*), mas a implementação deste fica a cargo do processo à comunicar-se.

<img src="Imagens/2022-09-29-14-25-07-image.png" title="" alt="" data-align="center">

## Princípios da transferência confiável de dados

Para explicar o funcionamento de um protocolo para comunicação confiável o qual aqui denominaremos rdt (**r**eliable **d**ata **t**ransfer [protocol]) esboçaremos aqui versões cada vez mais complexas deste protocolo considerando um meio de transmissão cada vez mais desconfiável para a comunicação. Em todas as versões do protocolo, este irá dispor dos seguintes comandos:

- **rdt_send(data):** comando emitido pela aplicação para realizar o envio de um pacote *data*;

- **udt_send(data):** chamada feita pelo próprio rdt para transferir o pacote por um canal não confiável ao destinatário;

- **rdt_rcv(data):** chamada feita pelo próprio rdt quando este recebe um pacote;

- **deliver_data(data):** chamada do rdt para enviar à aplicação um pacote recém recebido.

Para ilustrar a implementação, faremos uso de um diagrama de estados, onde a transição de estados é representada na forma:

```
Evento detectado
----------------------------
Respsota ao evento detectado
```

### rdt 1.0

Consideremos primeiramente um meio de transmissão perfeitamente confiável. Nessa situação, bastará o seguinte diagrama de estado para o emissor e receptor:

![](Imagens/2022-09-29-16-19-14-image.png)

### rdt 2.0

Consideremos agora um canal no qual os bits que compõem uma mensagem possam ser "flipados", distorcendo, assim, o conteúdo da mensagem. Para lidarmos com esta situação, faremos uso dos seguintes recursos:

- **checksums** para detectar a presença de erros na mensagem;

- O receptor responderá ao emissor com uma **resposta de reconhecimento**, indicando se, a depender da verificação do checksum, a mensagem foi recebida sem erros ‒ Acknowledgment (ACK) ‒ ou com erros ‒ Negative Acknoledgement (NAK) ‒ necessitando, portanto, do reenvio da mensagem.

- O emissor e receptor passarão a poder assumir um novo estado onde estes aguardam, respectivamente, a resposta da verificação do checksum, e o eventual reenvio de pacotes. Comportamento este aqui denominado **stop and wait**.

Assim sendo, a representação da máquina de estados finitos fica:

| Emissor                                    |
|:------------------------------------------:|
| ![](Imagens/2022-09-29-16-33-28-image.png) |
| **Receptor**                               |

### rdt 2.1

Existe uma falha fatal com o modelo anteriormente apresentado: existe a possibilidade do bits da mensagem de reconhecimento serem alterados e, assim, um pedido de reenvio pode não ser respondido. Assim sendo, designamos um bit de sequência ao nosso pedido ACK ou NACK, de tal sorte que o hospedeiro aguardam uma resposta com um número de sequência correspondente, ou

- No caso do emissor, reenvia o pacote;

- No caso do receptor, descarta o pacote.

Ficamos, assim com o seguinte diagrama de estados:

| Emissor                                                                                                                                             |
|:---------------------------------------------------------------------------------------------------------------------------------------------------:|
| ![](Imagens/2022-09-29-17-20-22-image.png)                                                                                                          |
| **Receptor**                                                                                                                                        |
| ![](file:///home/user/Public/USP/Sistemas de Informação/4° semestre/Redes de Computadores/Imagens/2022-09-29-17-21-56-image.png?msec=1664482916796) |

### rdt 2.2

Esta versão possui as mesmas funcionalidades da versã o 2.1, mas não faz uso de NAKs, o destinatário envia penas ACKs para o último pacote recebido sem falhas. Nesta nova configuração o destinatário envia explicitamente o número de sequência do pacote a ser reconhecido com ACK, de tal forma que um ACK de mesmo número de sequência no remetente resulta na mesma ação de um NAK: retransmitir o pacote atual.

![](Imagens/2022-09-29-17-29-33-image.png)

### rdt 3.0

Nesta versão do protocolo consideramos também a possibilidade de datagramas serem perdidos no envio (seja no envio de dados, ou no envio de respostas de reconhecimento). Na eventualidade de uma resposta não ser obtida naquilo que consideramos ser um tempo de resposta razoável, os dados são novamente enviados. Pode ser o caso do envio estar meramente atrasado, mas graças ao número de sequência, podemos descartar dados duplicados em função de um novo envio. O novo diagrama de estados fica:

| Emissor                                    |
|:------------------------------------------:|
| ![](Imagens/2022-09-29-17-45-43-image.png) |

#### O protocolo rdt 3.0 em ação

| Comunicação sem perda                      | Comunicação com perda do pacote                      |
|:------------------------------------------:|:----------------------------------------------------:|
| ![](Imagens/2022-09-29-18-23-30-image.png) | ![](Imagens/2022-09-29-18-23-45-image.png)           |
| **Comunicação com perda do ACK**           | **Comunicação com timeout prematuro / ACK atrasado** |
| ![](Imagens/2022-09-29-18-26-49-image.png) | ![](Imagens/2022-09-29-18-28-37-image.png)           |