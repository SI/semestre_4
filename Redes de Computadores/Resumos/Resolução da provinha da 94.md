# Resolução da provinha da 94

**1.** O hospedeiro X manda um pacote de $500 B$ para o hospedeiro Y através da internet. Entre eles há 3 enlaces a $4km$ cada e 2 roteadores. Em todo o trajeto, a velocidade de propagação do sinal é $2 \cdot 10^8m/s$ e a taxa de transmissão é $2,5MB/s$. O tempo de processamento em cada roteador é de $20 \mu s$ e não á congestionamento ou enfileiramento. Calcule o atraso total entre X e Y.

$$
Atraso = 3\ d_{nodal} =
3(d_{proc} + d_{fila} + d_{trans} + d_{prop}) =\\\ \\
3 \left(20 \cdot 10^{-6} +  0 + \frac{500}{2,5 \cdot 10^6} +
\frac{4 \cdot 10^3}{2 \cdot 10^8}\right)s = \\\ \\
3 \left(\frac 15 \cdot 10^{-4} + \frac 15 \cdot 10^{-3}
+ \frac 15 \cdot 10^{-4} \right)s =
\frac 35\left(10^{-3} + 2 \cdot 10^{-4}\right)s = 720 \mu s
$$

**2.** Cite e explique três garantias oferecidas pelo TCP.

O TCP controla para que no envio de arquivos byte-a-byte ocorra

- identificação da ordem de envio dos bytes;

- correção de erros no envio, retransmitindo dados quando necessário;

- controle da taxa de envio para não sobrecarregar a rede;

- teste-se a viabilidade da conexão antes que qualquer dado seja enviado;

**3.** Cite uma vantagem do uso da arquitetura cliente-servidor e uma vantagem de peer-to-peer.

***Peer-to-peer*** (do inglês par-a-par ou simplesmente ponto a ponto) ou **P2P** é uma arquitetura de redes de computadores onde cada um dos pontos ou nós da rede funciona tanto como cliente quanto como servidor, permitindo compartilhamentos de serviços e dados sem a necessidade de um servidor central. Por um lado, uma rede peer-to-peer é mais conveniente para o armazenamento de objetos imutáveis, mas seu uso em objetos mutáveis é mais desafiador pois, diferentemente da arquitetura cliente-servidor, não existe uma autoridade central que controle a versão ou a autenticidade dos dados distribuídos na rede. Não obstante isso possa ser resolvido com subterfúgios tais quais o *blockchain*.