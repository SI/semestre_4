# Camada de aplicação

Termo utilizado em redes de computadores para designar uma camada de abstração que engloba protocolos que realizam a comunicação fim-a-fim entre aplicações.

É a camada mais próxima do usuário, na qual é a encarregada quando o cliente acessa o e-mail, páginas web, mensageria, login remoto, videos, videoconferências, entre outros. A arquitetura de aplicação permite que o usuário acesse essas funções.

## Tipos de arquitetura de aplicação

### Cliente-servidor

O **modelo cliente-servidor** (em inglês *client/server model*), em computação, é uma estrutura de aplicação distribuída que distribui as tarefas e cargas de trabalho entre os fornecedores de um recurso ou serviço, designados como servidores, e os requerentes dos serviços, designados como clientes.

<img src="http://127.0.0.1:42009/b990b27c-37ef-87b3-4fd8-55a09ee916be/I/Cliente-Servidor.png.webp" title="" alt="" data-align="center">

Geralmente os clientes e servidores comunicam através de uma rede de computadores em computadores distintos, mas tanto o cliente quanto o servidor podem residir no mesmo computador.

### Peer-to-peer (P2P)

***Peer-to-peer*** (do inglês par-a-par ou simplesmente ponto a ponto) ou **P2P** é uma arquitetura de redes de computadores onde cada um dos pontos ou nós da rede funciona tanto como cliente quanto como servidor, permitindo compartilhamentos de serviços e dados sem a necessidade de um servidor central. Uma rede peer-to-peer é mais conveniente para o armazenamento de objetos imutáveis, seu uso em objetos mutáveis é mais desafiador, e pode ser resolvido com a utilização de servidores confiáveis para gerenciar uma sequência de versões e identificar a versão corrente, pode ser usada para compartilhar músicas, vídeos, imagens, dados, enfim qualquer coisa com formato digital. Um exemplo de transmissão de dados via peer-to-peer são os torrents.

### Híbrida

As redes aquelas em que mesclam as duas arquiteturas para utilização em diferentes aspectos da mesma.

## Processos

Programas rodando em um dado computador hospedeiro. Havendo comunicação deste processo com outros computadores, este pode, em momentos, ser designado um **processo cliente** ‒ quando inicia a comunicação ‒ou um processo servidor ‒ quando aguarda uma comunicação.

### Sockets

Estrutura de software presente em cada nó de uma rede de computadores (este próprio sendo um computador) que permite a comunicação pela rede dos processos presentes em cada computador ao manejar o envio e recebimento de dados de acordo com determinados **protocolos**. A escolha de protocolos e a capacidade de fixar determinados parâmetros destes é denominada a **API** (application programming interface) do socket.

## Endereçando processos

Para receber mensagens, um processo necessita de um identificador, usualmente composto por um endereço de IP em conjunção com um número de porta. São exemplos de números de porta:

- 80: servidor HTTP;

- 25: servidor de e-mail.

## Definições de protocolo

Um protocolo de aplicação necessita descrever:

- tipos de mensagens aptas a serem trocadas: como requisição ou resposta;

- a sintaxe da mensagem: como a mensagem é organizada em campos distintos;

- a semântica da mensagem: qual o significado da informação em cada campo;

- regras para o envio e recebimento de mensagens.

Os protocolos existentes são diversos e podem se encontrar no domínio público (como aqueles definidos em RFCs, o HTTP, o SMTP, entre outros) ou serem proprietários (como aquele do Skype, adquirido pela Microsoft).

### Fatores de escolha de um protocolo

#### Perda de dados

A ocorrência de perda de dados em uma comunicação pode ser desprezada para algumas aplicações, como a reprodução de áudio ou vídeo em tempo real. Noutras, como a transferência de arquivo, necessita o reenvio das informações perdidas.

#### Temporalização

Aplicações como telefonia via internet e jogos online necessitam que a comunicação se dê com o menor atraso possível, tomando prioridade sobre outras comunicações (como o envio de arquivos, que já é uma comunicação mais lenta).

#### Vazão

Da largura de banda disponível, quanto desta fazer uso? No caso da reprodução de vídeos em tempo real, o máximo de largura de banda disponível pode ser utilizada para reproduzí-lo com a maior qualidade possível, enquanto para o envio de mensagens, apenas uma quantidade mínima precisa ser utilizada.

#### Segurança

Certas aplicações necessitam checar a integridade das mensagens recebidas ou criptografá-las para assegurar a privacidade dos usuários.

## Protocolos de serviço de internet

### *Transmission Control Protocol (TCP)*

Protocolo para envio de arquivos byte-a-byte, controlando para

- ordem de envio dos bytes;

- correção de erros no envio, retransmitindo dados quando necessário;

- controle da taxa de envio para não sobrecarregar a rede;

- viabilidade da conexão antes que qualquer dado seja enviado;

Por razão destas características, se pode afirmar que o TCP prioriza confiabilidade no envio das informações sobre a velocidade ou latência do envio destas.

### User Datagram Protocol (UDP)

Protocolo para envio de informações, neste caso referidas enquanto *datagrams*, sem as verificações feitas pelo protocolo TCP. Desta forma, UDP é adequada a comunicações onde existe uma maior necessidade de velocidade sobre a confiabilidade das informações (por exemplo, no envio de vídeos ou audio em tempo real).

### Web e HTTP

Uma página **Web** consiste em uma coleção de objetos referenciados por um arquivo HTML, sendo que cada arquivo também pode ser acessado por um endereço URL. Por exemplo, a URL www.someschool.edu/someDeot/pic.gif, idne www.someschool.edu é o endereço do hospedeiro e someDept/pic.gif

O HTTP, ou HyperText Transfer Protocol é o protocolo da camada de aplicação da Web de modelo cliente/servidor pelo qual o envio dos objetos da página Web são requisitados. Este faz uso da conexão via TCP e não armazena o "estado" das páginas visitadas pelos clientes (função essa relegada aos *cookies*).

Uma conexão HTTP pode ser persistente ou não, quando esta o é, o servidor dedica um socket a aguardar por requisições de um dado cliente, sem encerrar a conexão após responder à uma requisição. Em geral, o padrão é ter a conexão encerrada após a resposta de cada requisição, para que o recurso do socket não seja desperdiçado em conexões inativas.

#### HTTP não persistente: tempo de resposta

Seja **RTT** o tempo para um pequeno pacote (de tamanho desprezível se comparado a largura de banda) trafegar do cliente ao servidor e retornar. Tempos que o tempo de resposta de uma conexão HTTP não persistente é a somatória de:

- 1 RTT para iniciar a conexão TCP;

- 1 RTT para a requisição HTTP e os primeiros bytes da resposta HTTP retornarem;

- o tempo de transmissão do arquivo.

total = 2 RTT + tempo de transmissão

#### HTTP persistente

Uma única conexão TCP é feita, o que descarta o *overhead* da criação da conexão para cada requisição. O tempo de resposta total fica 1 RTT + tempo de transmissão.

#### Mensagens HTTP

Existem dois tipos de mensagens HTTP: **requisição** e **resposta**. Um exemplo de mensagem de requisição seria o seguinte: 

```http
HTTP/1.1 302 Found
X-Powered-By: Express
Location: https://tinywars.online
Vary: Accept
Content-Type: text/plain; charset=utf-8
Content-Length: 45
Date: Thu, 15 Sep 2022 18:11:05 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```

A primeira linha é a linha de requisição, as seguintes compõem o cabeçalho da requisição e a última linha possui apenas uma quebra de linha indicando o término da mensagem.

## Métodos do HTTP

### POST

Por meio deste entradas dos clientes são enviadas ao servidor do corpo da requisição HTTP.

### GET

As entradas do cliente são enviadas na URL da requisição, por exemplo: www.umsite.com/buscaanimal?macacos&banana, onde o texto após "?" compõe as entradas do usuário separadas entre si por "&".

### HEAD

O cliente solicita o envio do cabeçalho da requisição, mas não do conteúdo associado a este.

### PUT

Envia arquivos no corpo da requisição para o endereço no servidor especificado pela URL. Este método esta disponível apenas para a versão 1.1, ou versões mais atuais, do HTTP.

### DELETE

Exclui o arquivo especificado no endereço da URL. Este método esta disponível apenas para a versão 1.1, ou versões mais atuais, do HTTP.

## Exemplos de códigos de estado da resposta HTTP

Códigos estes os quais são descritos na primeira linha do cabeçalho.

- **200 OK:** requisição bem-sucedida, objeto requisitado mais adiante

- **301 Moved Permanently:** objeto requisitado movido, com novo local especificado mais adiante. Usualmente um navegador de internet imediatamente redireciona o acesso ao endereço descrito em sequência.

- **400 Bad request:** a mensagem de requisição não foi entendida pelo servidor

- **404 Not Found:** O documento requisitado não foi encontrado.

- **505 HTTP Version Not Supported**

## Caches Web (servidores proxy)

Um servidor intermediário ao cliente e o servidor de destino que armazena o conteúdo das páginas Web recentemente visitadas. Desta forma, havendo novas requisições, por um mesmo conteúdo, a requisição HTTP não necessita viajar até um servidor mais distante que aquele do cache, ou mesmo congestioná-lo sendo este bastante acessado por diversos clientes. Tais servidores são usualmente mantidos por empresas, especialmente pelas ISPs.

## DNS

O *Domain Name System* trata-se de um banco de dados distribuído em diversos servidores (denominados *serviços de DNS*) o qual associa endereços de IP à endereços tais como www.yahoo.com. A distribuição deste banco de dados o confere uma série de benefícios:

- Não possui um ponto único de falha;

- Distribui o volume de tráfego entre um número maior de servidores;

- Distribui os servidores geograficamente, reduzindo a distância entre estes e os clientes.

- Permite que a manutenção dos servidores se dê periodicamente sem afetar a disponibilidade do serviço.

### Distribuição hierárquica dos servidores DNS

<img src="Imagens/2494cb6190ff4d4561a80287b51067fd48f1551d.webp" title="" alt="" data-align="center">

Suponha que o cliente busca o IP referente ao domínio www.amazon.com, tem-se:

- O cliente consulta o servidor raiz para encontrar o servidor DNS referente aos domínios `.com`;

- O cliente então consulta o este servidor para obter o servidor DNS referente aos domínios `amazon.com`;

- O cliente então consumta este servidor e obtêm o endereço de IP para www.amazon.com.

### Top Level Domains

Denomina-se servidores de alto nível (Top-Level Domain ‒ TLD) os servidores queles responsáveis por gerenciar os endereços de terminações como `.com`, `.org`, `.net`, etc. ou as terminações associadas à países como `.br`, `.uk`, `.fr`, etc.

### Servidores com autoridade

Denominação dada aos servidores aqueles que respondem uma solicitação de resolução de DNS com um IP, ao invés de apontar o endereço doutro servidor o qual possa (ou saiba qual possa) realizar a resolução do DNS.

### Servidores de nomes local

Os servidores aqueles que não pertencem, estritamente, a hierarquia organizacional dos servidores DNS distribuídos. Cada ISP possui servidores deste tipo, e por padrão as consultas de seus clientes são direcionadas a estes, que atuam enquanto caches. O uso destes simplifica o processo de consulta, já que estes armazenam endereços dos domínios mais relevantes ao seu conjunto de clientes, dada a utilização destes.

### Formas de resolução de DNS

À título de exemplo venhamos a assumir que um cliente no endereço cis.poly.edu busca o endereço IP relativo ao endereço gaia.cs.umass.edu

#### Consulta iterativa

<img src="Imagens/2022-09-22-13-58-16-image.png" title="" alt="" data-align="center">

- O cliente solicita ao servidor DNS local, dns.poly.edu (1), a resolução do endereço passado, este servidor de DNS passa a ser o *DNS iterator* desta consulta.

- O DNS iterator pergunta ao servidor raiz o servidor TLD para o domínio `.edu` (2) e este o responde (3);

- O DNS iterator então pergunta ao servidor TLD pelo servidor com autoridade para o domínio `.umass.edu` (4) e este o responde (5);

- O DNS iterator então pergunta ao servidor com autoridade pelo endereço IP de `gaia.cs.umass.edu` (6) e este o responde (7);

- O DNS iterator repassa o endereço IP de volta ao cliente que o solicitou (8).

#### Consulta recursiva

Nota-se que no modelo anterior recai sobre o servidor DNS mais imediato a consultar a todos os demais servidores DNS necessários a realização da resolução do domínio. No modelo recursivo é possível distribuir esta tarefa entre os demais servidores participantes.

<img src="Imagens/2022-09-22-14-09-50-image.png" title="" alt="" data-align="center">

- O cliente solicita ao servidor DNS local, dns.poly.edu (1), a resolução do endereço passado, este então repassa  a solicitação ao servidor raiz (2);

- O servidor raiz repassa a solicitação ao servidor TLD para `.edu` (3);

- O servidor TLD repassa a solicitação para o servidor com autoridade para `umass.edu` (4);

- O servidor com autoridade resolve o domínio e repassa o IP deste para o TLD (5);

- Que o repassa ao servidor raiz (6);

- Que o repassa ao servidor local (7);

- Que o repassa ao cliente (8);

Embora  seja possível uma requisição DNS neste modelo, isso geralmente não é permitido, pois faz recair sobre servidores centrais a necessidade de passar e repassar entre si requisições de DNS e endereços de IP. É preferível que esta carga seja delegada aos servidores locais, que são mais numerosos.

### Caching e atualização de registros

- Quando qualquer servidor DNS descobre um mapeamento, ele o mantém em *cache* por um tempo determinado. Servidores TLD normalmente são mantidos em caches nos servidores de nomes locais para que, desta forma, servidores raiz não necessitem ser consultados com frequência.

### Registros de DNS

São armazenados em registros de recursos (RR), de formato:

$$
(nome,\ valor,\ tipo,\ ttl)
$$

$ttl$, ou "time to live", descreve o tempo pelo qual o registro fica armazenado no servidor DNS. No mais, o registro pode ser de um dentre diversos tipos, por exemplo,

- **Tipo A**:
  
  - **nome** descreve um hostname;
  
  - **valor** descreve um endereço de IP.

- **Tipo NS (Name Server):**
  
  - **nome** descreve um domínio (p. e. foo.com);
  
  - **valor** descreve o hostname do servidor de nomes com autoridade para este domínio.

- **Tipo CNAME (Canonical Name Record):**
  
  - **nome** é p apelido para algum nome "canônico" (real), por exemplo, "www.ibm.com" pode ser um apelido para o mais complexo nome canônico "servereast.backup2.ibm.com";
  
  - **valor** é o nome canônico.

- **Tipo MX (Mail exchanger):** 
  
  - **valor** nome de um servidor de coreio;
  
  - **nome** endereço ao qual este servidor de correio encontra-se associado.

### Formato de mensagem

> Para a consulta ou resposta de uma requisição de DNS.

#### Cabeçalho

| Identificação                | Flags                     |
|:----------------------------:|:-------------------------:|
| Número de perguntas          | Número de RRs de resposta |
| Número de RRs com autoridade | Número de RRs adicionais  |

Flags podem descrever:

- Se esta se trata de uma mensagem de requisição ou resposta;

- A recursão desejada;

- A recursão disponível;

- Se a resposta é com autoridade.

#### Corpo

| Perguntas ‒ campos de nome e tipo para consulta                                               |
| --------------------------------------------------------------------------------------------- |
| Respostas ‒ RRs na resposta à consulta                                                        |
| Autoridade ‒ registros para servidores com autoridade                                         |
| Informação adicional ‒ espaço para se encaminhar quisquer outras informações avaliadas úteis. |

## FTP

Protocolo que segue o modelo cliente/servidor para a transferência de arquivos de/para um hospedeiro remoto.

![](Imagens/2022-09-28-20-50-58-image.png)

### Funcionamento

O FTP faz uso de conexões separadas para controle e transmissão de dados, ambas fazem uso do TCP como protocolo de transporte.

<img src="Imagens/2022-09-29-09-31-15-image.png" title="" alt="" data-align="center">

O cliente contacta o servidor FTP, usualmente através da porta 21. Ao ser autorizada sua conexão de controle, este navega pelo mapa do diretório comunicado pelo servidor. Quando o servidor então recebe um comando de transferência de arquivo, este abre uma segunda conexão com o cliente para realizar a transferência. Após ter transferido o arquivo, o servidor fecha esta conexão para transferência de dados e atualiza o estado do diretório de acordo.

### Exemplos de comandos FTP

Estes são enviados como texto ASCII pelo canal de controle

- **USER:** nome de usuário;

- **PASS:** senha;

- **LIST:** Lista o conteúdo do diretório atual;

- **RETR \<nome-do-arquivo>:** recupera uma cópia do arquivo do servidor ao cliente;

- **STOR \<nome-do-arquivo>:** armazena uma cópia do arquivo do cliente ao servidor.

### Exemplos de códigos de retorno

São compostos de códigos e frases de estado, tal como com o HTTP.

- **331 Username OK, password required**

- **125 data connection already open; transfer starting**

- **425 Can't open data connection**

- **452 Error writing file**

## SMTP

Trata-se de um protocolo para servidor de correio, isto é, para o envio e recebimento de mensagens entre servidores do tipo os quais clientes podem acessar para lê-las. O acesso, por vez, é feito por meio de um dos seguintes protocolos:

- **POP (Post Office Protocol)**

- **IMAP (Internet Mail Acess Protocol)**

- **HTTP:** por meio de clientes de email online como Gmail, Hotmail, Yahoo! Mail, etc.

Tanto o envio de mensagens quanto o acesso ao servidor é feito via TCP.