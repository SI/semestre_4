# DHT

> Distributed Hash Table, ou Tabela de Espalhamento Distribuida

Considere um sistema contendo oito nós, o qual pode aqui ser considerado um sistema de servidores web, a título de exemplo.

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-20-53-59-image.png)

Se este sistema não for centralizado significa que não haverá um nó o qual esteja conectado à todos os demais. Por outro lado, se este o for, este se sua representação esquemática será algo parecido com a imagem seguinte:

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-20-57-50-image.png)

Isso seria uma representação adequada a representar um serviço online como a rede social *Facebook*, onde há um conjunto de servidores centrais comunicando entre si e intermediando a comunicação de quaisquer clientes acessando o serviço. Com um DHT, não há a necessidade um servidor, ou servidores, centrais. Em uma rede orientada por DHT, cada nó está ligado apenas a seus vizinhos imediatos, assim assegurando a descentralidade da rede.

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-21-01-19-image.png)

Não obstante, nessa configuração, como informações armazenadas em algum nó desta rede pode ser recuperada por qualquer outro nó que a integra?

Em cada nó encontra-se uma *tabela hash* e, assim que um pedido de consulta chega ao nó adequado, para encontrar a informação correspondente basta buscar sua chave correspondente nesta tabela. Mas como que o pedido de consulta alcança o nó adequado sem que haja um servidor central para consulta, e com pedidos podendo ser feitos à partir de qualquer nó?

Em uma DHT, cada nó contém uma tabela hash que abarca valores de chave menores que o valor da chave do nó e maiores ou iguais ao valor da chave da chave do nó adjacente de menor valor. Isso é, senão pelo servidor 1, que contém todas as chaves menores que um e todas as chaves iguais ou maiores que 8.

Imagine que ocorra um pedido de consulta no nó **6**, buscando a informação associada a chave **4.2**.

![](Imagens/2022-09-11-21-17-42-image.png)

O sistema reconhece que a chave não se encontra no escopo da *hash table* do nó atual pois está abaixo do escopo, então uma comunicação é feita com o nó de chave 5, cuja chave 4.2 encontra-se no escopo de sua *hash table*. A pesquisa então procede na tabela e, se nesta houver a chave buscada, a informação é repassada de volta ao nó que fez a solicitação, recursivamente. Senão, o que é repassado é um sinal de que a informação buscada não existe na rede.

Por fim, inserir ou remover nós da rede implica que o escopo das *hash tables* destes é distribuído entre nós adjacentes. Por exemplo, fosse o nó 3 removido da rede, passaríamos a ter a seguinte configuração:

![](Imagens/2022-09-11-22-05-54-image.png)

Fosse um servidor de chave 3 novamente inserido, a configuração voltaria àquela observada anteriormente.
