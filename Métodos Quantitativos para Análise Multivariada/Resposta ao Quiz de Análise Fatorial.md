# Resposta ao Quiz de Análise Fatorial

### 1. Qual é o principal objetivo da análise fatorial?

Criar a partir de um conjunto de variáveis originais que apresentam comonalidades entre si um novo conjunto de variáveis que representem especificamente tais comonalidades para que, de tal forma, **o fenômeno sendo observado possa ser representado com um número menor de variáveis sem perda significativa de informação**.

### 2. Quais são as suposições na análise fatorial?

As suposições envolvidas na análise fatorial podem ser distinguidas como sendo de uma de duas naturezas: **conceitual** ou **estatística**.

**Suposições conceituais**

- **Existe uma estrutura subjacente ao conjunto de variáveis escolhidas**, que explica as correlações observadas entre estas. Essa é uma suposição básica da Análise Fatorial;
- Com relação ao fenômeno sendo estudado, **as variáveis que integram a cada fator são homogêneas entre si**.

**Suposições estatísticas**

- As variáveis sobre as quais a análise fatorial é aplicada são **métricas ou, quando não, dicotômicas**, dado o emprego de métodos especializados para tratar com estas últimas;
- As variáveis são suficientemente correlacionadas umas com as outras para produzir **fatores representativos**.

### 3. Quais são os dois tipos de análise fatorial, e qual é a diferença entre eles?

A análise fatorial pode ser de tipo **exploratória** ou, senão, **confirmatória** à depender dos objetivos pretendidos. Naquelas do primeiro tipo, busca-se identificar a estrutura subjacente às variáveis à partir da combinação destas, sem que hajam restrições estabelecidas *a priori.* Enquanto naquelas do segundo tipo busca-se testar hipóteses sobre um número de fatores pré-estabelecido, seja quando ao número de variáveis que estes agregam, seja se estes se encontram em número adequado.

### 4. Quais são os dois métodos de extração fatorial, e qual é a diferença entre eles?

Estes são a **Análise de Componentes Principais (ACP)** e a **Análise de Fatores Comuns (AFC)**, os quais diferem-se entre si por seus objetivos: o primeiro objetiva resumir a maior parte da variância original a um número mínimo de fatores para fins de previsão. Em contraste, o segundo é usado prioritariamente para identificar fatores ou dimensões latentes que refletem o que as variáveis possuem em comum. Esta distinção se dá em função da maneira pela qual cada método aborda a variância explicada *versus* não-explicada:

- A ACP a variância total e deriva fatores que contêm **pequenas proporções de variância única ou de erro**;
- A AFC despreza as variâncias única e de erro assumindo que estas não são de interesse para a definição da estrutura das variáveis.

### 5. A que tipos de variáveis esta análise é mais aplicável?

A ACP é adequada quando (1) busca-se explicar a proporção máxima de variância total com o menor conjunto de fatores ‒ portanto, tem-se que a redução de dados é uma preocupação prioritária ‒ e (2) é sabido de antemão que a variância específica e de erro representam uma proporção relativamente pequena da variância total ‒ portanto tem-se a expectativa de que o método terá os subsídios necessários para ser efetivo na minimização do número de variáveis ‒. Em contrapartida, a AFC é apropriada quando (1)  tem-se como objetivo identificar fatores ou dimensões latentes que refletem o que as variáveis possuem em comum e (2) tem-se pouco conhecimento sobre a quantia de variância específica e de erro e, portanto, busca-se eliminar essa variância.

### 6.  Liste as etapas (ou estágios) que o texto sugere para que se planeje a aplicação desta técnica.

**Estágio 1:** Decide-se se a análise é **exploratória** (objetiva-se o resumo dos dados e identificação de estruturas) ou **confirmatória** (objetiva-se apenas a redução dos dados). O segundo tipo de análise não é abordado no presente capítulo do texto, então prosseguiremos com a descrição das etapas referentes às análises do primeiro tipo.

**Estágio 2:** Define-se o tipo de análise fatorial a ser utilizado a depender se o que está sendo agrupado são **variáveis** (análise fatorial do tipo R) ou **casos** (do tipo Q), faz-se um delineamento da pesquisa em função das variáveis incluídas, formas de medição e o tamanho da amostra desejado.

**Estágio 3:** São explicitadas as suposições acerca da amostra, tais quais sua normalidade, linearidade, homoscedasticidade, e relações conceituais; ou a ausência de qualquer uma destas.

**Estágio 4:** Seleção do método fatorial à depender de termos a variação total analisada ‒utiliza-se a a análise de componentes principais ‒ ou apenas a variância comum ‒ utiliza-se a análise de fatores comuns. Qualquer seja o método empregado, este produzira fatores dos quais deve-se decidir a quantidade destes que será mantida.

**Estágio 5:** Seleção de um método rotacional a ser empregado, de maneira a maximizar a representatividade dos fatores. À saber: rotação ortogonal se compreendido não haver correlação entre os fatores obtidos da amostra ou, senão, rotação obliqua. A depender do resultado, fatores menos representativos podem ser descartados de forma a simplificar o modelo.

**Estágio 6:** Validação da análise fatorial. O que pode ser feito adotando os métodos da análise fatorial confirmatória, ou aplicando-se o modelo enquanto preditivo do resultado de uma nova amostra.

### 7. Como o tamanho da amostra influencia na análise fatorial?

Quanto maior for o tamanho da amostra, maior será a generalidade do modelo resultante da aplicação do método da análise fatorial sobre esta. Também, melhor é o desempenho de medidas de intercorrelação, como o *teste de esfericidade de Barlett* ou a *medida de adequação da amostra (MSA)*, obtidas sobre a mesma. Tais medidas dão validação a existência de correlações suficientes na matriz de dados para justificar a aplicação da análise fatorial.

### 8. Qual é o meio de interpretar o papel que cada variável tem na definição de cada fator?

Fatores são gerados de forma a explicarem, ao máximo possível, a variância observada entre as variáveis. O índice que correlaciona o quanto isso é feito para cada variável original com relação cada fator é dado pelo índice da **carga fatorial**. Este valor elevado ao quadrado indica o quanto da variância de uma dada variável original é explicada por um dado fator.

### 9. O que é rotação fatorial e para que serve?

Rotação é o meio pelo qual os eixos relativos a cada fator em um gráfico de dispersão são ajustados de forma a maximizar a variância explicada pelos fatores principais (os da primeira e segunda componente, e assim por diante). Essa pode ser feita de maneira ortogonal ou oblíqua (cada eixo independentemente dos demais), a depender dos fatores não possuírem correlação ente si ou *poderem possuir*, respectivamente.

### 10. Comente sobre o potencial do uso desta técnica no seu banco de dados: ela ajudaria em que?

Nosso dataset consitui-se numa lista de imóveis disponíveis para locação em cinco cidades da Índia. A fim de se predizer o valor do aluguel a análise de fatores pode ser utilizada para se identificar dentre as variáveis métricas aquelas que apresentam maior correlação com a variável "aluguel", e com qual representatividade. Por exemplo, separada a cidade de Dheli somente e rotacionados os eixos dos fatores obliquamente, obtemos o seguinte gráfico:

![](Imagens/598385ac6e1503999cb39cbe5c3eaa414674998b.png)

Vê-se que para esta amostra, os eixos aluguel e andar encontram-se orientados quase que ortogonalmente, ou seja, a correlação é muito baixa. E embora a correlação entre aluguel e área seja grande, ela é significativamente maior com relação ao número de dormitórios e número de banheiros, de tal forma que essa métrica poderia ser desprezada. Tal experimento poderia ser replicado para demais cidades, para então avaliar-se seu potencial preditivo.