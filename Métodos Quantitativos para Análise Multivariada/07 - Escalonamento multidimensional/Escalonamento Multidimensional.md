# Relatório

> Aplicação da técnica de Escalonamento Multidimensional

| Aluno                      | nUSP     |
| -------------------------- | -------- |
| Cesar Billalta Yamasato    | 12542992 |
| Guilherme de Abreu Barreto | 12543033 |
| Lucas Panta de Moura       | 12608650 |

## Resumo

O presente relatório descreve a aplicação da técnica de Escalonamento Multidimensional (*Multidimensional Scaling* ‒ MDS) para fins exploratórios de um *dataset*. Buscamos identificar a ocorrência de dissimilaridades entre os imóveis de tal forma que fosse possível, por seus atributos, identificar a cidade em que cada um destes encontra-se localizado. Compara-se os resultados obtidos com análise prévia de componentes principais, a fim de se avaliar o potencial explicativo que tais técnicas oferecem para o presente objeto de estudo.

## Descrição do banco de dados e seu tratamento

Fazemos uso do banco de dados House Rent Prediction Dataset[^1], o qual possui dados extraídos do site Magic Bricks ([https://www.magicbricks.com/](https://www.magicbricks.com/)). O site permite a seus usuários anunciar seus imóveis na plataforma, ao que parece, sem exercer muito controle sobre a forma como isto é feito. Temos imóveis listados com uma área inferior à 1 m², ou com 10 metros quadrados e 4 banheiros, o que cremos serem erros. De forma a não ter estes domicílios afetarem o modelo na capacidade deste em descrever imóveis de características mais comuns, após ter traduzido o dataset procedemos à retirar deste outliers em seus parâmetros quantitativos. Utilizamos como parâmetro de corte a amplitude interquartil (IQR ‒ InterQuartile Range): se, em qualquer parâmetro, um domicílio apresenta um valor Val para um atributo X tal que:

$$
\text{Exclui(\it X)}\ se\ Val(X) < Val(Q_1) - \frac 32\ IQR\ ou\  Val(X) > Val(Q_3)
+ \frac 32\ IQR
$$

No mais, a nosso próprio critério, removemos os imóveis aqueles descritos com 6 m² ou menos de área. Com isso restam-nos 3.992 do total de 4.498 descritos pelo banco de dados.

Ainda, como estes 3992 imóveis estão distribuídos em cinco cidades distintas, ocorre que o parâmetro “bairro” em geral não chega a acomodar mais de dois imóveis a cada tempo e, portanto, não contribui para apontar correlações significativas entre os imóveis. Assim sendo, este fator foi desprezado em nossa análise. Ao sumarizar-se o resultado desta operação, obtemos o seguinte *dataset*:

```r
 > summary(sample)
       X        anunciado.em         num..dorm. 
 Min.   :   1   Length:3992        Min.   :1.000
 1st Qu.:1414   Class :character   1st Qu.:1.000
 Median :2534   Mode  :character   Median :2.000
 Mean   :2474                      Mean   :1.901
 3rd Qu.:3649                      3rd Qu.:2.000
 Max.   :4746                      Max.   :3.000

    num..banheiros       área       tipo.de.área
 Min.   :1.000   Min.   :  7.0   Length:3992
 1st Qu.:1.000   1st Qu.:118.0   Class :character
 Median :2.000   Median :217.0   Mode  :character
 Mean   :1.746   Mean   :220.5
 3rd Qu.:2.000   3rd Qu.:311.0
 Max.   :3.000   Max.   :472.0

    andar        quantidade.de.andares
 Min.   : 0.000   Min.   : 0.000
 1st Qu.: 1.000   1st Qu.: 2.000
 Median : 2.000   Median : 3.000
 Mean   : 2.432   Mean   : 5.067       
 3rd Qu.: 3.000   3rd Qu.: 5.000       
 Max.   :34.000   Max.   :48.000 

    andar            cidade    
 Length:3992      Bangalore:824
 Class :character Chennai  :824
 Mode  :character Delhi    :519
                  Hyderabad:785
                  Mumbai   :538

    mobília                        tipo.de.inquilino
 Inteiramente mobiliado: 467    Famílias       : 355
 Não mobiliado         :1657    Sem preferência:2962
 Parcialmente mobiliado:1868    Solteiros      : 675
```

```r
    ponto.de.contato    aluguel   
 Corretor    : 926    Min.   :0.150
 Proprietário:3066    1st Qu.:0.900
                      Median :1.400
                      Mean   :1.777
                      3rd Qu.:2.200
                      Max.   :5.700
```

Sendo:

- $X$ a variável que identifica numérica e sequencialmente cada observação.

- O valor do aluguel descrito em dezenas de milhares de rupias indianas (₹).

- 0 para o valor de andar descreve um andar térreo.

Por fim, para a realização desta análise dispomos do software R Studio, mais as bibliotecas de funções `pca3d`, `psych`, `factoextra` e `biotools`.

## Escalonamento Multidimensional

A presente técnica a qual utilizamos para visualizar a dissimilaridade (ou distâncias) entre os objetos, trata-se de um método de mapeamento pelo qual tais dissimilaridades podem ser representadas em um espaço cartesiano. Com isso, torna-se possível identificar a formação de grupos de objetos similares entre si, quando estes houverem, pela proximidade destes. 

### Análise dos objetos avaliados

Avaliamos os imóveis em termos de variáveis métricas, a saber: (1.) número de dormitórios, (2.) número de banheiros, (3.) área, (4.) andar, (5.) quantidade de andares (no edifício em que este se encontra) e (6.) aluguel.

```r
> metrics <- subset(sample, select = c(num..dorm., num..banheiros, área, andar, quantidade.de.andares, aluguel))
```

Em análise fatorial prévia identificamos que estas variáveis podem ser agrupadas em três componentes principais, utilizando o critério do "cotovelo" para o seguinte gráfico de scree:

![](Imagens/a9bd644f26f7200aa2c17dc7ee291236d10bc872.png)

```r
> sample.pca <- prcomp(metrics, scale. = T, center = T)
> sample.pca$x <- -1 * sample.pca$x
> sample.pca$rotation <- -1 * sample.pca$rotation
> screeplot(sample.pca, type = "lines")
```

Projetando-se um gráfico de dispersão tridimensional para os componentes principais enquanto eixos, obtemos o seguinte resultado:

| ![](Imagens/2022-12-09-10-11-34-image.png) | ![](Imagens/2022-12-09-10-12-21-image.png) |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-12-09-10-12-58-image.png) | ![](Imagens/2022-12-09-10-13-41-image.png) |

```r
> pca3d(sample.pca)
```

Ou seja, temos os imóveis agrupados em uma forma que se assemelha a uma forma monolítica de base quadrada com pontos que se desprendem desta todos em uma mesma direção diagonal, formando faixas mais ou menos difusas. Este gráfico refere-se aos seguintes parâmetros exibidos pelo sumário da análise de componentes principais:

```r
> sample.pca
Standard deviations (1, .., p=6):
[1] 1.6990877 1.3692924 0.7022460 0.5675361 0.5086914 0.4051243
```

```r
Rotation (n x k) = (6 x 6):
                             PC1        PC2         PC3         PC4
num..dorm.            -0.4447834 -0.3609772 -0.14882876  0.29538595
num..banheiros        -0.4846283 -0.2548950 -0.01232379  0.55004856
área                  -0.4211207 -0.3697044 -0.27536198 -0.75918853
andar                 -0.3057931  0.5574454 -0.38789628  0.02741113
quantidade.de.andares -0.3210132  0.5627055 -0.19995337 -0.02263448
aluguel               -0.4397745  0.2016429  0.84346253 -0.18045167
                              PC5         PC6
num..dorm.            -0.74989576 -0.01080347
num..banheiros         0.62855370  0.04877346
área                   0.18311717  0.01589437
andar                  0.01042246 -0.66664963
quantidade.de.andares -0.06024895  0.73224963
aluguel               -0.07284323 -0.12899828
> summary(sample.pca)
Importance of components:
                          PC1    PC2     PC3     PC4     PC5     PC6
Standard deviation     1.6991 1.3693 0.70225 0.56754 0.50869 0.40512
Proportion of Variance 0.4812 0.3125 0.08219 0.05368 0.04313 0.02735
Cumulative Proportion  0.4812 0.7936 0.87584 0.92952 0.97265 1.00000
```

Observa-se por este sumário que dos três primeiros eixos o número de banheiros, o número de dormitórios e a área encontram-se mais fortemente alinhados ao primeiro eixo; o andar e a quantidade de andares ao segundo; e o aluguel ao terceiro. Esse alinhamento torna-se ainda mais pronunciado rotacionando-se os eixos dos componentes principais ortogonalmente pelo método "varimax":

```r
> sample.principal <- principal(metrics, nfactors = 3, rotate = "varimax")
> sample.principal
Principal Components Analysis
Call: principal(r = metrics, nfactors = 3, rotate = "varimax")
Standardized loadings (pattern matrix) based upon correlation matrix
                       RC1   RC2  RC3   h2    u2 com
num..dorm.            0.90 -0.01 0.14 0.83 0.174 1.0
num..banheiros        0.84  0.10 0.28 0.80 0.200 1.2
área                  0.90  0.00 0.04 0.81 0.194 1.0
andar                 0.04  0.95 0.12 0.93 0.073 1.0
quantidade.de.andares 0.02  0.92 0.25 0.91 0.089 1.1
aluguel               0.27  0.34 0.89 0.99 0.015 1.5
```

```r
                       RC1  RC2  RC3
SS loadings           2.40 1.88 0.97
Proportion Var        0.40 0.31 0.16
Cumulative Var        0.40 0.71 0.88
Proportion Explained  0.46 0.36 0.19
Cumulative Proportion 0.46 0.81 1.00

Mean item complexity =  1.2
Test of the hypothesis that 3 components are sufficient.

The root mean square of the residuals (RMSR) is  0.05 
 with the empirical chi square  304.08  with prob <  NA 

Fit based upon off diagonal values = 0.99
```

Tal qual demonstram os seguintes gráficos:

<img title="" src="Imagens/52ca6db45e43be865bbe71836d6ace4921294ef0.png" alt="" width="400" data-align="center">

```r
> biplot(sample.principal, choose = c(1,2))
```

<img src="Imagens/e206622784716e13772b63a28b8034c8aa1e9912.png" title="" alt="" width="400" data-align="center">

```r
> biplot(sample.principal, choose = c(1,3))
```

<img src="Imagens/4c232dd26a5f7e44787252ccfcdbd259dc94147f.png" title="" alt="" width="400" data-align="center">

```r
> biplot(sample.principal, choose = c(2,3))
```

Tem-se que os imóveis podem ser situados e agrupados em função de três dimensões principais:

- Número de cômodos

- Preço do aluguel

- Verticalidade (andar em que o imóvel se encontra mais o número de andares do edifício)

### Escalonamento Multidimensional

#### Por distância Euclidiana

Iniciamos nossa análise de escalonamento multidimensional utilizando uma matriz de distâncias euclidianas entre as observações, parâmetro este padrão para função `dist` da linguagem R.

```r
euclid <- dist(scale(metrics, center = T, scale = T))
```

Cria-se o modelo de análise MDS:

```r
sample.mds <- cmdscale(euclid, eig = T)
```

Utilizamos os autovalores (*Eigenvalues*) para calcular o quanto da variância na distância entre as observações cada eixo abarca:

```r
> head(mds.var.per <- round(sample.mds$eig/sum(sample.mds$eig) * 100, 1), 9)
[1] 48.1 31.2  8.2  5.4  4.3  2.7  0.0  0.0  0.0
```

Vê-se que seis eixos representam a toda a variância, no mais, os valores aqui observados são *os mesmos* para os eixos da Análise de Componentes Principais antes destes serem rotacionados. Não apenas, ao desenharmos um gráfico de dispersão observamos que mesmo a posição dos pontos também é a mesma, com o eixo principal invertido:

| PCA                                                                                                  | MDS (distância euclidiana)                                                                           |
|:---------------------------------------------------------------------------------------------------- |:---------------------------------------------------------------------------------------------------- |
| <img src="Imagens/ed7e74c82c64f6b76b17a3f3c041d474995c59e0.png" title="" alt="" data-align="center"> | <img title="" src="Imagens/31f26e290204b5bfc031e02ed20f40be73125431.png" alt="" data-align="center"> |
| `> plot(data.frame(PC1 = sample.pca$x[,1], PC2 = sample.pca$x[,2]))`                                 | `> plot(data.frame(PC1 = sample.mds$points[,1], PC2 = sample.mds$points[,2]))`                       |

Em nossa análise fatorial, pudemos identificar que em todas as nossas amostras a variação observada podia ser explicada em sua quase totalidade em três componentes principais.

#### Por distância de Minkowski

Não obtendo qualquer ganho a nossa análise pelo parâmetro de distância euclidiana, seguimos a experimentar com outros parâmetros de distância. A distância ou métrica de Minkowski[^2] é uma generalização da qual podemos obter uma variedade de parâmetros tais: dados dois pontos $X = (x_1, x_2, \dots, x_n)$ e $Y(y_1,y_2, \dots, y_n) \in \R^n$ esta pode ser definida por

$$
D(X,Y) = \left(\sum^n_{i = 1} |x_i - y_i|^p\right)^{\frac 1p}
$$

Onde alterando-se o valor de $p$ obtemos:

- quando $p = 2$ a distância Euclidiana;

- quando $p = 1$ a distância de Manhattan, ou *Taxicab*;

- quando $\lim_{p \to \infty}$ a distância de Chebyshev, ou "maximalista";

- dentre outras.

À seguir experimentamos com a distância de Manhattan e Chebyshev, começando pelo primeiro.

```r
> taxicab <- dist(scale(metrics, center = T, scale = T), method = "minkowski", p = 1)
> sample.mds.taxicab <- cmdscale(taxicab, eig = T, x.ret = T)
> mds.var.per.taxicab <- round(sample.mds.taxicab$eig/ sum(sample.mds.taxicab$eig) * 100, 1)
```

Ao criar o modelo, nota-se que alguns dos últimos autovalores são negativos:

```r
> tail(mds.car.per.taxicab)
[1] -1.7 -2.1 -2.2 -2.4 -5.0 -8.5
```

Então para calcular a participação de cada eixo na variação total, utilizamos valores absolutos:

```r
> mds.var.per.taxicab <- as.data.frame(round(abs(sample.mds.taxicab$eig)/ sum(abs(sample.mds.taxicab$eig)) * 100, 1))
> names(mds.var.per.taxicab) <- "variance_percentage"
> mds.var.per.taxicab$axis <- 1:nrow(> mds.var.per.taxicab)
```

E  à partir dos valores obtidos, geramos o seguinte gráfico scree (limitado para abranger os 50 primeiros autovalores):

<img title="" src="Imagens/cd7475f76604059e1434c7ffe557c1edbffec1d4.png" alt="" data-align="center" width="500">

```r
> ggplot(mds.var.per.taxicab, aes(x = axis, y = variance_percentage, group = 1)) + geom_line() + geom_point() + xlim(0,50)
```

Novamente tem-se que os primeiros três eixos descrevem juntos a maior porção da variância, não obstante, a variância explicada por esta é significativamente menor, pouco mais de 50%:

```r
> head(round(cumsum(abs(sample.mds.taxicab$eig) / sum(abs(sample.mds.taxicab$eig)) * 100), 1))
[1] 29.7 45.6 50.3 54.2 56.6 58.5
```

O agrupamento no gráfico de dispersão é semelhante, mas com as observações mais distantes ainda mais espaçadas entre si:

<img title="" src="Imagens/1e9ff26c75acb85367394093459ba9beae93e635.png" alt="" data-align="center" width="600">

```r
> sample.mds.taxicab$points[,2] <- -1 * sample.mds.taxicab$points[,2]
> plot(data.frame(PC1 = sample.mds.taxicab$points[,1], PC2 = sample.mds.taxicab$points[,2]))
```

Seguimos com o uso da distância de Chebyshev:

```r
> sample.mds.maximum <- cmdscale(dist(scale(metrics, center = T, scale = T), method = "maximum"), eig = T)
> mds.var.per.maximum <- as.data.frame(round(abs(sample.mds.maximum$eig)/ sum(abs(sample.mds.maximum$eig)) * 100, 1))
> names(mds.var.per.maximum) <- "variance_percentage"
> mds.var.per.maximum$axis <- 1:nrow(> mds.var.per.maximum)
> head(round(cumsum(abs(sample.mds.maximum$eig) / sum(abs(sample.mds.maximum$eig)) * 100), 1), n = 9)
[1] 16.9 29.2 34.1 37.8 41.2 44.1 46.1 47.3 48.4
```

Tem-se que o modelo com distância maximalista é ainda menos ajustado que o modelo da distância de Manhattan.

![](Imagens/38607e9cc9e682c1eeeae8ecdb3d8ab481957632.png)

```r
> ggplot(mds.var.per.maximum, aes(x = axis, y = variance_percentage, group = 1)) + geom_line() + geom_point() + xlim(0,50)
```

O screeplot aponta que os 8 primeiros eixos são os mais significativos para explicação da variância, não obstante estes abarcam apenas 47.3% da variação total.

![](Imagens/704d40fe9d4ee64943c3d169fa771b19a2929abd.png)

```r
> plot(data.frame(PC1 = sample.mds.maximum$points[,1], PC2 = sample.mds.maximum$points[,2]), col = c("#FDFD97", "#CC99C9", "#9EC1CF", "#9EE09E", "#FEB144")[sample$cidade])
```

Embora seja possível o desenho do gráfico acima, sabemos que este só explica a 29.2% da variação total.

#### Por distância de Mahalanobis

Por fim, aplicamos a distância de Mahalanobis[^3] para examinarmos de outra forma as correlações entre variáveis. Esta pode ser definida como:

$$
D_M(X) = \sqrt{(X - \mu)^T S^{-1}(X - \mu)}
$$

Sendo:

- $\mu$ é o vetor de médias dos valores usados;

- $S$ é a matriz de covariância;

- e $X$ um vetor multivariado representando uma observação.

Ou seja, é a multiplicação quadrática simples das médias diferenciais com a matriz inversa da matriz de covariância conjunta. O resultado é usado como medida da distância entre um ponto P e uma distribuição D (ou melhor, o valor médio dela).

```r
> library(biotools)
> sample.mds.mahalanobis <- cmdscale(D2.dist(metrics, cov(metrics)), eig = T)
> head(round(cumsum(abs(sample.mds.maximum$eig) / sum(abs(sample.mds.maximum$eig)) * 100), 1), n = 9)
[1] 16.9 29.2 34.1 37.8 41.2 44.1 46.1 47.3 48.4
```

![](Imagens/02c68091ed14ce07a967f82fc0692ba7d04636e2.png)

Observa-se que a distância de Mahalanobis em nosso caso produziu um número elevado de eixos principais (7, não houve redução de dimensionalidade) com os primeiros eixos abarcando um percentual relativamente pequeno da variância total.

![](Imagens/4c751443b74a9b1bdc698f9bff853f6199932235.png)

O gráfico de dispersão continua a apresentar uma configuração monolítica das observações, que se dispersam gradualmente sem notáveis transições, tornando difícil uma investigação de correlação entre estas.

## Conclusão

Os métodos de análise por escalonamento multidimensional se demonstraram na análise de nosso *dataset* de utilidade equivalente ou menor que a análise de componentes principais, com demais métricas de distância demonstrando uma porção menor da variância quando comparadas a distância euclidiana.

[^1]: BANERJEE, S. **House Rent Prediction Dataset**. Disponível em: <[House Rent Prediction Dataset | Kaggle](https://www.kaggle.com/datasets/f3fcbc8053a9243797d18047789e027fbffc29deed67c3c323b830c94b9e8fe3)>. Acesso em: 2 set. 2022.

[^2]: **Minkowski distance**. Disponível em: <[Minkowski distance - Wikipedia](https://en.wikipedia.org/w/index.php?title=Minkowski_distance&oldid=1065591739)>. Acesso em: 10 dez. 2022.

[^3]: **Minkowski distance**. Disponível em: <[Minkowski distance - Wikipedia](https://en.wikipedia.org/w/index.php?title=Minkowski_distance&oldid=1065591739)>. Acesso em: 10 dez. 2022.