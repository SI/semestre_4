# Relatório

> Aplicação da técnica de Análise de Clusters (conglomerados)

| Aluno                      | nUSP     |
| -------------------------- | -------- |
| Cesar Billalta Yamasato    | 12542992 |
| Guilherme de Abreu Barreto | 12543033 |
| Lucas Panta de Moura       | 12608650 |

## Resumo

O presente relatório descreve a aplicação da técnica de Análise de Clusters para fins exploratórios. Anteriormente, em uma análise fatorial, algumas das amostras que estudamos apresentaram observações cuja representação gráfica apontava notável aglomeração. Então buscamos aplicar aqui as técnicas de agrupamento hierárquico e não hierárquico para ver se, para este contexto, estas são capazes de retornar agrupamentos claramente definidos.

## Descrição do banco de dados e seu tratamento

Fazemos uso do banco de dados House Rent Prediction Dataset[^1], o qual possui dados extraídos do site Magic Bricks ([https://www.magicbricks.com/](https://www.magicbricks.com/)). O site permite a seus usuários anunciar seus imóveis na plataforma, ao que parece, sem exercer muito controle sobre a forma como isto é feito. Temos imóveis listados com uma área inferior à 1 m², ou com 10 metros quadrados e 4 banheiros, o que cremos serem erros. De forma a não ter estes domicílios afetarem o modelo na capacidade deste em descrever imóveis de características mais comuns, após ter traduzido o dataset procedemos à retirar deste outliers em seus parâmetros quantitativos. Utilizamos como parâmetro de corte a amplitude interquartil (IQR ‒ InterQuartile Range): se, em qualquer parâmetro, um domicílio apresenta um valor Val para um atributo X tal que:

No mais, a nosso próprio critério, removemos os imóveis aqueles descritos com 6 m² ou menos de área. Com isso restam-nos 3.992 do total de 4.498 descritos pelo banco de dados.

Por fim, como estes 3992 imóveis estão distribuídos em cinco cidades distintas, ocorre que o parâmetro “bairro” em geral não chega a acomodar mais de dois imóveis a cada tempo e, portanto, não contribui para apontar correlações significativas entre os imóveis. Assim sendo, este fator foi desprezado em nossa análise. Ao sumarizar-se o resultado desta operação, obtemos o seguinte *dataset*:

```r
 > summary(sample)
       X        anunciado.em         num..dorm. 
 Min.   :   1   Length:3992        Min.   :1.000
 1st Qu.:1414   Class :character   1st Qu.:1.000
 Median :2534   Mode  :character   Median :2.000
 Mean   :2474                      Mean   :1.901
 3rd Qu.:3649                      3rd Qu.:2.000
 Max.   :4746                      Max.   :3.000

    num..banheiros       área       tipo.de.área
 Min.   :1.000   Min.   :  7.0   Length:3992
 1st Qu.:1.000   1st Qu.:118.0   Class :character
 Median :2.000   Median :217.0   Mode  :character
 Mean   :1.746   Mean   :220.5
 3rd Qu.:2.000   3rd Qu.:311.0
 Max.   :3.000   Max.   :472.0

    andar        quantidade.de.andares
 Min.   : 0.000   Min.   : 0.000
 1st Qu.: 1.000   1st Qu.: 2.000
 Median : 2.000   Median : 3.000
 Mean   : 2.432   Mean   : 5.067       
 3rd Qu.: 3.000   3rd Qu.: 5.000       
 Max.   :34.000   Max.   :48.000 

    andar            cidade    
 Length:3992      Bangalore:824
 Class :character Chennai  :824
 Mode  :character Delhi    :519
                  Hyderabad:785
                  Mumbai   :538

    mobília                        tipo.de.inquilino
 Inteiramente mobiliado: 467    Famílias       : 355
 Não mobiliado         :1657    Sem preferência:2962
 Parcialmente mobiliado:1868    Solteiros      : 675
```

```r
    ponto.de.contato    aluguel   
 Corretor    : 926    Min.   :0.150
 Proprietário:3066    1st Qu.:0.900
                      Median :1.400
                      Mean   :1.777
                      3rd Qu.:2.200
                      Max.   :5.700
```

Sendo:

- $X$ a variável que identifica numérica e sequencialmente cada observação.

- O valor do aluguel descrito em dezenas de milhares de rupias indianas (₹).

- 0 para o valor de andar descreve um andar térreo.

## Análise de Clusters

### Análise dos objetos a serem agrupados

Em nossa análise fatorial, pudemos identificar que em todas as nossas amostras a variação observada podia ser explicada em sua quase totalidade em três componentes principais.

| ![](Imagens/9887840bdfb94b4ca76de0e81a0d208b894ca643.png) | ![](Imagens/3d1df7541e055239765ded7e6c1eae7ca842ca95.png) | ![](Imagens/cd5f6ecdad8476e7536db9c8ccb3a7b0ae8420d4.png) |
| --------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------- |
| ![](Imagens/f52cdf475e478828226298cac0380eaac6b0d4d4.png) | ![](Imagens/7cf707d76f08073bfcf0d4211e7aa4181036c3c2.png) | ![](Imagens/26af152e990372c749c45d92689ced0fc44cec0d.png) |

No mais, nalgumas amostras, observa-se a formação de agrupamentos bastante definidos, tais quais planos orientados paralelamente entre si. Por exemplo, ao aplicarmos o seguinte comando na análise fatorial de Kolkata,

```r
> pca3d(prcomp(subset(sample, select = c(num..dorm., num..banheiros, área, andar, quantidade.de.andares, aluguel), cidade == "Kolkata")))
```

obtemos

| ![](Imagens/2022-11-27-21-41-05-image.png) | ![](Imagens/2022-11-27-21-41-30-image.png) |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-11-27-21-41-50-image.png) | ![](Imagens/2022-11-27-21-42-21-image.png) |

Podemos observar no plano XZ, correspondente ao primeiro e terceiro componentes principais, a formação de aproximadamente 3 agrupamentos, cada vez mais esparsos conforme estes se afastam da origem das coordenadas. Feita a rotação varimax vemos que esta configuração se mantém aproximadamente a mesma, e ao analisarmos a orientação dos eixos das variáveis neste plano, obtemos

```r
> Kolkata.principal <- principal(Kolkata.vars, nfactors = 3, rotate = "varimax", scores = T)
> biplot(Kolkata.principal, choose= c(1,3), main= "Kolkata")
```

<img src="Imagens/3bc41038af84d7af7d150167fddb630da3e3f5ef.png" title="" alt="" data-align="center">

Vê-se claramente que a formação dos planos orienta-se perpendicularmente ao número de banheiros. Ainda, que os planos estão em número correspondente à abrangência do valor desta variável, que vai de 1 à 3 em valores inteiros. Separamos as posições destas observações neste plano para dar prosseguimento a esta análise.

```r
> Kolkata.scores <- Kolkata.principal$scores[, c(1,3)]
```

### Seleção da medida de separação e método de agrupamento

A partir das posições das observações, geramos uma matriz $n \times n$ da distância entre cada observação $i$, $1 \le i \le n$, para qualquer outra observação $j$, $1 \le j \le n$.

```r
> Kolkata.dist <- dist(Kolkata.scores)
```

E damos início ao cálculo da distância intragrupos. Testamos fazê-lo por três métodos: (1) por agrupamento de ligação única, (2) agrupamento de ligação completa e (3) agrupamento médio por grupo.

```r
> plot(Kolkata.cs <- hclust(Kolkata.dist, method = "single"), main = "Ligação simples")
> plot(Kolkata.cc <- hclust(Kolkata.dist, method = "complete"), main = "Ligação completa")
> plot(Kolkata.ca <- hclust(Kolkata.dist, method = "average"), main = "Agrupamento médio")
```

O resultado destes métodos de agrupamento pode ser visto nos seguintes dendrogramas, respectivamente:

<img src="Imagens/20d8d9a804fd1feef89e62476dcb03d4e32a4019.png" title="" alt="" data-align="center">

<img src="Imagens/ef6422778f5b48277c5285784824bf47d9143d23.png" title="" alt="" data-align="center">

<img src="Imagens/8b046e2d9c2a4fd5b641cde224885560444b4f83.png" title="" alt="" data-align="center">

Observa-se que em todos os casos a formação grupos hierárquicos em que há a formação de três grupos principais, dois de tamanho aproximadamente igual e mais um terceiro significativamente menor, o que corresponde a formação de grupos vista nos gráficos.

<div style="page-break-before: always;"></div>

### Verificação dos agrupamentos obtidos

Tendo em vista resultados promissores, prosseguimos comparando os resultados obtidos com o número de banheiros respectivos a cada imóvel. Para tal, criamos um gráfico bidimensional à partir da matriz de distâncias ‒ o qual descreve a posição relativa entre as observações ‒ e neste identificamos

- o número de banheiros em gradações de cinza, onde o preto corresponde à 1 banheiro, cinza escuro à 2 banheiros e cinza claro à 3 banheiros.

- os três grupos principais obtidos a partir do dendrograma em três formas geométricas distintas: triângulo, círculo e quadrado.

Para cada um dos três métodos, obtemos:

```r
> pr <- prcomp(Kolkata.dist)$x[, 1:2]
> plot(Kolkata.pr, pch = (0:2)[cutree(Kolkata.cs, k = 3)], col = c("black", "darkgrey", "lightgrey")[Kolkata$num..banheiros], xlim = range(pr) * c(1, 1.5), main = "Ligação simples")
```

<img src="Imagens/0e5a520bc0f39f762af22e4fd7e7c1369cc32087.png" title="" alt="" data-align="center">

```r
> plot(Kolkata.pr, pch = (0:2)[cutree(Kolkata.cc, k = 3)], col = c("black", "darkgrey", "lightgrey")[Kolkata$num..banheiros], xlim = range(pr) * c(1, 1.5), main = "Ligação completa")
```

<img title="" src="Imagens/a543d528e9fcbb372934083595864df026e63f2a.png" alt="" data-align="center" width="345">

```r
> plot(Kolkata.pr, pch = (0:2)[cutree(Kolkata.ca, k = 3)], col = c("black", "darkgrey", "lightgrey")[Kolkata$num..banheiros], xlim = range(pr) * c(1, 1.5), main = "Agrupamento m")
```

<img title="" src="Imagens/e7afbd915160a4fadc4688fd8caa919bdb9121a8.png" alt="" data-align="center" width="331">

Observamos que pelo método de ligação simples a separação dos grupos em função do número de banheiros se dá precisamente, enquanto os demais métodos apresentam resultados indistinguíveis entre si e com agrupamentos que misturam imóveis com diferentes números de banheiros.

## Validação do modelo

Vimos no caso de Kolkata que a análise de clusters utilizando-se do agrupamento hierárquico nos permitiu identificar precisamente grupos de imóveis homogêneos entre si, e por qual fator. Assim sendo, continuamos a conferir a aplicação do método em nossas demais amostras, as quais apresentavam algum nível de agrupamento. Outra cidade que observamos foi Hyderabad, cujo gráfico de dispersão é o seguinte:

| ![](Imagens/2022-11-28-12-35-29-image.png) | ![](Imagens/2022-11-28-12-35-49-image.png) |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-11-28-12-36-10-image.png) | ![](Imagens/2022-11-28-12-36-32-image.png) |

Ao observarmos o gráfico de determinado ângulo, foi possível observar que as observações apresentavam uma formação daquilo que aparentavam ser 5 grupos distintos.

<img src="Imagens/2022-11-28-12-38-20-image.png" title="" alt="" data-align="center">

A formação destes grupos se dá a um ângulo do plano PC1 x PC3, mas após a rotação varimax conseguimos que estes primeiros se alinhem-se melhor a este último, como se vê no gráfico seguinte.

<img src="Imagens/a1f333c227eeb78bcef427a08da2b3b6dbc2399e.png" title="" alt="" data-align="center">

Vê-se também que os planos são cortados quase que perpendicularmente por duas variáveis: número de banheiros e número de dormitórios. Como cada domicílio possui no mínimo um de cada e em nossa amostra possui no máximo três de cada, é possível que a formação destes 5 planos esteja ocorrendo em função do número de cômodos destes tipos, que pode ser desde 2 até 6 no total. Para verificar se este é o caso, acrescentamos ao dataframe de Hyderabad uma coluna “comodos”, que descreve a soma do número de banheiros e dormitórios no imóvel, para posterior análise.

```r
> Hyderabad$comodos <- Hyderabad$num..dorm. + Hyderabad$num..banheiros
```

Prosseguimos tal qual anteriormente,  e por fim criamos um gráfico bidimensional à partir da matriz de distâncias identificando

- o número de cômodos cada qual em um cor: 2 em lilás, 3 em ciano, 4 em verde, 5 em laranja e 6 em vermelho;

- os cinco grupos principais obtidos a partir do dendrograma em três formas geométricas distintas: x, cruz, triângulo, círculo e quadrado.

Para cada um dos três métodos, obtemos:

```r
plot(Hyderabad.pr, pch = (0:4)[cutree(Hyderabad.cs, k = 5)], col = c("#FDFD97", "#CC99C9", "#9EC1CF", "#9EE09E", "#FEB144", "#FF6663")[Hyderabad$comodos], main = "Ligação simples")
```

<img title="" src="Imagens/68f221b5b89b266c9a5f28e86f27ef828a6eea69.png" alt="" width="349" data-align="center">

```r
> plot(Hyderabad.pr, pch = (0:4)[cutree(Hyderabad.cc, k = 5)], col = c("#FDFD97", "#CC99C9", "#9EC1CF", "#9EE09E", "#FEB144", "#FF6663")[Hyderabad$comodos], main = "Ligação completa")
```

<img title="" src="Imagens/b792f6b6936fefc4dea98ccadae1faf5df28c97b.png" alt="" data-align="center" width="374">

```r
> plot(Hyderabad.pr, pch = (0:4)[cutree(Hyderabad.ca, k = 5)], col = c("#FDFD97", "#CC99C9", "#9EC1CF", "#9EE09E", "#FEB144", "#FF6663")[Hyderabad$comodos], main = "Agrupamento médio")
```

<img title="" src="Imagens/7eb5d64ba112f826eef2a080857e2f2edd483084.png" alt="" width="400" data-align="center">

Observa-se que nenhum dos métodos adequadamente distinguiu entre os grupos, ainda que exista uma pequena quantidade de imóveis de um dado grupo geograficamente mais próximos a imóveis doutro grupo no gráfico. Dada a falha deste método de tratamento, prosseguimos a tentar uma nova avaliação fazendo uso de um método de separação não hierárquico, o *k-means*. Para este estabelecemos de antemão o número de grupos a serem formados como sendo 5, e calculamos a soma dos quadrados dentro de cada grupo.

```r
> for (i in 1:5) Hyderabad.wss[i] <- sum(kmeans(Hyderabad.score, centers=i)$withinss)
> Hyderabad.wss
[1] 1568.0000 1061.6628  798.0023  530.8743  361.6144
```

De maneira similar ao procedimento com a análise de componentes principais, vemos o valor da soma dos quadrados por grupo em um gráfico Scree e vemos que não existe um dado ponto em que a variação desta é drasticamente reduzida de um grupo para outro, o que implica que a divisão em 5 grupos é adequada à nossa análise.

```r
> plot(1:5, Hyderabad.wss,type="b",xlab="Número de grupos", ylab="Soma quadros dentro dos grupos")
```

![](Imagens/3d3ea948661ed0af99f32a6d53b255054552fc6f.png)

Geramos novamente o gráfico das posições onde os grupos são distinguidos por número de cômodos por cores, e em função da análise *k-means* representado em formas geométricas. E temos que este método também falhou em corretamente identificar os grupos

```r
> plot(Hyderabad.pr, pch = kmeans(Hyderabad.dist, centers = 5)$cluster, col = c("#FDFD97", "#CC99C9", "#9EC1CF", "#9EE09E", "#FEB144", "#FF6663")[Hyderabad$comodos], main = "Agrupamento por k-means")
```

<img title="" src="Imagens/f9529aba36914805a88323b804cfb5a906a58a35.png" alt="" width="471" data-align="center">

## Conclusão

Embora inicialmente a análise de conglomerados com o método de agrupamento hierárquico houvesse se demonstrado bastante preciso no caso de Kolkata, para as cidades aquelas como Hyderabad onde há um número maior de grupos, mais próximos entre si, e onde um número maior (duas) de variáveis impactam mais fortemente a formação destes, tanto o método hierárquico quanto o não hierárquico, cujas aplicações foram aqui demonstram, produziram resultados os quais são menos satisfatórios que aqueles obtidos por meio de uma avaliação visual da amostra e uma racionalização dos eixos que a orientam.

Não obstante, é possível que os métodos aqui descritos possam ser melhor aproveitados dado outro método de rotação da amostra, ou de redução de sua dimensionalidade, de tal forma a mitigar a interação entre grupos próximos e aproximar observações separadas entre si por fatores os quais, para a presente análise, poderiam ser desprezadas. Esses são fatores passíveis a serem estudados para qualquer análise subsequente.

[^1]: BANERJEE, S. **House Rent Prediction Dataset**. Disponível em: <[House Rent Prediction Dataset | Kaggle](https://www.kaggle.com/datasets/f3fcbc8053a9243797d18047789e027fbffc29deed67c3c323b830c94b9e8fe3)>. Acesso em: 2 set. 2022.