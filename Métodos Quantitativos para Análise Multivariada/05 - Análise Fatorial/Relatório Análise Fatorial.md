# Relatório

> Aplicação da Análise fatorial na redução de dimensionalidade de um dataset

| Aluno                      | nUSP     |
| -------------------------- | -------- |
| Cesar Billalta Yamasato    | 12542992 |
| Guilherme de Abreu Barreto | 12543033 |
| Lucas Panta de Moura       | 12608650 |

## Resumo

O presente relatório descreve a aplicação da técnica de Análise Fatorial para fins exploratórios sobre variáveis métricas, objetiva-se a redução de dimensionalidade de um dataset extraindo destes fatores que agrupam suas variáveis em função da variação comum observada entre estas. Por fim o modelo é validado pela coerência que este mantém dada sua aplicação em diferentes amostras e os resultados desta validação são apresentados e discutidos.

## Descrição do banco de dados e seu tratamento

Fazemos uso do banco de dados House Rent Prediction Dataset[^1], o qual possui dados extraídos do site Magic Bricks ([https://www.magicbricks.com/](https://www.magicbricks.com/)). O site permite a seus usuários anunciar seus imóveis na plataforma, ao que parece, sem exercer muito controle sobre a forma como isto é feito. Temos imóveis listados com uma área inferior à 1 m², ou com 10 metros quadrados e 4 banheiros, o que cremos serem erros. De forma a não ter estes domicílios afetarem o modelo na capacidade deste em descrever imóveis de características mais comuns, após ter traduzido o dataset procedemos à retirar deste outliers em seus parâmetros quantitativos. Utilizamos como parâmetro de corte a amplitude interquartil ($IQR$ ‒ InterQuartile Range): se, em qualquer parâmetro, um domicílio apresenta um valor $Val$ para um atributo $X$ tal que:

No mais, a nosso próprio critério, removemos os imóveis aqueles descritos com 6 m² ou menos de área. Com isso restam-nos 3.992 do total de 4.498 descritos pelo banco de dados.

Por fim, como estes 3992 imóveis estão distribuídos em cinco cidades distintas, ocorre que o parâmetro “bairro” em geral não chega a acomodar mais de dois imóveis a cada tempo e, portanto, não contribui para apontar correlações significativas entre os imóveis. Assim sendo, este fator foi desprezado em nossa análise. Ao sumarizar-se o resultado desta operação, obtemos o seguinte *dataset*:

```r
 > summary(sample)
       X        anunciado.em         num..dorm. 
 Min.   :   1   Length:3992        Min.   :1.000
 1st Qu.:1414   Class :character   1st Qu.:1.000
 Median :2534   Mode  :character   Median :2.000
 Mean   :2474                      Mean   :1.901
 3rd Qu.:3649                      3rd Qu.:2.000
 Max.   :4746                      Max.   :3.000

    num..banheiros       área       tipo.de.área
 Min.   :1.000   Min.   :  7.0   Length:3992
 1st Qu.:1.000   1st Qu.:118.0   Class :character
 Median :2.000   Median :217.0   Mode  :character
 Mean   :1.746   Mean   :220.5
 3rd Qu.:2.000   3rd Qu.:311.0
 Max.   :3.000   Max.   :472.0

    andar        quantidade.de.andares
 Min.   : 0.000   Min.   : 0.000
 1st Qu.: 1.000   1st Qu.: 2.000
 Median : 2.000   Median : 3.000
 Mean   : 2.432   Mean   : 5.067       
 3rd Qu.: 3.000   3rd Qu.: 5.000       
 Max.   :34.000   Max.   :48.000 

    andar            cidade    
 Length:3992      Bangalore:824
 Class :character Chennai  :824
 Mode  :character Delhi    :519
                  Hyderabad:785
                  Mumbai   :538

    mobília                        tipo.de.inquilino
 Inteiramente mobiliado: 467    Famílias       : 355
 Não mobiliado         :1657    Sem preferência:2962
 Parcialmente mobiliado:1868    Solteiros      : 675
```

```r
    ponto.de.contato    aluguel   
 Corretor    : 926    Min.   :0.150
 Proprietário:3066    1st Qu.:0.900
                      Median :1.400
                      Mean   :1.777
                      3rd Qu.:2.200
                      Max.   :5.700
```

Sendo:

- $X$ a variável que identifica numérica e sequencialmente cada observação.  

- O valor do aluguel descrito em dezenas de milhares de rupias indianas (₹).  

- 0 para o valor de andar descreve um andar térreo.

## Análise Fatorial

### Tipo de análise

Com nosso *dataset* buscamos predizer valores de aluguéis com base nas características de cada imóvel. Assim sendo, optamos por conduzir nossa análise fatorial de forma **exploratória**, objetivando um resumo dos dados em fatores os quais permitem a identificação de estruturas que contribuem, em maior ou menor escala, para a precificação do aluguel.

Como visto em análises anteriores, o preço do aluguel pode variar bastante entre imóveis equivalentes, em termos das suas variáveis métricas, a depender deste localizar-se nesta ou naquela cidade; sendo Mumbai a cidade em que esta dinâmica fica mais evidente quando comparada a todas as demais cidades. Assim sendo, para nossas análises iniciais, nos restringimos a abarcar observações tidas todas em uma mesma cidade, no caso, uma amostra de 519 observações na cidade de Delhi.

Ainda, optamos por realizar uma análise fatorial do **tipo R** pois a maior parte dos atributos relativos aos imóveis ‒ à saber: o número de dormitórios, número de banheiros, área, andar e número de andares no edifício em que este se encontra ‒ podem ser descritos em termos de variáveis.

Por fim, apontamos que fizemos uso da linguagem R de programação em nossas análises, assim como funções disponibilizadas pelos seguintes pacotes: `corrplot`, `onewaytests`, `factoextra`, `psych` e `pca3d`.

<div style="page-break-before: always;"></div>

### Suposições acerca das variáveis

Tal qual atestado em análises anteriores, não há distribuição normal presente entre as variáveis observadas, no teste de normalidade Shapiro-Wilk todas apresentam um valor $p$ inferior à 0.05:

```r
> shapiro.test(Delhi$num..dorm.)

    Shapiro-Wilk normality test

data:  Delhi$num..dorm.
W = 0.80717, p-value < 2.2e-16

> shapiro.test(Delhi$num..banheiros)

    Shapiro-Wilk normality test

data:  Delhi$num..banheiros
W = 0.75386, p-value < 2.2e-16

> shapiro.test(Delhi$área)

    Shapiro-Wilk normality test

data:  Delhi$área
W = 0.91212, p-value < 2.2e-16

> shapiro.test(Delhi$andar)

    Shapiro-Wilk normality test

data:  Delhi$andar
W = 0.89631, p-value < 2.2e-16

> shapiro.test(Delhi$quantidade.de.andares)

    Shapiro-Wilk normality test

data:  Delhi$quantidade.de.andares
W = 0.56716, p-value < 2.2e-16
```

Aplicar o teste de Bartlett sobre o dataset **não rejeita** a hipótese nula de que as variáveis não sejam correlacionadas entre si.

```r
> Delhi.vars <- subset(Delhi, select = c(num..dorm., num..banheiros, área, andar, quantidade.de.andares))
```

```r
> bartlett.test(Delhi.vars)

    Bartlett test of homogeneity of variances

data:  Delhi.vars
Bartlett's K-squared = 16138, df = 4, p-value < 2.2e-16
```

Não obstante, este pode ser o caso simplesmente porque as variáveis não apresentam normalidade e este é um teste sensível a desvios da normalidade[^2]. Com uma matriz de correlação, podemos visualizar que as variáveis encontram-se todas **positivamente correlacionadas entre si**. Ainda que o sejam em diferentes proporções as quais favorecem a identificação de fatores:

```r
> matcor <- cor(Delhi.vars)
> print(matcor, digits = 2)
                      num..dorm. num..banheiros  área andar
num..dorm.                 1.000          0.718 0.513 0.053
num..banheiros             0.718          1.000 0.536 0.024
área                       0.513          0.536 1.000 0.046
andar                      0.053          0.024 0.046 1.000
quantidade.de.andares      0.130          0.104 0.114 0.319
                      quantidade.de.andares
num..dorm.                             0.13
num..banheiros                         0.10
área                                   0.11
andar                                  0.32
quantidade.de.andares                  1.00
```

<img title="" src="Imagens/a31e3f543b6eceaf9d73ef2e9711a09395f79b92.png" alt="" width="511" data-align="center">

> Matriz produzida pela chamada `> corrplot(matcor, method = "circle")`.

Enquanto isso, o teste Kaiser-Meyer-Olkin indica o grau de adequação do ajuste da amostra para a análise fatorial como sendo mediano (0,67 de uma escala que vai de 0 à 1):

```r
> KMO(Delhi.vars)
Kaiser-Meyer-Olkin factor adequacy
Call: KMO(r = Delhi.vars)
Overall MSA =  0.67
MSA for each item = 
           num..dorm.        num..banheiros                  área 
                 0.65                  0.64                  0.82 
                andar quantidade.de.andares 
                 0.52                  0.58
```

### Análise de Componentes Principais

Optamos pelo uso da Análise de Componentes Principais como método de obtenção de fatores. Isto pois a Análise de Componentes Principais objetiva explicar a proporção máxima da variância total com o menor conjunto de fatores, e temos a redução de dados como uma preocupação prioritária. Isto pois, ao fazê-lo, buscamos comparar os resultados obtidos contra uma única variável: o aluguel, a fim de encontrar uma fórmula simplificada de estimá-lo.

Procedemos  a criar um modelo para a análise de componentes principais:

```r
> Delhi.pca <- prcomp(Delhi.vars, center = T, scale. = T)
> Delhi.pca$rotation <- -1 * Delhi.pca$rotation
> Delhi.pca$x <- -1 * Delhi.pca$x
> Delhi.pca
Standard deviations (1, .., p=5):
[1] 1.4932202 1.1311947 0.8218865 0.7314622 0.5292992

Rotation (n x k) = (5 x 5):
                            PC1         PC2         PC3          PC4
num..dorm.            0.5835661 -0.10827855  0.03866661 -0.413670441
num..banheiros        0.5856277 -0.14883687  0.03614825 -0.334794703
área                  0.5203458 -0.09409863  0.03553276  0.846493986
andar                 0.1041534  0.71387329  0.69201658 -0.012380951
quantidade.de.andares 0.1867702  0.66906929 -0.71906097 -0.009160604
                              PC5
num..dorm.            -0.68928076
num..banheiros         0.72214513
área                  -0.05070585
andar                  0.02228816
quantidade.de.andares  0.01818231
```

```r
> summary(Delhi.pca)
Importance of components:
                          PC1    PC2    PC3    PC4     PC5
Standard deviation     1.4932 1.1312 0.8219 0.7315 0.52930
Proportion of Variance 0.4459 0.2559 0.1351 0.1070 0.05603
Cumulative Proportion  0.4459 0.7019 0.8370 0.9440 1.00000
```

Vemos pelo sumário que com um total de três componentes já é possível se explicar um total de 83% da variância observada. A primeira componente principal por si só explica quase metade da variância observada, enquanto as duas componentes seguintes explicam cada qual aproximadamente metade do que explica a componente anterior. Para as demais componentes, os ganhos em variância explicada é marginal, tal qual observamos no gráfico de Scree:

![](Imagens/cdddbf623713cbe8ec2743a51c95a2f4582dba1d.png)

> Resultado da chamada da função `> scree(matcor, factors = F)`, Neste a proporção de variância explicada é mensurada em função das componentes e seus respectivos autovalores (desvio padrão elevado ao quadrado)

Assim sendo, optamos por reter em nossa análise estes três primeiros componentes principais. Com isso, já é possível realizarmos representações gráficas que situem a maneira pela qual nossas observações estão dispostas ao longo dos eixos principais:

| ![](Imagens/3af7df54377d011b886b3815c7d7ca31f341eb83.png) | ![](Imagens/2022-11-20-14-12-22-image.png) |
|:---------------------------------------------------------:|:------------------------------------------:|
| ![](Imagens/2022-11-20-14-13-22-image.png)                | ![](Imagens/2022-11-20-14-14-13-image.png) |

> Saída da chamada `> pca3d(Delhi.pca)`, rotacionada em 4 posições diferentes.

Tem-se que as observações encontram-se agrupadas em forma aproximadamente cilíndrica cuja altura encontra-se alinhada ao eixo da primeira componente principal em um ângulo descendente, e cujo diâmetro encontra-se ligeiramente amassado ao longo do eixo da terceira componente principal quando comparado ao diâmetro observado no eixo da segunda componente principal.

No mais, o gráfico *Biplot* revela a seguinte orientação das variáveis ao longo dos eixos das componentes principais:

![](Imagens/65fa33e9fb37fdddc871e26d74cf91d4cda10ca0.png)

> Gráfico resultante da chamada `> fviz_pca_var(Delhi.pca, col.var = "contrib", gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"), repel = TRUE)`

Vê-se que o número de dormitórios, a área do imóvel e o número de banheiros seguem juntas a orientação da altura do cilindro com relação a primeira componente principal, enquanto o andar e a quantidade de andares seguem em direção aproximadamente ortogonal a estes, podendo ser estas as dimensões aquelas que conferem ao cilindro seu diâmetro com relação aos demais eixos.

#### Rotação dos componentes principais

Optamos por rotacionar os eixos principais de maneira a melhorar o alinhamento destes com relação às observações tidas. Dado que a variáveis aparentam se alinhar de maneira bastante próxima aos componentes e formam grupos distintos orientados ortogonalmente entre si, optamos por realizar uma rotação ortogonal, a **varimax**. Rotações ortogonais como a varimax maximizam as cargas fatoriais de cada variável com relação a um único componente e, portanto, não assumem que os componentes estejam correlacionados entre si. Dada nossa matriz de correlação, que aponta a formação de grupos de variáveis não correlacionadas entre si, cremos ser esta uma solução adequada à amostra.

```r
> Delhi.principal <- principal(Delhi.vars, nfactors = 3, rotate = "varimax", scores = T)
> Delhi.principal
Principal Components Analysis
Call: principal(r = Delhi.vars, nfactors = 3, rotate = "varimax", scores = T)
Standardized loadings (pattern matrix) based upon correlation matrix
                       RC1   RC2  RC3   h2      u2 com
num..dorm.            0.88  0.03 0.06 0.78 0.22466 1.0
num..banheiros        0.89 -0.01 0.03 0.79 0.20607 1.0
área                  0.78  0.03 0.05 0.62 0.38410 1.0
andar                 0.02  0.99 0.16 1.00 0.00022 1.1
quantidade.de.andares 0.08  0.16 0.98 1.00 0.00014 1.1

                       RC1  RC2  RC3
SS loadings           2.18 1.00 1.00
Proportion Var        0.44 0.20 0.20
Cumulative Var        0.44 0.64 0.84
Proportion Explained  0.52 0.24 0.24
Cumulative Proportion 0.52 0.76 1.00

Mean item complexity =  1
Test of the hypothesis that 3 components are sufficient.

The root mean square of the residuals (RMSR) is  0.08 
 with the empirical chi square  64.5  with prob <  NA 

Fit based upon off diagonal values = 0.95
> summary(Delhi.principal)

Factor analysis with Call: principal(r = Delhi.vars, nfactors = 3, rotate = "varimax", scores = T)

Test of the hypothesis that 3 factors are sufficient.
The degrees of freedom for the model is -2  and the objective function was  0.18 
The number of observations was  519  with Chi Square =  93.05  with prob <  NA 

The root mean square of the residuals (RMSA) is  0.08 
```

Observa-se que após a rotação as cargas fatoriais do número de dormitórios, da área do imóvel e do número de banheiros todas aumentaram com relação à primeira componente principal (respectivamente e aproximadamente,de 0,58 para 0,88, de 0,58 para 0,89 e de 0,52 à 0,78), enquanto as cargas fatoriais para andares e quantidade de andares passam a demonstrar um alinhamento quase perfeito com o segundo (de 0,71 para 0,99) e terceiro (de -0,71 para 0,98) componentes principais, respectivamente.

| CP1 x CP2                                                 | CP1 x CP3                                                 | CP2 x CP3                                                 |
|:---------------------------------------------------------:| --------------------------------------------------------- | --------------------------------------------------------- |
| ![](Imagens/aaedb8a69318a1971d919bfb19b84aa2c367cd56.png) | ![](Imagens/ad19866ca2c7c9fa49e64c893c2c73c51444b493.png) | ![](Imagens/de8644abf75dbf5501bcf1139bc0fad1c5fc78ee.png) |

> Resultados da chamada `> biplot(Delhi.principal, choose = x, main = "")` para x = c(1,2), x = c(1,3), e x = (2,3), respectivamente.

Com isso, podemos identificar claramente a existência de três fatores: andar e quantidade de andares sozinhas compõem cada qual um fator, enquanto as demais variáveis compõem juntas um terceiro fator, a ser por nós inferido. Cremos que este fator possa ser um **estimador** do preço do aluguel. Isto pois repetimos os procedimentos anteriores tendo o aluguel acrescido como uma sexta variável e temos que este se alinha com os demais fatores ao longo da primeira componente principal, sendo que as cargas fatoriais das demais variáveis são pouco afetadas por este acréscimo e a adequação do modelo como um todo melhora.

```r
> Delhi.vars2 <- subset(Delhi, select = c(aluguel, num..dorm., num..banheiros, área, andar, quantidade.de.andares))
> KMO(Delhi.vars2)
Kaiser-Meyer-Olkin factor adequacy
Call: KMO(r = Delhi.vars2)
Overall MSA =  0.77
MSA for each item = 
              aluguel            num..dorm.        num..banheiros 
                 0.82                  0.77                  0.74 
                 área                 andar quantidade.de.andares 
                 0.85                  0.52                  0.58 
> Delhi.pca2 <- prcomp(Delhi.vars2, center = T, scale. = T)
> Delhi.principal2 <- principal(Delhi.vars2, nfactors = 3, rotate = "varimax", scores = T)
> Delhi.principal2
Principal Components Analysis
Call: principal(r = Delhi.vars2, nfactors = 3, rotate = "varimax", 
    scores = T)
Standardized loadings (pattern matrix) based upon correlation matrix
                       RC1   RC3   RC2   h2      u2 com
aluguel               0.83 -0.03  0.02 0.69 0.30687 1.0
num..dorm.            0.84  0.09  0.01 0.71 0.29124 1.0
```

```r
num..banheiros        0.88  0.05 -0.01 0.77 0.23001 1.0
área                  0.77  0.06  0.03 0.60 0.39808 1.0
andar                 0.02  0.16  0.99 1.00 0.00065 1.1
quantidade.de.andares 0.07  0.98  0.16 1.00 0.00421 1.1

                       RC1  RC3  RC2
SS loadings           2.76 1.01 1.00
Proportion Var        0.46 0.17 0.17
Cumulative Var        0.46 0.63 0.79
Proportion Explained  0.58 0.21 0.21
Cumulative Proportion 0.58 0.79 1.00

Mean item complexity =  1
Test of the hypothesis that 3 components are sufficient.

The root mean square of the residuals (RMSR) is  0.07 
 with the empirical chi square  79.55  with prob <  NA 

Fit based upon off diagonal values = 0.97
> summary(Delhi.principal2)

Factor analysis with Call: principal(r = Delhi.vars2, nfactors = 3, rotate = "varimax", 
    scores = T)

Test of the hypothesis that 3 factors are sufficient.
The degrees of freedom for the model is 0  and the objective function was  0.21 
The number of observations was  519  with Chi Square =  108.48  with prob <  NA 

The root mean square of the residuals (RMSA) is  0.07 
```

## Validação do modelo fatorial

Aplicamos os mesmos procedimentos de análise fatorial feitos sobre as observações em Delhi para as demais cidades, cada qual com um número de observações entre 502 e 824, estes números inclusos. Temos que:

- Os valores de KMO para os modelos PCA sem e com aluguel ficaram aproximadamente os mesmos. A matriz de correlação também apresenta uma divisão das variáveis em dois grupos.

- A variação explicada pelos três primeiros primeiros componentes é sempre maior que 80%, com valor mínimo de 81,27% em Chennai e máximo de 92,53% em Bangalore.

Não obstante estas similaridades, a orientação das variáveis em função dos componentes principais difere em cada cidade e esta diferença só se torna mais explícita após ser feita a rotação ortogonal:

- Senão em Delhi, a quantidade de andar e andar do imóvel encontram-se ambas orientadas ao segundo fator em todas as demais cidades.

- Em Bangalore, Mumbai, e Chennai, o aluguel quando acrescido ao modelo PCA encontra-se orientado aproximadamente ortogonalmente aos demais fatores após a rotação varimax. Em Mumbai o acrécimo da variável aluguel desloca significativamente as demais variáveis em direção a ortogonalidade.

- Em Kolkata, o número de banheiros é ortogonal as demais variáveis, como ou sem o aluguel incluso.

## Conclusão

Embora fossem encontradas correlações interessantes em Delhi passíveis de serem exploradas para se obter um preditor para o valor do aluguel como sendo um dos fatores observados, o modelo de análise que empregamos não se manteve consistente quando aplicado a imóveis de demais cidades em nosso *dataset* e portanto não pôde ser eventualmente **validado**. Não obstante, é possível que Delhi possua particularidades que exigiriam uma adaptação do modelo para tê-lo aplicado às demais cidades e este modelo ainda pudesse ser aplicado consistentemente com novas amostras colhidas em Delhi. Tal investigação exigiria uma atualização do *dataset* para a realização de novos testes.

[^1]: BANERJEE, S. **House Rent Prediction Dataset**. Disponível em: <[House Rent Prediction Dataset | Kaggle](https://www.kaggle.com/datasets/f3fcbc8053a9243797d18047789e027fbffc29deed67c3c323b830c94b9e8fe3)>. Acesso em: 2 set. 2022.

[^2]: **1.3.5.7. Bartlett’s Test. Disponível em: [https://www.itl.nist.gov/div898/handbook/eda/section3/eda357.htm](https://www.itl.nist.gov/div898/handbook/eda/section3/eda357.htm). Acesso em: 19 nov. 2022.**