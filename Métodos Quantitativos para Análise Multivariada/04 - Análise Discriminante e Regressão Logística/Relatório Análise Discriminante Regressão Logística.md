# Relatório

> Aplicação de técnicas de Análise Discriminante e Regressão Logística

## Autores

| Aluno                      | nUSP     |
| -------------------------- | -------- |
| Cesar Billalta Yamasato    | 12542992 |
| Guilherme de Abreu Barreto | 12543033 |
| Lucas Panta de Moura       | 12608650 |

## Resumo

O presente relatório descreve a aplicação de técnicas de análise multivariada para fins preditivos, feita sobre um banco de dados que descreve domicílios disponíveis para locação na Índia, e com o uso da ferramenta *R Studio*.

### Descrição do banco de dados e seu tratamento

Fazemos uso do banco de dados House Rent Prediction Dataset[^1], o qual possui dados extraídos do site Magic Bricks (https://www.magicbricks.com/). O site permite a seus usuários anunciar seus imóveis na plataforma, ao que parece, sem exercer muito controle sobre a forma como isto é feito. Temos imóveis listados com uma área inferior à 1 m², ou com 10 metros quadrados e 4 banheiros, o que cremos serem erros. De forma a não ter estes domicílios afetarem o modelo na capacidade deste em descrever imóveis de características mais comuns, após ter traduzido o dataset procedemos à retirar deste *outliers* em seus parâmetros quantitativos. Utilizamos como parâmetro de corte a amplitude interquartil ($IQR$ ‒ *InterQuartile Range*): se, em qualquer parâmetro, um domicílio apresenta um valor $Val$ para um atributo $X$ tal que:

$$
\text{Exclui(\it X)}\ se\ Val(X) < Val(Q_1) - \frac 32\ IQR\ ou\  Val(X) > Val(Q_3)
+ \frac 32\ IQR
$$

No mais, a nosso próprio critério, removemos os imóveis aqueles descritos com 6 m² ou menos de área. Com isso restam-nos 3.992 do total de 4.498 descritos pelo banco de dados. Ao sumarizarmos o resultado desta operação, obtemos:

```r
       X        anunciado.em         num..dorm. 
 Min.   :   1   Length:3992        Min.   :1.000
 1st Qu.:1414   Class :character   1st Qu.:1.000
 Median :2534   Mode  :character   Median :2.000
 Mean   :2474                      Mean   :1.901
 3rd Qu.:3649                      3rd Qu.:2.000
 Max.   :4746                      Max.   :3.000

    num..banheiros       área       tipo.de.área
 Min.   :1.000   Min.   :  7.0   Length:3992
 1st Qu.:1.000   1st Qu.:118.0   Class :character
 Median :2.000   Median :217.0   Mode  :character
 Mean   :1.746   Mean   :220.5
 3rd Qu.:2.000   3rd Qu.:311.0
 Max.   :3.000   Max.   :472.0

    andar        quantidade.de.andares
 Min.   : 0.000   Min.   : 0.000
 1st Qu.: 1.000   1st Qu.: 2.000
 Median : 2.000   Median : 3.000
 Mean   : 2.432   Mean   : 5.067       
 3rd Qu.: 3.000   3rd Qu.: 5.000       
 Max.   :34.000   Max.   :48.000 

    andar            cidade    
 Length:3992      Bangalore:824
 Class :character Chennai  :824
 Mode  :character Delhi    :519
                  Hyderabad:785
                  Mumbai   :538

    mobília                        tipo.de.inquilino
 Inteiramente mobiliado: 467    Famílias       : 355
 Não mobiliado         :1657    Sem preferência:2962
 Parcialmente mobiliado:1868    Solteiros      : 675

    ponto.de.contato    aluguel   
 Corretor    : 926    Min.   :0.150
 Proprietário:3066    1st Qu.:0.900
                      Median :1.400
                      Mean   :1.777
                      3rd Qu.:2.200
                      Max.   :5.700
```

Sendo:

- $X$ a variável que descreve numérica e sequencialmente cada observação.

- O valor do aluguel descrito em dezenas de milhares de rupias indianas (₹).

- 0 para os valores de andar descreve um andar térreo.

## Análise discriminante

A primeira técnica que viemos a aplicar foi a análise discriminante. Conforme descreve Hair[^2] a Análise Discriminante consiste em uma combinação linear de duas ou mais variáveis independentes de tal forma a discriminar as obervações em grupos distintos, baseados em categorias determinadas *a priori*. Assim sendo, optamos por agrupar os imóveis em dadas faixas de renda, necessárias ao pagamento do valor do aluguel.

Como seria de praxe, optamos por montar as faixas de renda em termos de múltiplos do salário mínimo, mas logo nos deparamos com o fato de a Índia não possuir um salário mínimo nacional[^3]. Prosseguimos para obter uma valor médio de qual seria este, e com isso obtemos o valor de ₹ 7.871,00, o equivalente a R$ 493, 12 na atual cotação. Dada a nossa amostra, com aluguel de valor mínimo de ₹ 1500 e máximo de ₹ 57.000, aproximadamente 8 salários mínimos, optamos por dividir a amostra em múltiplos de 2:

- **Categoria 1:** menor que 2 salários mínimos;

- **Categoria 2:** de 2 salários mínimos à menos que 4 salários mínimos;

- **Categoria 3:** de 4 salários mínimos à menos que 6 salários mínimos;

- **Categoria 4:** 6 salários mínimos ou mais.

Havendo procurado imóveis para se alugar antes, nosso primeiro instinto foi estimar o custo do aluguel à partir da área do imóvel e seu número de dormitórios. Uma métrica bastante comum à anúncios de imóveis.

<img src="Imagens/83a8e20d8fe9a87cd4216122bdb052ad9df783a8.png" title="" alt="" data-align="center">

Utilizando-se de um gráfico de dispersão onde cada faixa de renda é apontado por uma cor distinta, não é possível se ver uma clara distinção entre os grupos. Não obstante continuamos com a aplicação do método:

```r
> sample.lda <- lda(faixa.salarial ~ área + num..dorm., data = sample)
> sample.lda.p <- predict(sample.lda, newdata = sample[, c(3, 5)])$class
```

E testamos o ajuste do modelo utilizando uma matriz de classificação:

```r
> table(sample.lda.p, sample[, 15])

sample.lda.p    1    2    3    4
           1 2168  754  257   95
           2  147  370  156   45
           3    0    0    0    0
           4    0    0    0    0
```

Notamos imediatamente que o modelo é insuficiente: este sequer prevê a ocorrência de duas faixas de renda. Sabemos que imóveis desta faixa de renda são bastante minoritários em nossa amostra:

```r
> nrow(subset(sample, faixa.salarial >= 3))
[1] 553
```

Então supomos que ao não fornecemos maiores subsídios para encontrá-los, estes tenham sido meramente desprezados. Adicionamos as variáveis quantitativas de que dispomos para equiparar o número de categorias e variáveis independentes:

```r
> sample.lda <- lda(faixa.salarial ~ área + num..dorm. + num..banheiros + quantidade.de.andares, data = sample)
> sample.lda.p <- predict(sample.lda, newdata = sample[, c(3, 4, 5, 8)])$class
```

E assim obtemos:

```r
> table(sample.lda.p, sample[, 15])

sample.lda.p    1    2    3    4
           1 2169  707  164   30
           2  113  307  122   35
           3   15   43   60   31
           4   18   67   67   44
```

Agora o modelo já faz predições que incluem todas as faixas de renda, mas ainda se encontra bastante distante de um valor de acurácia aceitável:

```r
> matconf <- table(sample$faixa.salarial, sample.lda.p)
> sum(diag(matconf))/sum(matconf)
[1] 0.6462926
```

Atribuímos isto ao fato de que a distribuição de tais dados viola os pressupostos de normalidade e homocedasticidade da Análise Discriminante conforme apontam, respectivamente, os seguintes testes de Shapiro-Wilk e Fligner-Killeen:

```r
> shapiro.test(sample$área)

    Shapiro-Wilk normality test

data:  sample$área
W = 0.97008, p-value < 2.2e-16

> shapiro.test(sample$num..dorm.)

    Shapiro-Wilk normality test

data:  sample$num..dorm.
W = 0.79921, p-value < 2.2e-16

> shapiro.test(sample$quantidade.de.andares)

    Shapiro-Wilk normality test

data:  sample$quantidade.de.andares
W = 0.6192, p-value < 2.2e-16

> fligner.test(sample$área, sample$num..dorm., sample$num..banheiros, sample$quantidade.de.andares)

    Fligner-Killeen test of homogeneity of variances

data:  sample$área and sample$num..dorm.
Fligner-Killeen:med chi-squared = 358.03, df = 2,
p-value < 2.2e-16
```

Vemos que em todos os casos tem-se um valor p inferior à 0.05, indicativo de baixa correspondência com uma distribuição normal e pouca homogeneidade das matrizes de variância. Assim o sendo, seguimos à aplicação de uma técnica que não possui tais exigências.

## Regressão logística

Conforme explica Hair, a regressão logística trata-se de uma forma especializada de regressão formulada para prever uma variável categórica e binária à partir uma ou mais variáveis independentes as quais podem ou não serem métricas.

Assim sendo, em nossas análises nos confrontamos com o seguinte dado:

<img src="Imagens/0e87198ec7c8b0a8bae3a257c33994a824b9f6e5.png" title="" alt="" data-align="center">

Nota-se que a cidade Mumbai possui preços de alugueis bastante discrepantes com relação às demais cidades observadas. Então nos propomos a criar um modelo de regressão logística de tal forma a predizer se um dado domicílio encontra-se ou não em Mumbai dada uma combinação de características deste. Começamos por avaliar um modelo que olha apenas para o aluguel para fazer tal afirmação:

```r
> modlr <- glm(em.Mumbai ~ aluguel, data=sample, family = "binomial")
> modlr.p <- data.frame(probability.of.Mumbai = modlr$fitted.values, is.Mumbai = sample$em.Mumbai)
> modlr.p <- modlr.p[order(modlr.p$probability.of.Mumbai, decreasing = FALSE),]
> modlr.p$rank <- 1:nrow(modlr.p)
> ggplot(data=modlr.p, aes(x = rank, y = probability.of.Mumbai)) + geom_point(aes(color = is.Mumbai), alpha = 1, shape = 4, stroke = 2) + xlab("Observação") + ylab("Probabilidade de estar em Mumbai")
```

E com isso obtemos o seguinte gráfico:

<img src="Imagens/65b4c53d8f95ce299e63850b55571161ff56639d.png" title="" alt="" data-align="center">

Observa-se que uma série de erros de predição são cometidos fazendo uso desta variável apenas. Se formos avaliar o ajuste do modelo com base no valor pseudo R² deste, temos:

```r
> pR2(modlr)
fitting null model for pseudo-r2
          llh       llhNull            G2      McFadden 
-1113.7164649 -1578.2526177   929.0723057     0.2943357 
         r2ML          r2CU 
    0.2076353     0.3799530
```

Vê-se pelo índice de McFadden ( que vai de 0 à 1, 1 sendo um modelo de predição perfeito) que este modelo ainda se encontra bastante distante do ideal. Então iteramos sobre este modelo para buscar melhorá-lo. Outros fatores destoantes em Mumbai com relação às demais cidades são:

- A presença do intermédio comercial de um corretor de imóveis, diferentemente das demais cidades, é mais provável que este ocorra que um contato direto com o proprietário do imóvel.

```r
> table(sample$ponto.de.contato, sample$cidade)

               Bangalore Chennai Delhi Hyderabad Kolkata Mumbai
  Corretor           138      89   182        75      76    366
  Proprietário       686     735   337       710     426    172
```

- A significativa maior verticalização dos domicílios, seja no andar que estes ocupam, seja na quantidade de andares que o edifício do qual estes fazem parte dispõe.

<img src="Imagens/abbc5ff6366d3090d8298d1c5c4211ee865911ba.png" title="" alt="" data-align="center">

<img src="Imagens/64e92236ab81cdbea08783beda016dd966707641.png" title="" alt="" data-align="center">

- A menor área dos domicílios se comparado àqueles das demais cidades.

![](Imagens/6c347e8fbc199070a41f189932b5667ab824665e.png)

- A menor média no número de dormitórios por domicílio

![](Imagens/dbaab5197e7f77fa38250700da3f64a1b4ed736c.png)

Adicionamos estas cinco variáveis independentes ao modelo pelo procedimento anteriormente descrito, e assim obtemos o seguinte modelo:

```r
> summary(modlr3)

Call:
glm(formula = em.Mumbai ~ aluguel + andar + quantidade.de.andares + 
    ponto.de.contato + num..dorm., family = "binomial", data = sample)

Deviance Residuals: 
    Min       1Q   Median       3Q      Max  
-3.4935  -0.2517  -0.0908  -0.0446   3.6961  

Coefficients:
                             Estimate Std. Error z value Pr(>|z|)    
(Intercept)                  -0.90902    0.25267  -3.598 0.000321 ***
aluguel                       1.87430    0.09671  19.381  < 2e-16 ***
andar                         0.07174    0.03092   2.321 0.020311 *  
quantidade.de.andares         0.12535    0.02076   6.039 1.55e-09 ***
ponto.de.contatoProprietário -0.35011    0.17576  -1.992 0.046371 *  
num..dorm.                   -3.62382    0.19054 -19.019  < 2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 3156.5  on 3991  degrees of freedom
Residual deviance: 1146.2  on 3986  degrees of freedom
AIC: 1158.2

Number of Fisher Scoring iterations: 7
```

Que corresponde ao seguinte gráfico:

![](Imagens/03a44e643398106a5185b4dfe320195a7c8c23a7.png)

Vê-se uma curva melhor ajustada à um formato de S. Não apenas os resultados relativos à Mumbai estão concentrados à direita, mas o modelo prevê corretamente casos em que o domicílio certamente seria, ou não seria, encontrado em Mumbai. O índice pseudo R² de McFadden obtido também é significativa maior:

```r
> pR2(modlr3)
fitting null model for pseudo-r2
          llh       llhNull            G2      McFadden
 -573.0913067 -1578.2526177  2010.3226219     0.6368824
          r2ML         r2CU 
     0.3956416    0.7239867
```

Em termos de acurácia, isso representa:

```r
> table(sample$em.Mumbai, modlr3.p$is.Mumbai)

       0    1
  0 3177  277
  1  277  261
> matconf <- table(sample$em.Mumbai, modlr3.p$is.Mumbai) 
> sum(diag(matconf))/sum(matconf)
[1] 0.8612224
```

Testes posteriores indicam que o acréscimo doutras variáveis que dispomos ao modelo é capaz de melhorar sua capacidade preditiva, mas em quantidades muito pequenas. Então optamos por apresentá-lo tal qual se encontra por ser o modelo de melhor retorno em termos de predições corretas por quantidade de variáveis independentes.

## Conclusões

A análise discriminante demonstrou-se ser uma técnica inadequada para fins preditivos em nosso banco de dados. Embora esta fosse sensível aos dados, apresentando melhores predições conforme acrescentamos variáveis independentes ao modelo, esta não conseguiu alcançar uma taxa de acurácia satisfatória. Por outro lado, a Regressão Logística apresentou predições bastante acuradas, e mesmo certificar-se alguns resultados. Confirmando assim que esta é uma técnica mais adequada para trabalhar com dados em distribuições não normais e com pouca homogeneidade das matrizes de variância.

[^1]: BANERJEE, S. **House Rent Prediction Dataset**. Disponível em: <[House Rent Prediction Dataset | Kaggle](https://www.kaggle.com/datasets/f3fcbc8053a9243797d18047789e027fbffc29deed67c3c323b830c94b9e8fe3)>. Acesso em: 2 set. 2022.

[^2]: HAIR, J. et al. **Análise Multivariada de Dados**. Tradução: Adonai Sant’Anna. 6. ed. Porto Alegre: Bookman, 2009.

[^3]: **A Guide to Minimum Wage in India**. Disponível em: <[A Guide to Minimum Wage in India - India Briefing News](https://www.india-briefing.com/news/guide-minimum-wage-india-2022-19406.html/)>. Acesso em: 23 out. 2022.