# Problema dos leitores e escritores

> Um problema clássico de multiprogramação e memória

Tem-se uma base de dados (ou base, simplesmente) acessada por múltiplos processos para lê-la ou escrever nesta. Neste sistema, múltiplos processos podem lê-la ao mesmo tempo, porém, quando um destes necessita escrever nela, mais nenhum outro processo pode acessá-la até este terminar de escrever. Sendo assim:

- Todo escritor bloqueia a base de dados a todos os demais processos, e a desbloqueia ao terminar de escrever nela;

- Se a base está desbloqueada para escrita, um processo de leitura que a acessa a bloqueia para escrita. Por outro lado, se ela esta bloqueada para escrita mas o processo a acessá-la busca lê-la, este simplesmente a lê.

- Ao saírem da base os processos de leitura verificam se existem outros processos a lê-la e, senão, a desbloqueiam para a escrita.

- Usualmente, o acesso de escritores necessita ser prioritário sobre o de leitores pois estes primeiros mantêm a base com dados atualizados.
