# Sistemas Operacionais: Escalonamento e Threads

Para obter um sistema com comportamento em tempo real (Sistema de Tempo Real ‒ STR), faz-se necessário que programas sejam divididos em vários processos, cada um previsível e, usualmente, de curta duração. Neste, **eventos** causam a execução de processos.

## Eventos podem ser classificados como

- **Periódicos:** quando ocorrem em intervalos regulares de tempo;

- **Aperiódicos:** quando ocorrem de modo imprevisível.

## Condição para existência de escalonamento em processos periódicos

Um conjunto de processos quaisquer só são escalonáveis se para $m$ eventos periódicos

$$
\sum^m_{i = 1} \frac{C_i}{P_i} \le 1
$$

Sendo:

- $i$ um dado evento;

- $P_i$ é o período do evento $i$;

- $C_i$ é a quantidade de tempo necessária para o evento $i$ ser tratado pela CPU; ou seja,

- $\frac{C_i}{P_i}$ é a fração da CPU usada por $i$.

Dependendo do tempo necessário para cada evento, pode não ser possível processar a todos eles em tempo hábil. Nesta situação tempos um **sistema não escalonável**.

## Algoritmos de escalonamento para STRs

Estes existem em duas variedades:

- **Estáticos:** decisões de escalonamento são tomadas antes do sistema começar a rodar. Atribui-se antecipadamente uma prioridade fixa a cada processo, e então estes escalonam.
  
  - **Rate Monotonic Scheduling:** Cada processo é alocado buscando-se tê-los terminados dentro de um determinado período. Para tal, atribui-se prioridade aos processos de acordo com a frequência destes. Por exemplo considere um processo que executa a cada 30ms, este executa aproximadamente 33 vezes por segundo e, portanto, a prioridade atribuída à este é de valor 33. A cada escalonamento, executa-se o processo de maior prioridade.
  
  <img src="Imagens/2022-09-21-17-17-39-image.png" title="" alt="" data-align="center">

- **Dinâmicos:** decisões de escalonamento são tomadas em tempo de execução. Algoritmos deste tipo não apresentam propriedades fixas.
  
  - **Earliest Deadline First:** Executa-se o processo aquele previsto para terminar antes. Em caso de empate, entre um outro processo e o processo atual, não se troca o contexto.
  
  ![](Imagens/2022-09-27-17-49-15-image.png)

## Threads (linhas de controle)

A unidade básica de utilização da CPU, esta é composta por

- Uma identificação da thread;

- A indicação do estado da thread;

- Um contador de programa;

- Um conjunto de registradores;

- Uma pilha;

No mais, esta compartilha com demais threads **sob um mesmo processo** endereços de memória para o código em execução, dados de uso comum, arquivos abertos, entre outros recursos disponibilizados pelo sistema operacional ao processo, como alarmes.

Um processo tradicional / pesado possui uma única thread. Mas se um processo possui múltiplas threads, este pode realizar múltiplas tarefas ao mesmo tempo. A grande maioria dos programas modernos cria processos que geram múltiplas threads em execução.

![](Imagens/2022-09-27-20-38-07-image.png)

> Esquema representando lado a lado processos com uma única thread (à esquerda) e múltiplas threads (à direita), respectivamente.

### Benefícios da programação em múltiplas threads

**1. Maior responsividade:** Uma aplicação interativa pode, fazendo uso de múltiplas threads, realizar operações demoradas sem por isso interromper a interação do usuário até que esta retorne uma resposta, designando à diferentes threads as operações interativas e as operações custosas, que executam ao mesmo simultaneamente.

**2. Compartilhamento de recursos:** Threads compartilham entre si recursos que as permitem operar sobre um mesmo espaço de endereçamento simultaneamente.

**3. Economia:** Alocar memória e recursos para a criação de processos é custoso. Como threads compartilham recursos do processo ao qual estas pertencem, é mais econômico criar e mudar de contexto entre threads (threads também existem nos estados *executando, bloqueado* ou *pronto*).

**4. Faz uso de arquiteturas multiprocessador:** Os benefícios de multithreading pode ser amplamente explorado com o uso de arquiteturas multiprocessador, pois desta forma threads podem executar paralelamente em processadores distintos. Em contraposição, um processo de uma única thread só pode executar em um único processador, não importando quantos desses estejam disponíveis. Assim sendo, multithreading em computadores de múltiplos processadores aumenta a concorrência do processo.

### Malefícios

**1. Não existe proteção entre threads:** se uma thread escreve sobre o espaço de endereçamento doutra de uma maneira não prevista, os resultado disto também pode ser imprevisível.

**2. Pode haver a necessidade de sincronização entre as threads:** Se a tarefa feita por uma thread é pré-requisito de uma tarefa executada por outra thread, faz-se necessário coordenar a execução destas duas threads.

### Threads criadas no espaço do usuário

Para gerenciar múltiplas threads no espaço do usuário, cada processo possui seu próprio *runtime environment*, que contém uma tabela de threads e um escalonador destas organizando a execução destas. O sistema operacional não "enxerga" multiplas threads associadas ao processo, apenas a thread sendo executada pelo processo naquele momento (modelo N para 1).