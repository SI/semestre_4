# Sistemas operacionais: Conceitos Básicos

## Sistema computacional

> Vulgo "computador".

Objeto de pesquisa no desenvolvimento de Sistemas Operacionais (SOs). São compostos por:

- Um ou mais processadores.

- Uma memória principal.

- Dispositivos de entrada e saída como: discos, impressoras, monitores, memórias secundárias, dentre outros.

Antes da existência de sistemas operacionais os programas executados por sistemas computacionais necessitavam saber lidar com a particularidades de um conjunto destes componentes supracitados e, após o surgimento destes, essa capacidade foi delegada ao SO do computador.

## A importância de sistemas operacionais

Sistemas operacionais permitem que os programadores não necessitem se preocupar com que seus programas sejam adaptados às particularidades do hardware do computador em que estes serão executados, pois o SO se encarregará disso. Isso permite uma maior portabilidade do programa e libera o programador a dedicar-se à questões de mais alto nível (de abstração) da implementação do programa.

## Definição de SO

Programa, ou conjunto de programas interrelacionados, cuja finalidade é agir como:

- intermediário entre o usuário e o hardware;

- gerenciador de recursos.

Dada a abrangência e complexidade para se alcançar tais objetivos, este concilia objetivos contraditórios como:

- Conveniência x eficiência

- Implementação de novas funcionalidades x compatibilidade com funcionalidades antigas.

## Vantagens do uso de SOs

Um Sistemas Operacional torna o computador:

- mais adaptado para usos de propósito geral;

- gerir seus componentes de hardware de maneira mais integrada e eficiente;

- o uso compartilhado e protegido dos recursos de software e hardware de um computador para múltiplos usuários.

## Interação com o SO

Comandos podem ser repassados ao Sistema Operacional diretamente por meio da "linguagem de comando" passada para o interpretador de comandos (o *shell*) do sistema operacional por meio textual através do terminal de comando. Outra alternativa, pré-configurada, é fazê-lo por meio de uma interface gráfica (graphic shell).

Comandos invocam a execução de serviços deste por meio de "chamadas ao sistema operacional".

## Processamento

Um SO pode processar sua carga de trabalho de duas formas:

- Serial (recursos são alocados a um único programa);

- Concorrente (recursos são dinamicamente reassociados entre uma coleção de programas em diferentes estágios).