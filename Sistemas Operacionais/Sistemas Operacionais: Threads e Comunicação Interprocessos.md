# Sistemas Operacionais: Threads e Comunicação Interprocessos

## Threads no Espaço do Núcleo

São criadas, escalonadas, e gerenciadas por tabelas pelo kernel via chamadas ao sistema. Assim sendo, o kernel é capaz de "enxergar" e manipular diretamente a todas as threads criadas por processos deste tipo (modelo 1 para 1). Os algoritmos mais utilizados para o gerenciamento de threads são Round Robin e Prioridade. Não obstante a mudança de contexto entre o modo usuário e o modo núcleo feita para se manipular tais threads implique em um tempo de processamento maior do que seria necessário estivesse a thread em espaço de usuário, este custo é ainda preferível a alternativa de se fazer uma mudança de contexto entre processos.

### Benefícios

- Quando uma thread deste tipo bloqueia, o kernel pode rodar outra thread do mesmo processo ou mesmo doutro processo.

- O processo inteiro não é bloqueado se uma thread realizar uma chamada apta a ser bloqueada pelo sistema, como E/S.

### Desvantagem

- Uma thread deste tipo requer uma mudança de contexto do modo usuário para o modo núcleo para ser manipulada.

## Threads híbridas

> Modelo N para M, onde M ≤ N

É possível processos abrirem threads de ambos os tipos, melhor aproveitando seus benefícios e mitigando seus malefícios.

<img src="Imagens/2022-09-28-18-05-24-image.png" title="" alt="" data-align="center">

## Tabela comparativa dos modelos

| Modelo                                            | N:1                                                                         | 1:1                                                                | N:M                                                                                    |
| ------------------------------------------------- | --------------------------------------------------------------------------- | ------------------------------------------------------------------ | -------------------------------------------------------------------------------------- |
| Resumo                                            | Todos as $N$ threads do processo são mapeadas à uma única thread no núcleo. | Cada thread do processo possui uma thread correspondente no núcleo | Os $N$ threads do processo são mapeados em um conjunto de $M$ threads no núcleo        |
| Local da implementação                            | Blibliotecas no nível do usuário                                            | dentro do núcleo                                                   | em ambos                                                                               |
| Complexidade                                      | baixa                                                                       | média                                                              | alta                                                                                   |
| Custo de gerência para o núcleo                   | nulo                                                                        | baixo                                                              | alto                                                                                   |
| Escalabilidade                                    | alta                                                                        | baixa                                                              | alta                                                                                   |
| Suporte a vários processadores                    | não                                                                         | sim                                                                | sim                                                                                    |
| Velocidade das trocas de contexto entre *threads* | rápida                                                                      | lenta                                                              | rápida entre threads do mesmo processo, mas lenta entre threads de processos distintos |
| Divisão de recursos entre tarefas                 | injusta                                                                     | justa                                                              | variável, pois o mapeamento thread → processador é dinâmico                            |
| Exemplos                                          | GNU Portable Threads                                                        | Windows XP, Linux                                                  | Solaris, FreeBSD KSE                                                                   |

## Reciclagem de threads

Já que criar e destruir threads implicam em custos de processamento, evita-se destruí-las após o processamento destas se encerrar, mantendo-se suas estruturas no núcleo e marcando-as como estando aptas a serem reapropriadas.

Assim, quando houver a necessidade de se criar novas threads, sobrescreve-se uma thread anterior com os novos dados, reutilizando-a.

## Comunicação Interprocessos

### Condição de corrida (race condition)

> Também chamada de "condição de disputa"

Situação onde dois ou mais processos acessam recursos compartilhados concorrentemente. Não necessariamente é um problema pois, digamos, dois programas lerem um mesmo arquivo não acarreta em consequências imprevisíveis, tal qual estes escreverem em um mesmo arquivo poderia provocar.

#### Exclusão mútua e região crítica

Uma solução possível para condições de corrida é denominar certas regiões da memória como sendo "regiões críticas", e não permitir que dois processos escrevam sobre estas regiões ao mesmo tempo (um processo necessita sinalizar que terminou de fazer uso daquela região de memória antes que outro processo faça uso da mesma). Esta forma de restrição de acesso é conhecida como "exclusão mútua".

![](Imagens/2022-10-02-16-44-48-image.png)

#### Condições para (uma boa) programação concorrente

1. Dois processos nunca podem estar simultaneamente dentro de suas regiões críticas;
2. Não se pode fazer suposições em relação à velocidade e ao número de CPUs;
3. Um processo fora da região crítica não deve causar bloqueio a outro processo;
4. Um processo não pode esperar eternamente para entrar em sua região crítica.

#### Soluções de exclusão mútua

##### Desabilitar interrupções

Onde cada processo desabilita a todas as interrupções (inclusive a do relógio) ao entrar na região crítica e as habilita novamente antes de deixá-la. Esta é uma solução bastante problemática, pois

- em sistemas com várias CPUs, desabilitar interrupções em uma delas não evita que outras acessem a memória compartilhada. Isso consiste em uma violação das condições 1 e 2.

- Um processo pode falhar em reabilitar as interrupções e não ser finalizado, o que consiste em uma violação da condição 4.

Contudo, é útil, para o kernel, ter este poder de se desabilitar interrupções, para sua própria manutenção; por exemplo, quando este atualiza a lista de processos prontos, pois uma interrupção poderia deixá-la em estado inconsistente

##### Espera ocupada

###### Lock variable

Consiste na constante checagem de uma variável denominada *lock*, que indica se uma região crítica está sendo acessada por algum processo. Após o valor equivalente à disponíbilidade da região crítica aparecer, o processo altera o valor desta variável e faz uso do recurso, alterando novamente o valor da variável ao terminar. Esta é uma solução problemática, se for possível trocar o processo em execução antes que o processo anterior tenha alterado o valor da variável de trava depois de acessar a região crítica. Assim, violando a condição 1. 

###### Strict alternation

Similar à solução anterior, esta consiste na constante checagem de uma variável denominada *turn*, a qual indica o processo aquele que possui acesso a região crítica em determinado momento. Se a variável turn possui valor correspondente à identificação do atual processo, este faz uso da região crítica e, ao terminar de fazê-lo, altera o valor de *turn* para um que identifique outro processo em execução. Desta forma, processos alternam entre si o acesso à uma região crítica.

###### Solução de Peterson

A solução de Peterson trata-se de uma conjunção das duas soluções anteriores, e funciona apenas entre dois processos buscando acessar a mesma região crítica. Quando um processo deseja adentrar uma região crítica, este atribui um valor verdadeiro a uma variável *flag*, a ele associada e atribui o valor do identificador do processo **seguinte** a desejar entrar na região crítica para a variável *turn*. Ou seja, este sinaliza necessidade de adentrar a região crítica, mas oferece a oportunidade de fazê-lo a outro processo que também o necessite. O processo seguinte reciprocamente realiza este mesmo procedimento, de tal forma que os processos acessam a região crítica em ordem inversa à passagem do acesso.

###### Instrução TSL (Test and Set Lock)

Instrução em Assembly, da forma TSL RX, LOCK (voltar a isto)

##### Primitivas Sleep/Wakeup

Tratam-se, respectivamente, de chamadas ao sistema para o bloqueio e desbloqueio de processos, implementadas via traps. Assim, precede a necessidade de uma espera ocupada.

##### Semáforos

Variável inteira utilizada para controlar o acesso a recursos compartilhados, sincronizando o uso destes ainda que estes se encontrem em grande quantidade. Tem-se,

- Semáforo = 0 → não há recurso livre, pois nenhum wakeup está armazenado.

- Semáforo > 0 → há recurso livre, pois um ou mais wakeups estão pendentes.

- os comandos Down(semáforo) ou Wait(Semáforo), executados sempre que um processo deseja usar um recurso compartilhado, produzem:
  
  - verifica-se se o valor de semáforo é maior que 0;
  
  - se sim, decrementa-se o valor em uma unidade;
  
  - se depois disso o valor do semáforo for 0, o processo que invocou o uso do semáforo é posto para "dormir".

- os comandos Up (semáforo) ou Signal (semáforo), executados sempre que um processo liberar um recurso, produzem:
  
  - um incremento de uma unidade no semáforo ou
  
  - se houverem processos dormindo em função deste semáforo, escolhe-se um destes e o "acorda", caso em que o valor do semáforo permanece o mesmo.

- Todas as modificações ao valor inteiro do semáforo por meio dos comandos supracitados são executados indivisivelmente. Isto é, quando um processo modifica o valor do semáforo, nenhum outro processo pode modificá-lo simultaneamente.

###### Semáforo binário

Um semáforo cujo valor varia apenas entre 0 e 1. Em alguns sistemas estes são designados como travas *mutex*, pois estes funcionam enquanto travas que, entre dois processos, resolvem o problema da **exclusão mutua**.

###### Semáforo de contagem

Um valor que pode se estender sobre um domínio infinito. Este é utilizado como forma de controle de acesso a um recurso à múltiplas instâncias.

###### Problema do Produtor/Consumidor (bounded buffer problem)

Este problema é explicado no seguinte [vídeo](https://youtu.be/Qx3P2wazwI0).

##### Monitores

Rotinas as quais dois ou mais processos não podem acessar concorrentemente. Após um monitor ser acessado por um processo, quaisquer outros que tentarem acessá-lo serão bloqueados até que o processo o acessando atualmente o deixe. Monitores podem implementar *variáveis de condição*, ou seja, testes os quais, se não satisfeitos, bloqueiam o processo (operação *wait*) e, senão, permitem a execução ou retomam a execução deste (operação *signal*).

##### Troca de mensagens

## Deadlock

Quando um conjunto de processos está reciprocamente aguardando o retorno um do outro e, por isso, encontram-se paralisados. Um exemplo da ocorrência disto é quando um processo $B$ de baixíssima prioridade cede seu processamento após adentrar a região crítica compartilhada com um processo $A$ de altíssima prioridade. Para terminar a execução de $A$ o escalonador haverá de percorrer os processos na lista de prioridades até chegar novamente ao $B$ para que este possa sair da região crítica, somente então a execução poderá ser retornada em $A$.

## Problemas clássicos de comunicação interprocessos



[^1]: KSE ‒ Kernel Scheduled Entities