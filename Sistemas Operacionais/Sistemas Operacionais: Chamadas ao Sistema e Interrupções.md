# Sistemas Operacionais: Chamadas ao Sistema e Interrupções

Chamadas ao sistema, ou **instruções TRAP**, são solicitações feitas por processos que buscam realizar alguma instrução privilegiada, isto é, a qual só pode ser executada no *modo kernel*. Ao se fazer uma chamada ao sistema, a execução do processo é interrompida e o controle sobre o processador é transferido de volta ao SO que executa, ou não (caso não seja aceita), a instrução requerida, retornando o controle ao processo em seguida.

TRAPs podem ser resultado do funcionamento normal de um processo ou de uma exceção no funcionamento deste que requer o tratamento do SO, tais quais a divisão por zero, ou um acesso inválido à memória. Usualmente, instruções TRAP são feitas de maneira que o processo obtenha acesso à porções do hardware, notadamente os dispositivos de entrada e saída.

No mais, TRAPs requerem que o processo tenha salvo seu estado de execução em registradores, de tal forma que este possa ser restaurado após a chamada houver sido terminada (no mínimo, é salvo o valor do contador de programa). Ou seja, toda chamada ao sistema implica em um custo adicional de acesso à memória.

## Procedimento de execução de uma interrupção TRAP

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-18-11-15-30-image.png)

**a.** O processo faz a chamada a chamada ao sistema, cedendo o controle sobre o processador ao SO, que executa em *modo kernel*.

**b.** O SO recebe a chamada a qual identifica, por um índice, uma rotina de serviço dentre uma lista de opções contidas em uma tabela pelo sistema operacional.

**c.** a rotina de serviço é executada.

**d.** O controle do processador é devolvido ao processo, em modo usuário.

No passo **c** assume-se que não haveriam outras rotinas de serviço a serem executadas naquele momento. Na grande maioria dos casos, entretanto, existem outras chamadas a serem processadas, as quias são empilhadas em uma **pilha de execução** (*dispatcher*) do sistema operacional para serem processadas conforme a disponibilidade do sistema e a prioridade das chamadas. Nalguns casos, é possível que a chamada permaneça na pilha por algum tempo sem ser processada, pois está aguardando uma resposta do hardware (seja input do teclado, ou uma leitura da memória secundária). Quando isso ocorre, o SO segue para resolver outras chamadas na pilha até que um sinal de interrupção o chame de volta a chamada em questão.

### Instrução exit();

Havendo a necessidade de se transferir o controle ao SO para que este volte a ter controle sobre o processador, processos necessitam ser terminados com uma instrução TRAP invocando o controle do sistema operacional. Não sendo feito isso, corre-se o risco de que na pilha de execução haja uma instrução seguinte válida para ser executada, mas que seja de fora do contexto do programa, levando a resultados inesperados.

## Wrapper API

SO distintos possuem diferentes interpretações às chamadas que lhes são feitas. Assim sendo, wrappers são bibliotecas as quais mapeiam chamadas ao sistema de maneira agnóstica ao SO, tal que o programa funcione com SO diferentes. As chamadas feitas ao wapper são traduzidas de tal forma a serem corretamente interpretadas pelo SO em questão com o qual o processo se comunica. São exemplos de Wrappers: Win32, POSIX, e JVM (Java Virtual Machine).

## Interrupções por hardware

Uma interrupção por hardware é uma condição relacionada ao estado do hardware que pode ser sinalizada por um aparelho externo ao processador, por exemplo, uma solicitação de interrupção por um modem, ou interno a este, por exemplo, um [watchdog timer](https://en.wikipedia.org/wiki/Watchdog_timer). Não obstante, estas interrupções restituem o controle do SO sobre o processador, que então decide como se deve proceder.