# Sistemas Operacionais: Escalonamento de Processos

## Mudança de Contexto

Denominação dada à mudança de execução de um processo por outro na CPU, buscando maximizar o aproveitamento da CPU na ocorrência de processos ociosos. Nem sempre a mudança de processo é justificável pois o processo de mudança em si consome recursos:

- Necessita-se salvar as informações do processo em execução em seu respectivo BCP;

- Sobrescreve-se os registradores com as informações do processo que será colocado na CPU.

Por isso tal recurso necessita ser utilizado com parcimônia. Não obstante, este processo é feito toda vez que se interrompe a CPU.

![](Imagens/2022-09-20-20-24-26-image.png)

> Passos de uma mudança de contexto

## Despachante (Dispatcher)

- Armazena e recupera o contexto;

- Mantém atualizadas as informações no BCP;

- Opera de maneira relativamente rápida (0,1 ms para operar).

## Escalonador (Scheduler)

- Escolhe a próxima tarefa a ser executada pelo processador;

- Opera significativamente mais lentamente que o Dispatcher.

- Algoritmos podem ser preemptivos ou não preemptivos, quando fazem, ou não, provisão para serem interrompidos e transferir o controle ao escalonador.

### Situações nas quais escalonamento é necessário

- **Quando um novo processo é criado:** determina-se se se deve dar continuidade ao atual processo ou interrompe-lo para executar o novo processo.

- **Quando um processo termina:** havendo processos prontos a serem executados, o controle do processador é transferido a um destes.

- **Quando um processo é bloqueado:** o que ocorre quando há uma dependência de uma resposta por parte de um dispositivo de E/S. O processo bloqueado e então colocado em uma fila específica à processos bloqueados.

- **Quando uma interrupção de E/S ocorre:** o escalonador então deve decidir entre continuar executando o processo atual, ou retomar aquele que estava aguardando o retorno da E/S, ou senão outro.

## Algoritmos de escalonamento

São objetivos comuns a todos os sistemas:

- **Justiça (Fairness):** cada processos deve receber uma parcela justa de tempo da CPU, processos de mesma categoria devem receber a mesma fatia de tempo;

- **Balanceamento:** deve-se diminuir a ociosidade do sistema tanto quanto possível;

- Finalmente, deve-se **respeitar as políticas do sistema**, em termos da prioridade dos processos.

### São algoritmos de escalonamento para sistemas *Batch*

- **First-Come First-served (ou FIFO):** Processos são executados pela CPU seguindo a ordem de requisição. Neste
  
  - há apenas uma fila de processos prontos;
  
  - cada processo roda o tempo que quiser;
  
  - novos processos entram no final da lista;
  
  - se um processo é bloqueado, o primeiro na fila é o próximo a executar;
  
  - quando um processo bloqueado fica pronto, é recolocado no final da fila.

- **Shortest Job First (SJF):** Menor processo da fila de processos prontos é executado primeiro.
  
  - Não-preemptivo.
  
  - Supõe que se sabe de antemão o tempo de execução de todos os processos.
  
  - Se algum processo for bloqueado por E/S este voltará a fila de processos bloqueados ordenado.
  
  - Minimiza-se o *turnaround* médio se comparado ao FIFO.

- **Shortest Remaining Time Next (SRTN):**
  
  - Versão preemptiva do (SJF);
  
  - Processos com menor *tempo restante* são executados primeiro.

### São algoritmos de escalonamento para sistemas interativos

- **Round-Robin (alternancia circular, ou revezamento):** algoritmo preemptivo em que cada processo recebe uma fatia de tempo igual denominado "quantum". Os processos são então colocados em uma fila circular (de prontos) e executados em ordem. Quando o quantum relativo a um processo é esgotado, passa-se a executar o próximo processo.

<img src="Imagens/2022-09-21-14-25-07-image.png" title="" alt="" data-align="center">

- **Prioridade:** algoritmo preemptivo onde cada processo possui uma prioridade, e os processos prontos com maior prioridade são executados primeiro. A prioridade de um processo pode ser atribuída estáticamente (pelo usuário) ou dinamicamente (pelo sistema, por exemplo, decrementando a prioridade do processo em função do tempo).

![](Imagens/2022-09-21-14-46-41-image.png)

> Neste exemplo, quanto menor o tempo de execução, maior a prioridade.

**Obs.:** Quando múltiplos processos possuem mesma prioridade, o escalonamento entre estes pode ser administrado utilizando-se o algoritmo round-robin.

- **Múltiplas filas:** algoritmo preemptivo, uma variação da classe de prioridades. Nesta,
  
  - cada classe de prioridades tem quantas diferentes;
  
  - se um processo usar todos os quanta alocados a ele, este é rebaixado de classe;
  
  - a última classe roda com Round Robin;

- **Shortest Process Next:** algoritmo preemptivo, executa a tarefa mais curta primeiro, de acordo com uma estimativa do tempo de execução desta.

- **Garantido:** Para $n$ processos a serem executados, garante-se que o processador destinará $\frac 1n$ do seu tempo de execução para cada um deles.

- **Lottery:** Sorteiam-se fatias de tempo de execução entre os processos prontos.

- **Fair-share:** P tempo de CPU é alocado de acordo com o usuário que possui "*owns*" o processo.

**Obs.:** Não necessáriamente $\frac 1n$ do tempo para cada, se o usuário A detém 9 processos e o usuário B detém 1, e estes correspondem a totalidade dos processos, o usuário A controlará em 90% do tempo a CPU.

### Um sistema é escalonável?

Para que um sistema seja escalonável, a somatória das frações da CPU não podem ultrapassar de 1, ou seja:

$$
\sum^n_i \frac{s_i}{p_i} \leq 1
$$

Sendo,

- $n$ o número de processos concorrentes;

- $s_i$ a duração de um surto de execução do processo $i$;

- $p_i$ o período de recorrência do processo $i$.