# Sistemas Operacionais: Processos e Escalonamento

## Processo

Denominação dada a qualquer conjunto de programa, ou programas, de computador em execução **e** os dados em memória necessários a funcionamento deste, ou destes. Isto é:

- O programa ou programas;

- Um espaço de endereçamento: porções da memória sobre as quais os processos podem ler e escrever;

- Contextos de hardware: program counter, ponteiro de pilha, registradores em uso;

- Contextos de software: variáveis, listas de arquivos abertos, processos relacionados, dentre outras informações acessórias.

Em um processo podem haver programas em diversos estados (como executando, bloqueado e pronto). Um mesmo programa pode ser executado em processos distintos, entretanto, cada processo é **único**. Processos são criados à partir da inicialização do sistema ou por uma chamada ao sistema feita por outros processos.

**Processos em primeiro plano:** os processos aqueles com os quais o usuário interage ou pode interagir.

**Processos em segundo plano (daemons):** os processos aqueles que são executados de maneira independente da interação dos usuários.

### Espaço de endereçamento

Possui três segmentos:

- **Texto:** o código executável do(s) programa(s);

- **Dados:** as variáveis;

- **Pilha de Execução:** Estrutura que armazena os dados de rotina chamada que ainda não retornou. Contém:
  
  - As variáveis locais e os parâmetros da rotina;
  
  - O endereço de retorno a quem a chamou.

### Contexto

Todas as informações necessárias para que um processo, após ter sido suspenso, possa voltar a ser executado do ponto em que parou.

### Hierarquia de processos

Em sistemas baseados em Unix, todos os processos descendem do processo init, formando uma árvore de hierarquia de processos. O estado atual desta árvore pode ser conferido pelo terminal com o uso do comando `pstree`.

### Término de processos

Um processo pode ser encerrado de duas formas:

- **Voluntáriamente:** O processo faz uma chamada exit (UNIX) ou ExitProcess (Windows), seja porque terminou de realizar sua tarefa ou encontrou um erro de execução o qual não é possível de ser tratado.

- **Involuntáriamente:** O processo é interrompido via sinal do SO, seja porque este ocasionou um erro fatal (fez divisão por zero, ou referência a memória inexistente, ou executou uma instrução ilegal); ou o usuário optou por terminá-lo via processos como Kill (UNIX), TerminateProcess (Windows), ou qualquer outro que tenha permissão para terminá-lo.

### Estado do processo

Processos possuem três estados básicos:

- **executando:** realmente usando a CPU naquele dado momento;

- **pronto:** em memória, aguardando apenas a disponibilidade da CPU para ser executado.

- **bloqueado:** em memória, mas, por qualquer outra razão, é incapaz de ser executado enquanto uma dada condição não ocorrer.

Existe também um quarto estado, potencialmente problemático:

- **zumbi:** encontra-se em memória mas não é referenciado por qualquer outro processo. Ocorre quando um processo filho perde sua ligação com o processo pai que o criou.

### Tabela de processos

Todo controle de processos é feito por uma tabela ‒ usualmente implementada como um *array* ‒ a **tabela de processos**. Nesta,

- cada processo constitui uma entrada;

- cada entrada possui um ponteiro para um Bloco de Controle de Processo (BCP) ou descritor do processo. O BCP possui todas as informações do processo que são necessárias a reiniciá-lo a partir de um dado ponto de sua execução, seja este interrompido.

### Criação de processos

Para criar um processo, o SO executa os seguintes passos:

- Atribui-se um identificador único ao processo (PID);

- Aloca-se uma entrada na tabela de processos;

- Aloca-se espaço para o processo na memória;

- Inicializa-se o respectivo BCP;

- Coloca-se o endereço do BCP na fila apropriada (fila de processos prontos ou bloqueados);

- Cria-se estruturas auxiliares.

## Escalonador

Processo do nível mais baixo do sistema SO que, junto com o *dispatcher*, realiza todo o tratamento de interrupções; efetivamente decidindo o próximo processo a ser executado. Por vezes, entretanto, esta função é realizada diretamente pelo *dispatcher*. Existem diversas técnicas/algoritmos para realização do escalonamento de processos, os quais são denominados a *política* adotada pelo escalonador.