## Resolução da lista 2

> Da disciplina de Sistemas Operacionais

**1.** Ecalonamentos preemptivos são aqueles que permitem a mudança de contexto entre processos em meio a execução destes, ao salvarem e restaurarem, em registradores, todas as informações pertinentes a descrição do estado atual destes. Escalonamentos não-preemptivos, por outro lado, não fazem tal provisão; os processos por estes escalonados necessitam ser terminados para que este decida sobre a mudança de contexto a ser realizada.

**2.** Tendo em vista que para qualquer porção de tempo dado processo neste esquema teria alocado para este a $n$ésima fração de tempo de execução, onde $n$ é o número de processos prontos, seja $T$ o tempo de execução deste processo, fosse este executado em separado, $n \cdot T$ é o tempo de execução total deste enquanto este houver sendo executado em paralelo a outros $n$ processos. $\blacksquare$

**3.** Dado um intervalo de tempo suficientemente grande, isso implicaria que a este processo seria alocado o dobro do tempo de execução que todos os demais processos listados, os quais ocorrem na lista uma única vez. Uma razão para se alocar recursos desta forma seria para, em um esquema *round robin*, criar uma distinção de prioridade para a execução de determinados processos.

**4.** Seja $E$ a eficiência em termos da razão entre o tempo de execução útil e total, respectivamente.

**a.** Na situação em que $Q > T$ o tempo de execução total não é afetado por sucessivos chaveamentos, mas um único chaveamento ao final da execução do processo. Tido que $T$ é finito e $Q$ não o é, temos:

$$
E = \frac T{T + S}
$$

**b.** Novamente, o tempo de execução total não é afetado enquanto $Q > T$, então:

$$
E = \frac T{T + S}
$$

**c.** De modo geral, com $Q < T$, o sistema fará $\lceil \frac TQ \rceil$ trocas ao executar um processo por $T$ unidades de tempo. Assim, para rodar $T$, o sistema gastará $\lceil \frac TQ \rceil \cdot S$ unidades de tempo com chaveamento e a eficiência resultante será:

$$
E = \frac T{T + \lceil \frac TQ \rceil \cdot S}
$$

**d.** Construindo sobre a fórmula anterior:

$$
E = \frac T{T + \lceil \frac TQ \rceil \cdot S} =
\frac T{T + \lceil \frac TS \rceil \cdot S} = \frac{T}{2T} = \frac 12
$$

**e.**

$$
\lim_{Q \to 0} \frac T{T + \lceil \frac TQ \rceil \cdot S}
= \frac T{T + \infty} = 0
$$

**5.** Para tal, executamos os processos na ordem do menor ao maior tempo de execução previsto: 3, 5, 6 e 9, com X sendo colocado na posição adequada.

**6.** Seja $T(X)$ o tempo de execução total do processo $X$.

**a.** Sejam os processos executados em esquema *round robin* na ordem das prioridades a estes designadas, com um quanta $Q$ equivalente ao $mdc$ dos tempos de execução previstos para estes, ou seja, $Q = 2 \min$. Temos:

| Rodada | B   | E   | A   | C   | D   |
| ------ | --- | --- | --- | --- | --- |
| 0      | 6   | 8   | 10  | 2   | 8   |
| 1ª     | 4   | 6   | 8   | 0   | 2   |
| 2ª     | 2   | 4   | 6   |     | 0   |
| 3ª     | 0   | 2   | 4   |     |     |
| 4ª     |     | 0   | 2   |     |     |
| 5ª     |     |     | 0   |     |     |

O tempo de retorno médio fica:

$$
\frac{T(A) + T(B) + T(C) + T(D) + T(E)}5 =
\frac{40 + 22 + 18 + 28 + 36}5\\\ \\ = 28.8 \min = 28 \min 48 \sec
$$

**b.**

$$
\frac{6 + (6 + 8) + (6 + 8 + 10) + (6 + 8 + 10 + 2) +
(6 + 8 + 10 + 2 + 4)}5 \\\ \\=  20 \min
$$

**c.**

$$
\dfrac{10 + (10 + 6) + (10 + 6 + 2) + (10 + 6 + 2 + 4)
+ (10 + 6 + 2 + 4 + 8)}5 \\\ \\= 19 \min 12 \sec
$$

**d.**

$$
\frac{2 \cdot 5 + 4 \cdot 4 + 6 \cdot 3 + 8 \cdot 2 + 10}5 =
14 \min
$$

**7.** Seja $Q$ o número de vezes em que a fita será escalonada (e portanto "posta para rodar"), temos:

$$
Q = \lceil \log_2 30\rceil \approx \lceil 4.906\rceil = 5
$$

**8.** Para que um sistema seja escalonável, a somatória das frações da CPU não podem ultrapassar de 1, ou seja:

$$
\sum^n_i \frac{s_i}{p_i} \leq 1
$$

Sendo,

- $n$ o número de processos concorrentes;

- $s_i$ a duração de um surto de execução do processo $i$;

- $p_i$ o período de recorrência do processo $i$.

Logo, se buscamos encontrar o valor máximo para $x$, temos que:

$$
\frac{35}{50} + \frac{20}{100} + \frac{10}{200} + \frac{x}{250}
= 1 \implies x = 12,5 \text{ ms } \blacksquare
$$

**9.** 

Vide caderno.