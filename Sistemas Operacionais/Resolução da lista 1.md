# Resolução da lista 1

> Da disciplina de Sistemas Operacionais

**1.** A capacidade que determinados Sistemas Operacionais têm para manter mais de um processo em execução simultânea na memória principal, por meio do compartilhamento efetivo entre esses de tempo na CPU entre outros recursos.

**2.** Respondendo as perguntas uma a uma, temos:

- Se admitirmos que cada caractere pode assumir um valor descrito por um byte (tal qual descrito pelo padrão ASCII), esta necessitaria fazer uso de 2000 B de memória.

- 1024 ⨉ 768 ⨉ 24 = 18.874.368 bits = 2.359.393 B

- Sendo kB = 1024 B, tem-se, respectivamente, 2000 / 1024 ⨉ 5 = US$ 9,76 e  2.359.393 / 1024 ⨉ 5 =  US\$ 11.520,47.

**3.** O modo núcleo é o modo de execução aquele que permite ao processo realizar operações que, normalmente, são capazes de interferir com a segurança ou integridade do sistema. Portanto,

**a.** Desabilitar interrupções é uma operação capaz de interferir com o funcionamento esperado do sistema, e portanto **necessita** ser realizado em modo núcleo.

**b.** Ler o horário do relógio não interfere com o funcionamento normal do sistema, e, portanto, **não necessita** ser executado no modo núcleo.

**c.** Alterar o horário do relógio interfere com o funcionamento normal do sistema pois podem haver múltiplos processos a ser executados periodicamente (*scheduling*) e, portanto, **necessita** ser executado no modo núcleo.

**d.** Como se pode imaginar, **necessita** ser executado em modo núcleo.

**4.** A cada 1 ns, uma nova instrução é iniciada e, desde 4 ns segundos após o início da operação, uma instrução se encerra a cada 1ns. Logo, em média, cada segundo são concluídas 999.999.998,5 instruções.

**5.** Número total de caracteres do texto: 700 ⨉ 50 ⨉ 80 = 28.000.000 caracteres.

**Memória interna**

> Cada caractere constitui um acesso à memória.

- **Cache:** cada acesso toma 2ns, o tempo de acesso para todo o texto é 2.800.00 ⨉ 2 ns = 5.600.000 ns = *5,6 ms*.
- **RAM:** 10 ns por acesso, 2.800.000,00 ⨉ 10 = 28.000.000 ns = *28 ms*.

**Memória externa**

- **HD:** acesso feito em blocos de 1024 caracteres. Então, tem-se ⌈2.800.000 ÷ 1024⌉ = 2.735. Cada bloco necessita de 10ms para ser acessado, então: 2.735 ⨉ 10 ms = 27.350 ms = *27,35 s*.

- **Fita:** A busca ao início dos dados leva 100 s e acessos subsequentes a blocos de memória se igualam ao HD, logo: 100 s + 27,35 s = 127,35 s = *2 min e 7,35 s*.

**6.** Quaisquer programas que façam uma chamada ao SO para realizar um acesso ao disco em uma posição ≥ posição do ponteiro e < tamanho do buffer de dados necessitam ser bloqueados até que a escrita no disco termine.

**7.** Chamadas ao sistema, ou **instruções TRAP**, são solicitações feitas por **processos** que buscam realizar alguma instrução privilegiada, isto é, a qual só pode ser executada no *modo kernel*. Ao se fazer uma chamada ao sistema, a execução do processo é interrompida e o controle sobre o processador é transferido de volta ao SO que executa, ou não (caso não seja aceita), a instrução requerida, retornando o controle ao processo em seguida.

Interrupções, por outro lado, são uma condição relacionada ao estado do **hardware** que pode ser sinalizada por um aparelho externo ao processador, por exemplo, uma solicitação de interrupção por um modem, ou interno a este, por exemplo, um [watchdog timer](https://en.wikipedia.org/wiki/Watchdog_timer). Não obstante, estas interrupções restituem o controle do SO sobre o processador, que então decide como se deve proceder.

Então, em suma, em ambos os casos, o controle é restituído ao SO, mas em um caso por um processo e noutro por um sinal do hardware.

**8.** Um exemplo seriam interrupções provocadas por periféricos como o teclado, o mouse ou touchpad. O uso destes envia um sinal de interrupção ao processador, capturado pelo Sistema Operacional, que então lê a tecla pressionada ou posição do cursor registrada e armazena-os nos registradores adequados.

**9.** Pois sistemas operacionais de tempo compartilhado são SOs os quais são destinados ao uso iterativo do usuário, ou usuários. Assim sendo, estes SOs necessitam administrar processos os quais podem ser executados de maneira intermitente à revelia do usuário, assim como processos aqueles os quais necessitam da iteração do usuário e outros que não necessitam desta (daemons), intercaladamente. Assim sendo, o controle da execução desses processos armazenados em memória principal e que podem ou não estar sendo executados no processador em determinado momento é feito por meio da tabela de processos.

Por outro lado, por estas mesmas razões, SOs aqueles em que existe apenas um processo em execução no processador sem que haja a necessidade de se interrompê-lo até seu término, não necessitam fazer uso de uma tabela de processos.

**10.** Tal qual explicado na pergunta 7, chamadas ao sistemas são solicitações feitas por processos os quais buscam realizar alguma instrução privilegiada, isto é, que só pode ser executada em *modo kernel*. O SO administra o acesso ao modo kernel, de maneira a preservar a integridade e segurança do sistema como um todo.

**11.** É importante saber quando uma chamada é direcionada ao sistema pois chamadas deste tipo implicam em uma **mudança de contexto** do *modo usuário* ao *modo kernel*. Este é um procedimento custoso pois faz necessário salvar em registradores todas as informações pertinentes ao atual estado do processo em modo usuário para depois passar ao modo kernel, retornar ao modo usuário e recuperar o estado salvo. Se possível, é preferível evitar este processo de gravação e regravação sobre os registradores.

**12.** O risco, quase que certo, é de que se este programa fazer uso de quaisquer chamadas ao sistema este não será compatível para funcionamento no Windows. Assim sendo, este programador necessita ou

- lançar mãos de chamadas ao sistema, fazendo uso de máquinas virtuais como aquela vista na linguagem Java, a JVM;

- fazer uso de bibliotecas que adequem a chamada ao sistema presente no código ao sistema em que este está sendo executado. Um exemplo de biblioteca deste tipo seria a Wine, para converter chamadas ao sistema do Windows em chamadas POSIX-compliant utilizadas em Linux entre outros sistemas baseados em Unix.

**13.** A tabela de processos permite que os processos nela listados sejam interrompidos pelo sistema operacional para este dedicar o processador à leitura de sinais de entrada ou saída e, depois, recuperar os processos tal qual estes se encontravam. Os processos então podem fazer uso dos dados armazenados em registradores advindos de tais dispositivos, o que confere uma percepção de responsividade entre os comandos passados pelos dispositivos e o programa sendo executado.

**14.** Não.

Primeiramente, processos são eles próprios responsáveis por bloquearem-se, estando estes apossados do controle sobre a CPU. E um processo em estado pronto não se encontra em execução, mas armazenado em memória primária. Então, este não dispõe dos meios para bloquear-se, e por isso não pode transitar do estado de prontidão para bloqueado.

Segundamente, um processo bloqueado não pode transitar diretamente do estado bloqueado para o estado em execução, pois este necessita ter seu contexto recuperado pelo *dispatcher*, que assim o faz quando o *escalonador* decide um processo pronto a ser executado em seguida.

**15.** o processo foi criado [*foi do estado 8 ao 3*] e iniciou a execução de instruções comuns (toda instrução que não corresponde a uma chamada de sistema) [*foi do estado 3 ao 2 e então ao 1*], sem deixar a CPU até a ocorrência de uma requisição de E/S [*passa para o estado 2*], a qual demanda um tempo 'longo' para ser atendida [*esta permanece bloqueada no estado 4 até que a requisição obtenha um retorno*]. Uma vez atendida esta requisição [*o processo passa para o estado 3*], o processo voltou imediatamente a executar instruções comuns [*após ter sido escalonado passando ao estado 2 e então retornado à 1*], até liberar a CPU para um outro processo [*passa ao estado 2 e em seguida 7*]. Ao ganhar a CPU novamente [*retorna ao estado 1*], o processo prosseguiu executando instruções comuns até o seu término [*passa ao estado 2 e então 9*].

**16.** O contador de programa mais a PSW (*Program Status Word*) ocupam cada qual o espaço de memória de um registrador, somados aos 32 registradores utilizados pelo programa, é necessário gravar 34 registradores para ter o contexto deste restaurado. Assim o sendo, o tempo para serem gravados e regravados estes registradores a cada interrupção equivale à 2 ⨉ (32 + 1 + 1) ⨉ 10 ns = *680 ns*. Considerando que nada mais seja feito após a interrupção senão proceder a restaurar o contexto do processo previamente interrompido, tem-se que esta máquina é capaz de processar até 10^9^ / 680 ns ≅ 1, 47 ⨉ 10^6^ interrupções por segundo.

**17.** Um pacote tem 1024B = 1024 ⨉ 8 8192 b de tamanho. Temos que seu tempo de transmissão à 10 Mb/s = 10 ⨉ 10^6^ b/s = 10^7^ b/s será 8192/ 10^7^ = 0,0008192s = 0,819 ms.

No mais, temos:

- Chamada ao SO para envio (interrupção): 1 ms

- Cópia ao buffer do SO: 1,024 ms

- Atraso de rede: 1 µs = 10^-3^ ms

- Interrupção no destino: 1 ms

- Cópia ao SO de destino: 1024 µs

- Cópia ao programa de destino: 1024 µs

Somando-se estes valores temos 6,126 ms para o envio de um pacote de 1024 B, ou seja 1024 / 6,916 ⨉ 10^3^ = 148.062,46 B/s. ■