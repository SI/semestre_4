# Sistemas Operacionais: Tipos e Estrutura de um SO

## Em relação ao compartilhamento de hardware

- **Monoprogramado, ou multitarefa:** Permite apenas que um programa seja executado por vez, o qual permanece na memória até seu término. Ex.: DOS.

- **Multiprogramado, ou multitarefa:** Mantém mais de um programa simultaneamente na memória principal, o que permite o compartilhamento efetivo do tempo da CPU e demais recursos. Enquanto um programa roda em um processador, outro aguarda um intervalo na execução para ser executado. Um sistema operacional deste tipo requer um mecanismo de troca rápida de operações. Ex.: Unix, VMS, Windows, etc.
  
  - A memória é particionada de tal forma que um serviço ocupe cada partição;
  
  - Mantém-se na memória uma quantidade de serviços suficiente para maximizar o uso do processador, desta forma reduzindo sua ociosidade. Em termos históricos, inicialmente, era o hardware responsável por proteger cada serviço de um acesso à memória indevido por parte doutro serviço;
  
  - Possibilita que um mesmo computador seja utilizado por múltiplos usuários (sistema multiusuário).
  
  - Nesta configuração, tarefas podem existir em 5 estados:

![](Imagens/2022-09-14-18-17-22-image.png)

Quando dois ou mais processos encontram-se no estado "pronto" o SO decidirá qual processo será executado em seguida. Denomina-se esta prática de "escalonamento".

**Pseudoparalelismo:** Quando múltiplos programas são executados alternadamente.

## Em relação à interação permitida

Classificados em função do tempo de resposta

- **Sistemas em lote (Batch):** computador aquele em que programas são executados em sequência, de uma só vez. Não há interação com o usuário até que a execução do lote seja concluída.

- **Sistemas de Tempo Compartilhado (Interativo):** Permitem que os usuários interajam com suas computações.

- **Sistemas de Tempo Real:** Executam aplicações que atendem a processos externos, e que possuem tempos de resposta limitados ‒ tais quais linhas de montagem, controle de caldeiras. Têm sinais de interrupção comandar a atenção do sistema.
  
  - **Sistemas de tempo real crítico (Hard real time):** As ações necessitam ocorrer em um determinado instante ou intervalo de tempo ‒ atrasos não são tolerados. Por exemplo: o piloto automático de aviões.
  
  - **Sistemas de tempo real não crítico (Soft real time):** O descumprimento ocasional de algum prazo, embora indesejável, é tolerável. Por exemplo: sistemas de áudio ou multimídia, transações em bancos.

## Em relação à estrutura interna

- **Monolítica:** Forma mais primitiva e comum de SO. Neste, quaisquer procedimento pode invocar a qualquer outro processo, e estes são todos executados no espaço do núcleo (*kernel*). Ex.: MS-DOS, Windows, Unix. Esta configuração apresenta bom desempenho mas é de mais difícil manutenção (é considerada uma "grande bagunça").
  
  - **Sistema de camadas:** sistema para a organização do sistema operacional monolítico em níveis de abstração (módulos): a cada novo nível de abstração, os detalhes da operação de níveis precedentes podem ser ignorados, confiando-se no funcionamento dos objetos e operações por estes fornecidos.

- **Micro-núcleo:** Busca tornar o núcleo do SO o menor possível, os serviços são disponibilizados em processos no mono usuário. A principal função do núcleo, assim, se reduz a gerenciar a comunicação entre esses processos (IPC, *Inter-Process Communication*). Ex.: Minix, Symbian.
  
  - **Prós**
    
    - **Maior proteção do núcleo:** todos os processos são executados em modo usuário.
    
    - **Alta disponibilidade:** se um serviço falhar, o sistema não ficará altamente comprometido.
    
    - **Maior eficiência:** a comunicação entre serviços pode ser realizada entre vários processadores ou até mesmo várias máquinas distribuídas.
    
    - **Melhor confiabilidade e escalabilidade**
  
  - **Contras**
    
    - Maior complexidade de implementação e menor desempenho em função da necessidade de mudança de modo de acesso (dentre os dois tipos que serão vistos à seguir).

- **Máquina virtual**

- **Cliente-servidor:** Uma variação da ideia de micro núcleo, implementa o máximo do SO como processos do Usuário (processos cliente).

## Modos de acesso ao hardware

São: **modo usuário** e **modo *kernel*** (supervisor ou núcleo). Quando o processador trabalha no modo usuário. aplicações só podem executar instruções sem privilégios (um subconjunto das instruções da máquina). Desta forma, busca-se garantir a integridade e a segurança dos sistema. Havendo falha,  apenas a execução da aplicação problemática será encerrado, o sistema como um todo não será afetado.