# Conflitos Bélicos: Diagrama ER+

> Resposta a primeira parte do trabalho semestral da disciplina de Banco de Dados 1

## Autores

| Aluno                              | nUSP     |
| ---------------------------------- | -------- |
| Cesar Billalta Yamasato            | 12542992 |
| Guilherme de Abreu Barreto         | 12543033 |
| Luiz Raphael Capelletto Lemos Reis | 11834133 |

## Diagrama ER+

![](Imagens/3373efe9e600e14f8af3043658c5ba84f58d2408.png)

> Sendo:
> 
> - "DPS" a sigla que indica o potencial destrutivo de um dado armamento.
> 
> - "Período" um atributo composto a partir da data de inicio e término de uma dada intervenção no conflito por parte de uma força armada ou mediador.
> 
> - As relações "Media" e "Participa" possuem atributos multivalorados pois assumem que mediadores ou forças armadas podem participar em um mesmo conflito em mais de um período, com atuações distintas em cada caso.