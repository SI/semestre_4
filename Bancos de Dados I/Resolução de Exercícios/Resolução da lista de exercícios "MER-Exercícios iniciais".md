# Resolução da lista de exercícios "MER-Exercícios iniciais"

> Feita por Guilherme de Abreu Barreto, nUSP 12543033

## 1.

### 1.1

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-18-42-42-image.png)

### 1.2

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-18-45-30-image.png)

### 1.3

![](Imagens/2022-09-11-18-50-54-image.png)

### 1.4

![](/home/user/Documents/Drives/USP/Introdução%20a%20Ciência%20da%20Computação/Imagens/2022-09-11-18-43-53-image.png)

## 2.

![](Imagens/2022-09-11-18-52-44-image.png)

## 3.

![](Imagens/2022-09-11-18-53-59-image.png)

## 4.

![](Imagens/2022-09-11-18-58-29-image.png)

## 5.

![](Imagens/2022-09-11-18-59-35-image.png)