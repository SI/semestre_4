# Resposta à lista de Banco de Dados

## ER

### 1.

#### Entidades

Designa a qualquer objeto imaginável, o qual possa ter valores (atributos) associados a este.

##### Exemplos

- Funcionários podem constituir uma entidade ao serem valorados com atributos tais quais: código de identificação, nome próprio, salário, departamento no qual trabalha, carga horária, etc.

- Alunos também, ao serem associados a atributos tais quais: curso, semestre ideal, disciplinas, notas, etc.

#### Relacionamento

Qualquer forma tangível de associação ou agregação de entidades.

##### Exemplos

- Funcionários podem estar relacionados à gestores em uma cadeia de comando.

- Porfessores podem estar relacionados a dado departamento em uma Universidade, o qual, por vez, encontra-se reciprocamente associado à um professor titular.

#### Atributo

Qualquer forma de valoração de uma entidade. Atributos podem ser monovalorados ou multivalorados, simples (descrito por um valor ou valores em um único domínio) ou compostos (descrito por valores em múltiplos domínios).

#### Generalização/especialização

Formas de associação de múltiplas entidades que satisfazem um mesmo tipo, respectivamente, quando estas não são e não são mutuamente exclusivas.

##### Exemplo

Tomando como exemplo conflitos armados tais como descritos no EP 1, temos,

###### Generalizações

<img src="Imagens/2022-10-14-17-52-50-image.png" title="" alt="" data-align="center">

Conflitos armados podem ter diversas facetas associadas a ele, como interesses territoriais, religiosos, econômicos, étnicos ou quaisquer combinações destes.

###### Especializações

<img src="Imagens/2022-10-14-17-58-11-image.png" title="" alt="" data-align="center">

Armamentos podem estar em prontidão para uso por uma força armada ou armazenados em estoque de um mercador de armas. Não obstante em ambas as situações tratem-se de armas, uma arma em uso não pode estar em estoque, e vice-versa: são especializações de um mesmo tipo, mutuamente exclusivas.

### 2.

Um tipo de entidade trata-se de um arquétipo sobre o qual instâncias de entidade, que são as ocorrências particulares do tipo de entidade, se baseiam.

### 3.

Ao ser acrecido um empregado, já deve haver no banco de dados pelo menos uma ocorrência de uma mesa vaga, sendo que o empregado só pode ser alocado à no máximo uma mesa. Por outro lado, ao se acrescentar uma mesa ao banco de dados, não a necessidade de se haver empregados disponíveis, não obstante, ainda só é possível associar esta mesa a um único empregado no futuro.

### 4.

![](Imagens/2022-10-14-21-55-00-image.png)

> Onde o atributo multivalorado "Notas" é o relatório de notas.

### 5.

![](Imagens/2022-10-18-19-18-40-image.png)

### 6.

<img src="Imagens/2022-10-18-20-04-16-image.png" title="" alt="" data-align="center">

A relação "Possui" pode ser redundante, na eventualidade de um departamento possuir até 3 funcionários e os telefones de contato do departamento forem exatamente aqueles associados aos funcionários.

### 7.

![](Imagens/2022-10-18-20-20-53-image.png)

Optei por renomear a relação "usa", enquanto "dispõe" pois o uso efetivo dos livros-texto é contingente a adoção destes por parte dos professores que lecionam as respectivas disciplinas.

### 8.

**a.** Verdade. Vide relação total entre ator e "atua_em".

**b.** Talvez. O modelo comporta que os atores podem atuar em qualquer número de filmes maior ou igual a 1, então é possível que tais atores existam neste banco de dados, mas nada garante que isso ocorra.

**c.** Talvez. Não existem garantias para tal.

**d.** Falso. Segundo o modelo, um filme pode ter qualquer número de protagonistas maior ou igual a 1. (creio que isto seja um erro na representação)

**e.** Talvez. Não existem garantias para tal.

**f.** Talvez. Mas se for este o caso, teríamos uma relação inutilizada na representação do dataset.

**g.** Falso. Um produtor pode ser um ator e um ator pode atuar em qualquer número de filmes segundo o modelo.

**h.** Talvez. Um filme pode ter qualquer número de atores maior ou igual à 1.

**i.** Talvez. É possível um ator ser também produtor, ou diretor, ou ambos, mesmo em um único filme.

**j.** Talvez. Todos os filmes tem pelo menos um diretor e produtor, mas não existem restrições quanto a razão entre os números destes.

**k.** Talvez.

**l.** Talvez.

**m.** Talvez.

### 9.

![](Imagens/2022-10-19-14-33-44-image.png)

> Onde índice e taxa são atributos complementares à especialidade descrita para cada consulta.

## Álgebra relacional

### 1.

- **Seleção ($\bm \sigma$):** Obtêm-se à partir de uma relação outra contendo apenas as tuplas aquelas cujos atributos correspondem a um dado parâmetro.

- **Projeção ($\bm \pi$):** Obtêm-se a partir de uma relação outra contendo apenas os atributos listados enquanto parâmetros.

- **Renomeação ($\bm \rho$):** Renomeia-se atributos em uma dada relação.

- **Operações de conjunto ($\bm{\cup, \cap, \Delta, -} $):** derivadas à partir da teoria de conjuntos, estas funcionam tal qual estamos familiarizados.

- **Produto Cartesiano ($\bm \times$):** A relação $T$ que é a  conjunção de todas as tuplas em uma relação $R$ cada qual com uma tupla numa relação $S$.

- **Junção ($\bm \Phi$):** A realização, entre duas relações $R$ e $S$ de um produto cartesiano produzindo uma relação $T$ seguido de uma operação de seleção.

- **Divisão ($\bm \div$):** Seja $R$ uma relação de grau $m + n$ e $S$ uma relação de grau $n$, a operação de divisão $R \div S$ retorna uma relação $T$ de tamanho $m$ tal que contém os atributos em $R$ que possuem correspondência a todo o domínio de um ou mais atributos em $S$.

### 2.

Quando duas relações quaisquer $R$ e $S$ possuem o mesmo *grau* (número de atributos) e seus atributos, listados ordenados, possuem paridade em termos de seus respectivos *domínios*. As operações listadas necessitam haver essa exigência pois as tuplas das diferentes relações necessitam ser comparáveis entre si para, como resultado da operação, aparecer ou não na relação resultante.

### 3.

A renomeação é muitas vezes necessária como parte do procedimento de junção ($\Phi$), tanto que vemos esta comumente empregada enquanto junção natural ($*$). Isto pois atributos em diferentes tabelas, embora possam ter nomes distintos, podem possuir valores iguais e esta correspondência de igualdade pode ser bastante útil se pareada entre atributos correspondentes para a consulta. Na divisão ($\div$), a renomeação também pode ser importante pois esta requer que ao menos um atributo em uma relação $A$ de grau $m + n$ conste noutra relação $B$ de grau $n$.

### 4.

**Junção Theta ($\bm \Theta$):** Realiza-se o produto cartesiano entre duas relações e seleciona-se as tuplas de acordo com um parâmetro comparativo entre dois atributos, cada qual em uma relação, de mesmo domínio.

**Equijunção:** Realiza-se o produto cartesiano e seleciona-se as tuplas  de acordo com um parâmetro de igualdade entre dois atributos, cada qual em uma relação, de mesmo domínio.

**Junção Natural ($\bm *$):** Realiza-se o produto cartesiano e seleciona-se as tuplas aquelas que possuem, em que todos os atributos comuns a ambas as relações, um correspondente de mesmo valor.

### 5.

Uma chave estrangeira é um parâmetro propício a realizar junções sobre duas relações de forma a obter total correspondência entre estas, especialmente nas operações de equijunção ou junção natural.