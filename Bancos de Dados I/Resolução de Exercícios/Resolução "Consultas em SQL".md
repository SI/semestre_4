# Resolução "Consultas em SQL"

1. Selecione os nomes dos empregados que têm um salário > 1200 e foram contratados em 1981.

```plsql
select ename
from emp
where sal > 1200 and extract(year from hiredate) = 1981
```

2. Selecione os nomes dos empregados que trabalham no departamento de ‘ACCOUNTING’.

```plsql
select ename
from emp natural join dept
where dname = 'ACCOUNTING'
```

3. Selecione os nomes dos empregados que têm um salário entre 1000 e 3000 e trabalham no departamento de ‘RESEARCH’.

```plsql
select ename
from emp natural join dept
where (sal between 1000 and 3000) and dname = 'RESEARCH'
```

4. Selecione os nomes dos empregados que têm um salário maior que o salário do ‘SMITH’.

```plsql
select ename
from emp
where sal > some (select sal
                  from emp
                  where ename = 'SMITH')
```

5. Selecione os nomes dos empregados que ganham mais que todos os empregados que trabalham no departamento, cujo Deptno é 30.

```plsql
select ename
from emp
where sal > all (select sal
                 from emp 
                 where deptno = 30)
```

Solução mais eficiente:

```plsql
select ename
from emp
where sal > all (select max(sal)
                 from emp 
                 where deptno = 30)
```

6. Selecione os nomes, salários e faixa salarial de todos os empregados.

```plsql
select ename, sal, grade
from emp, salgrade
where sal between losal and hisal
order by sal
```

7. Selecione os nomes dos empregados que ganham menos que algum empregado do departamento de ‘SALES’.

```plsql
select ename
from emp
where sal < (select min(sal)
            from emp natural join dept
            where dname = 'SALES')
```

8. Selecione os nomes dos empregados que têm um salário > 1200 e foram contratados em 1981, ordenados alfabeticamente.

```plsql
select ename
from emp
where sal > 1200 and extract(year from hiredate) = 1981
order by ename
```

9. Selecione o departamento (número e nome) e a soma total dos salários pagos aos seus empregados.

```plsql
select deptno, dname, sum(sal) as montante_dos_salários
from emp NATURAL JOIN dept
group by deptno, dname;
```

> **Atenção:** pelo fato dos atributos `deptno` e `dname` não estarem sendo utilizadas em uma fução de agregação como ocorre com `sal`, estas necessitam ser agrupadas pela cláusula `group by`.

10. Selecione o departamento (número e nome) e o seu número total de empregados.

```plsql
select deptno, dname, count(ename) as funcionários
from emp natural join dept
group by deptno, dname
```

11. Selecione o departamento (numero e nome) que pague a maior quantidade em salários.

```plsql
select deptno, dname, sum(sal) as montante_dos_salários
from emp NATURAL JOIN dept
group by deptno, dname
order by montante_dos_salários desc
limit 1
```

12. Selecione o departamento com o menor numero de empregados.

```plsql
select deptno, dname, count(ename) as funcionários
from emp natural join dept
group by deptno, dname
order by funcionários
limit 1
```

13. mude o empregado ‘SMITH’ para ele trabalhar no departamento de ‘ACCOUNTING’

```plsql
update emp
set deptno = (select deptno
             from dept
             where dname = 'ACCOUNTING')
where ename = 'SMITH'
```

14. remova os empregados que trabalham no departamento de ‘RESEARCH’

```plsql
update emp
set deptno = null
where deptno = (select deptno
             from dept
             where dname = 'RESEARCH')
```

15. decremente o salário dos empregados que ganham mais de 3000, em 10%

```sql
update emp
set sal = sal - sal / 10
where sal > 3000 and job != 'PRESIDENT'
```

16. Quais são os Nomes dos fornecedores que fornecem a Peça 'P2'?

```plsql
select fnome
from F natural join FP
where P# = 'P2'
```

17. Quais são os Nomes dos fornecedores que fornecem pelo menos uma peça de cor vermelha?

```plsql
select fnome
from (F natural join FP) natural join P
where COR = 'VERMELHA'
```

18. Quais são os códigos dos fornecedores que fornecem pelo menos todas as peças fornecidas pelo fornecedor ‘F2’?

```plsql
select distinct F#
from FP
where P# in (select P#
             from FP
             where F# = 'F2')
```

19. Quais são os nomes dos fornecedores que não fornecem a peça ‘P2’?

```plsql
select fnome
from F
where F# not in (select distinct F#
                 from FP
                 where P# = 'P2')
```

## Exercícios do livro

1. Recupere o nome de todos os funcionários do departamento 5 que trabalharam mais de 10 horas por semana no Projeto X

```sql
select Pnome from ((funcionario
                   join trabalha_em on Cpf = Fcpf)
                   join projeto on Pnr = Projnumero))
where (Projnome = 'ProdutoX'
       and Horas > 10
       and Dnome = (select Dnome
                    from Departamento
                   where Dnumero = 5));
```

2. Liste o nome de todos os funcionários que possuem um dependente com o mesmo primeiro nome que o seu próprio.

```sql
select Pnome from (funcionario join dependente on Cpf = Fcpf)
where Pnome = Nome_dependente
```

3. Ache o nome de todos os funcionários supervisionados diretamente por "Fernando Wong"

```sql
select Pnome from funcionario
where Cpf_supervisor = (select Cpf from funcionario
                       where Pnome = 'Fernando' and Unome = 'Wong')
```

4. Inserir um novo aluno no banco de dados

```sql
insert into aluno(Nome, Numero_aluno, Tipo_aluno, Curso) values ('Alves', 25, 1, 'MAT')
```

5. Atualizar o tipo de um aluno

```sql
update aluno set tipo_aluno = 2 where nome = 'Silva'
```

6. Excluir um registro de aluno

```sql
delete from aluno where nome = 'Silva' and Numero_aluno = 17
```

**4.15**

**a.** Todas as tuplas da tabela de funcionários em que se encontra um funcionário cujo último nome é brito são deletadas. No mais, quaisquer referências a essa tupla em demais tabelas com chave estrangeira para esta tabela também são apagadas.

**b.** Cascade, ou senão as referência a esse dado funcionário permaneceriam no dataset embora fossem inócuas.

**5.1.** Descreva as seis clausulas na sintaxe de uma consulta de recuperação SQL.

SELECT [DISTINCT] <columns> FROM <tables> [ WHERE <conditions>] [GROUP BY <atributes> [HAVING <conditions>] [ORDER BY <column name>  [, <order>]]

Group by permite agrupar tuplas em função de um dado atributo. Por si só, esta funciona tal qual uma chamada a "distinct", mas esta permite também o uso de funções de agregação como `sum()` ou `count()`. Por exemplo, o seguinte comando:

```sql
SELECT
    customer_id,
    SUM (amount)
FROM
    payment
GROUP BY
    customer_id;
```

Agrupa por identificação do consumidor, realizando a somatória de todos os valores pagos por este. E devolve uma tabela tal qual a seguinte:

<img src="Imagens/2022-12-14-21-28-18-image.png" title="" alt="" data-align="center">

**5.4.**

**a.** Consultas aninhadas

Permitem fazer consultas as quais não seriam possíveis doutra forma, por exemplo, a operação de divisão do modelo relacional pode ser executada com consultas aninhadas definindo cada qual o numerador e o denominador da operação.

**b.** Tabelas de junção e junções externas

Permitem juntar o conteúdo de duas tabelas em uma única. Junções externas permitem mesmo que tuplas as quais não seriam inclusas no resultado por possuírem valores nulos, seja na tabela à esquerda ou à direita (da operação) o sejam.

**c.** Funções de agregação e agrupamento

Agrupar permite juntar tuplas que possuem um mesmo atributo em comum, seja para eliminar repetições (sem fazer uso de funções de agrupamento), agregar valores (função `sum`), ou contabilizá-las (função `count`).

**d.** Triggers

PErmite a realização automatizada de consultas SQL quando um conjunto de condições pré-estabelecidas são satisfeitas.

**e.** Asserções e como elas diferem dos triggers.

Asserções referem-se a testes a serem realizados para avaliar se uma inserção ou atualização de uma tabela se dá dentro de parâmetros válidos e, senão, as impossibilita. Triggers também podem ser utilizados para esta finalidade, mas estes podem também realizar quaisquer outras funcionalidades de consultas SQL quando um dado tipo de interação qualquer com o banco de dados ocorre. Nalguns casos, pode se tratar de apenas emitir uma mensagem de alerta.

**f.** Views e suas formas de atualização.

Uma *view* em terminologia SQL é uma única tabela que é derivada a partir doutras tabelas: uma *tabela virtual* derivada a partir das *tabelas base*. Quando os dados da das tabelas de base são modificados, estes também o são na view. No mais, consultas a view funcionam da mesma maneira como em qualquer outra tabela. É possível se alterar o conteúdo de tais tabelas a partir de uma view gerada a partir delas, mas isso apenas quando todos os atributos *unique* e de chave primária estão nela representados.

**g.** Comandos de alteração de esquema

Comandos `Alter` os quais permitem alterar a tabelas, atributos, restrições, entre outros elementos.

**5.5.** Especifique as seguintes consultas no banco de dados da Figura 3.5 em SQL. Mostre os resultados da consulta se cada uma for aplicada ao banco de dados da Figura 3.6.

**a.** Para cada departamento cujo salário médio do funcionário seja maior do que R$30.000,00, recupere o nome do departamento e o número de funcionários que trabalham nele.

```sql
select Dname, count as employees
from (select Dname, avg(salary), count(Fname)
      from department join employee on dnumber = dno
      group by Dname) as result
```

**b.** Suponha que queiramos o número de funcionários do sexo masculino em cada departamento que ganhe mais de R$30.000,00, em vez de todos os funcionários (como no Exercício 5.5a). Podemos especificar essa consulta
em SQL? Por quê?

```sql
select Dname, count as employees
from (select Dname, avg(salary), count(Fname)
      from department join employee on dnumber = dno
      where sex = 'M'
      group by Dname) as result
```

**5.6.**

Especifique as seguintes consultas em SQL sobre o esquema de banco de dados da Figura 1.2.

**a.** Recupere os nomes e departamentos de todos os alunos com notas A (alunos que têm uma nota A em todas as disciplinas).

```sql
select Nome, Curso
from aluno
where Numero_aluno not in (select Numero_aluno
                           from Historico escolar
                           where Nota != 'A')
```

**b.** Recupere os nomes e departamentos de todos os alunos que não têm uma nota A em qualquer uma das disciplinas.

```sql
select Nome, Curso
from aluno
where Numero_aluno not in (select Numero_aluno
                           from Historico escolar
                           where Nota = 'A')
```

**5.7.**

Em SQL, especifique as seguintes consultas sobre o banco de dados da Figura 3.5 usando o conceito de consultas aninhadas e conceitos descritos neste capítulo.

**a.** Recupere os nomes de todos os funcionários que trabalham no departamento que tem o funcionário com o maior salário entre todos os funcionários.

```sql
select Fname from employee
where dno = (select dno from employee
             where salary = (select max(salary) from employee))
```

**b.** Recupere os nomes de todos os funcionários cujo supervisor do supervisor tenha como Cpf o número ‘888665555’.

```sql
select Fname from employee
where super_ssn in (select ssn from employee
                     where super_ssn = '888665555')
```

**c.** Recupere os nomes dos funcionários que ganham pelo menos R$10.000,00 a mais que o funcionário que recebe menos na empresa

```sql
select Fname from employee
where salary > (select min(salary) from employee ) + 10000
```