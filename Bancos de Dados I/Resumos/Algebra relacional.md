# Algebra relacional

## Operações Unárias

### Seleção ($\bm \sigma$)

Recupera-se uma relação a qual satisfaz uma dada condição de seleção. Denota-se:

$$
R_r = \sigma_c(R)
$$

Sendo:

- $\sigma$ o operador de seleção;

- $c$ a condição de seleção;

- $R$ a relação sobre a qual a operação é aplicada;

- $R_r$ a relação resultante da seleção.

#### Exemplo

Seja $R$:

| Empregados | Nome   | SNome | DNo |
| ---------- | ------ | ----- | --- |
|            | Tom    | Ford  | 3   |
|            | Amy    | Jones | 2   |
|            | Alicia | Smith | 2   |

Ao aplicar-se a operação $\sigma_{DNo = 2}(R)$ obtém-se:

| Empregados | Nome   | SNome | DNo |
| ---------- | ------ | ----- | --- |
|            | Amy    | Jones | 2   |
|            | Alicia | Smith | 2   |

O uso de operadores booleandos ($\land$, $\lor$, $\underline\lor$, $\neg$) podem tornar seleções mais ou menos específicas. A seguinte operação

$$
\sigma_{\text{DNo } =\ 2\ \land \text{ Salario } > \ 20000}(R)
$$

Seleciona funcionários tanto pelo departamento em que trabalham quanto por uma faixa salarial.

Noutras palavras, a operação de seleção consiste no *particionamento horizontal* de uma relação em dois conjuntos de tuplas: aquele que satisfaz a condição e aquele que não a satisfaz.

### Projeção ($\bm \pi$)

Recupera-se uma relação que contém uma dada seleção, não de tuplas, mas de colunas. Denota-se:

$$
R_r = \pi_{l}(R)
$$

Sendo:

- $\pi$ o operador de projeção;

- $l$ uma lista de atributos qualquer;

- $R$ a relação sobre a qual a operação é aplicada;

- $R_r$ a relação resultante da projeção.

#### Exemplo

Seja $R$:

| Empregados | Nome | Snome | Salario | DNo |
| ---------- | ---- | ----- | ------- | --- |
|            | Tom  | Ford  | 30000   | 3   |
|            | Amy  | Jones | 30000   | 2   |

Ao aplicar-se a operação $\pi_{\text{(Nome, Snome, Salario)}}(R)$ obtém-se:

| Empregados | Nome | Snome | Salario |
| ---------- | ---- | ----- | ------- |
|            | Tom  | Ford  | 30000   |
|            | Amy  | Jones | 30000   |

Na eventualidade de duas tuplas tornarem-se iguais em função de uma operação de projeção (digamos, havendo dois Tom Fords cada qual trabalhando em um departamento diferente) as tuplas duplicadas são apagadas.

Noutras palavras, a operação de seleção consiste no *particionamento vertical* de uma relação em dois conjuntos de tuplas: aquele que satisfaz a condição e aquele que não a satisfaz.

### Renomeação ($\bm \rho$)

Renomeia-se os atributos, o que pode vir a ser útil ao se combinar relações, como será visto adiante, na eventualidade de atributos de significados distintos possuírem mesmo nome. Denota-se:

$$
R_r = \rho_{S(l)} (R)
$$

Sendo:

- $\rho$ o operador de renomeação;

- $S$ um novo nome à relação, caso houver;

- $l$ a lista dos atributos renomeados;

- $R$ a relação sobre a qual a operação é aplicada;

- $R_r$ a relação resultante da projeção.

## Operações de conjunto

As operações de conjunto são derivadas à partir da teoria de conjuntos ($\cup$, $\cap$, $\Delta$, $-$) e funcionam tal qual estamos familiarizados. Não obstante, necessita-se atentar para a aplicação destas operações sobre duas relações $R$ e $S$ produzindo uma terceira relação $T$ só é possível se $R$ e $S$ forem relações de mesmo tipo (há a compatibilidade de tipo). Isto é, $R$ e $S$ possuem o mesmo *grau* (número de atributos) e seus atributos, listados ordenados, possuem paridade em termos de seus respectivos *domínios*. O **Produto Cartesiano** ($\times$) não tem tal exigência. Neste, cada tupla de uma relação é combinada a cada tupla da outra relação. Por exemplo:

- Seja $R$:

| R   | A   | B   |
| --- | --- | --- |
|     | 101 | 104 |
|     | 102 | 105 |
|     | 103 | 106 |

- Seja $S$:

| S   | C   | D   | E   |
| --- | --- | --- | --- |
|     | p   | a   | x   |
|     | q   | b   | y   |

- Tem-se que $R \times S$ é:

| R ⨉ S | A   | B   | C   | D   | E   |
| ----- | --- | --- | --- | --- | --- |
|       | 101 | 104 | p   | q   | x   |
|       | 101 | 104 | q   | b   | y   |
|       | 102 | 105 | p   | q   | x   |
|       | 102 | 105 | q   | b   | y   |
|       | 103 | 106 | p   | q   | x   |
|       | 103 | 106 | q   | b   | y   |

Note:

- Seja $m$ o grau da relação $R$ e $n$ o grau da relação $S$, tem-se que $m + n$ é o grau da relação $R \times S$.

- Seja $x$ a cardinalidade da relação $R$ e $y$ a cardinalidade da relação $S$, tem-se que $m \cdot n$ é a cardinalidade de $R \times S$.

## Operações Binárias

### Junção ($\Phi$)

A realização de uma operação de produto cartesiano seguida de uma operação de seleção para, desta forma, combinar tuplas de duas relações de acordo com **um parâmetro comparativo** (>, >=, !=, =, <=, <). Denota-se:

$$
R_r = R\ \Phi_{c}\ S = \sigma_c(R \times S)
$$

#### Exemplo

Seja $R$:

| Departamento | NomeD      | DNo | SSN_Gestor |
| ------------ | ---------- | --- | ---------- |
|              | Pesquisa   | 2   | 553621425  |
|              | Financeiro | 5   | 996856974  |

E seja $S$:

| Empregado | SSN       | Nome  | SNome    | DNo |
| --------- | --------- | ----- | -------- | --- |
|           | 123658974 | Alex  | Smith    | 2   |
|           | 553621425 | Fred  | Scott    | 2   |
|           | 996856974 | Elsa  | David    | 5   |
|           | 793818996 | Peter | Williams | 5   |

Tem-se $ R\ \Phi_{\text{SSN\_Gestor = SSN}}\ S$:

| Gerentes | SSN       | Nome | SNome | DNo | NomeD      | SSN_Gestor |
| -------- | --------- | ---- | ----- | --- | ---------- | ---------- |
|          | 553621425 | Fred | Scott | 2   | Pesquisa   | 553621425  |
|          | 996856974 | Elsa | David | 5   | Financeiro | 996856974  |

Repare que a duplicação de DNo desaparece após a junção. Se renomearmos SSN_Gestor como SSN, essa segunda duplicação também desaparecerá.

### Divisão ($\bm \div$)

Seja $A$ uma relação de grau $m + n$ e $B$ uma relação de grau $n$, a operação de divisão $A \div B$ retorna, como resultado de uma série de operações menores, uma relação $C$ de tamanho $m$ tal que contém os atributos em $A$ que possuem correspondência a todo o domínio de um ou mais atributos em $B$.

#### Exemplo

Busca-se recuperar todas as identidades dos funcionários aqueles que trabalham em todos os projetos geridos por uma dada empresa. Seja $A$ a relação de funcionários e projetos em que estes participam:

| Funcionários | ID   | PID |
| ------------ | ---- | --- |
|              | 1001 | 1   |
|              | 1002 | 1   |
|              | 1002 | 2   |
|              | 1003 | 2   |

E $B$ a relação de projetos existentes:

| Projetos | PID |
| -------- | --- |
|          | 1   |
|          | 2   |

O resultado da operação de divisão será:

| Resultado | ID   |
| --------- | ---- |
|           | 1002 |

Pois o funcionário de identidade 1002 é o único que satisfaz tal condição. Agora vejamos essa operação realizada passo à passo (5 passos no total):

##### 1. $\bm{T_1 \leftarrow \pi_{ID}(A)}$

| T1  | ID   |
| --- | ---- |
|     | 1001 |
|     | 1002 |
|     | 1003 |

> Repare que a duplicação de 1002 foi removida.

##### 2. $\bm{T_2 \leftarrow \pi_{ID}(T_1 \times B)}$

| T2  | ID   | PID |
| --- | ---- | --- |
|     | 1001 | 1   |
|     | 1001 | 2   |
|     | 1002 | 1   |
|     | 1002 | 2   |
|     | 1003 | 1   |
|     | 1003 | 2   |

##### 3. $\bm{T_2 \leftarrow T_2 - A}$

| T2  | ID   | PID |
| --- | ---- | --- |
|     | 1001 | 2   |
|     | 1003 | 1   |

##### 4. $\bm{T_2 \leftarrow \pi_{ID}T_2}$

| T2  | ID   |
| --- | ---- |
|     | 1001 |
|     | 1003 |

##### 5. $\bm{C \leftarrow T_1 -  T_2}$

| C   | ID   |
| --- | ---- |
|     | 1002 |