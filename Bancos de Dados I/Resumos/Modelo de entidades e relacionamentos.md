# Modelo de entidades e relacionamentos (Enterprise Schema)

Criado por Peter Chen em 1976 para ilustrar graficamente a implementação de um banco de dados

## Conceitos básicos

### Tipo de entidade

Conjunto de entidades que possuem mesmos atributos.

| Notação gráfica                            | Exemplo                                    |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-09-06-20-13-39-image.png) | ![](Imagens/2022-09-06-20-13-56-image.png) |

Por exemplo, considere dois tipos de entidade: **empregado** e **empresa**. Compõem o **conjunto entidade** de cada um destes tipos de entidade os seguintes itens:

![](Imagens/2022-09-06-20-51-51-image.png)

### Tipo de Relacionamento

Associações ou agregações entre entidades.

| Notação gráfica                            | Exemplo                                                                                                                                         |
| ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| ![](Imagens/2022-09-06-20-19-00-image.png) | ![](file:///home/user/Public/USP/Sistemas de Informação/4° semestre/Bancos de Dados I/Imagens/2022-09-06-20-18-22-image.png?msec=1662506302838) |

Estas relações podem ser **parciais** quando não se espera que todas as instâncias de uma entidade tipo participem do relacionamento, do contrário estas são **totais**.

Por exemplo, considere os conjuntos entidades **empregado** e **departamento**, e a relação **trabalha para**. Tem-se:

<img src="Imagens/2022-09-06-20-58-10-image.png" title="" alt="" data-align="center">

Vê-se na amostra observada que a relação **empregado *e* trabalha para departamento *d*** é total.

### Atributos

Valores associados às entidades.

| Notação gráfica                            | Exemplo                                    |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-09-06-20-20-19-image.png) | ![](Imagens/2022-09-06-20-20-34-image.png) |

Estes podem ser:

- Compostos ou, senão, simples;
  
  - Exemplo de atributo composto:

![](Imagens/2022-09-06-20-40-23-image.png)

Atributos compostos organizados em uma hierarquia

- Monovalorados ou, senão, multivalorados;

- Armazenados ou, senão derivados;

- Nulos (exemplo: "não aplicável", "desconhecido", quando o valor existe mas não se sabe qual é);

- Complexos

## Restrições sobre os tipo de relacionamentos e atributos

### Cardinalidade

O número de relações associados a uma entidade. Possui enquanto parâmetros:

- $CARD\_MIN$: indica a cardinalidade mínima;

- $CARD\_MAX$: indica a cardinalidade máxima;

Considerando o atributo $A$ e a entidade $E$, temos as seguintes implicações:

- $CARD\_MIN(A,E) > \empty$: atributo obrigatório;

- $CARD\_MIN(A,E) = \empty$: atributo opcional;

- $CARD\_MAX(A,E) = 1$: atributo monovalorado;

- $CARD\_MAX(A,E) > 1$: atributo multivalorado;

Enquanto, considerando a relação $R$ e a entidade $E$:

- $CARD\_MAX(R,E) = N, N > 1$: "Muitas";

- $CARD\_MAX(R,E) = 1$: "Uma".

Possíveis configurações:

| Atributo opcional                          | Atributo obrigatório                       |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-09-07-12-42-56-image.png) | ![](Imagens/2022-09-07-12-43-10-image.png) |

| Relacionamento                             |
| ------------------------------------------ |
| ![](Imagens/2022-09-07-12-46-15-image.png) |
| ![](Imagens/2022-09-07-12-46-27-image.png) |
| ![](Imagens/2022-09-07-12-46-42-image.png) |
| ![](Imagens/2022-09-07-12-56-59-image.png) |
| ![](Imagens/2022-09-07-12-57-22-image.png) |
| ![](Imagens/2022-09-07-12-57-50-image.png) |

## Identificador

Denominação dada ao conjunto de atributos $I$ relacionados à $E$ de tal forma que permite identificar, de maneira única, todas as instâncias de $E$. São propriedades dos identificadores:

1. Todo atributo $A_i$ de um identificador $I$ de um tipo de entidade $E$ **deve** ser monovalorado e obrigatório ($CARD(A_i, E) = (1,1)$).

2. Não existem duas entidades diferentes $e_i$ e $e_j$ tais que $I(e_i) = I(e_j)$.

3. Se algum atributo $A_i$ deixa de fazer parte de $I$, então a propriedade 2 não se verifica mais.

> Exemplo: seja uma dada mercadoria (item) identificada pela sua associação à duas entidades:
> 
> 1. o material que a compõe
> 
> 2. a nota fiscal que a acompanha
> 
> ![](Imagens/2022-09-08-10-41-32-image.png)
> 
> Seja qualquer uma destas entidades desvinculadas da mercadoria, esta não poderia mais ser identificada pois:
> 
> 1. Se o material não for identificado, não é possível diferenciar a mercadoria doutras doutros tipos;
> 
> 2. Se a nota fiscal não for especificada, não é possível diferenciar a mercadoria doutras do mesmo tipo.

> Obs.: Se um tipo de entidade possuir mais de um identificador um destes haverá de ser escolhido como sendo seu **identificador primário**. Exemplo:
> 
> <img src="Imagens/2022-09-07-13-11-57-image.png" title="" alt="" data-align="center">

### Entidades fortes e fracas

Existem tipos de entidades para as quais não é possível definir um identificador com base únicamente nos seus atributos, mas é possível fazê-lo considerando as relações destes com outras entidades na identificação. Ou seja, existe uma relação de **dependência** destes para com outras entidades para que haja sua identificação. Sendo assim, do ponto de vista de identificação, podemos classificar entidades em duas classes:

- **Entidades fortes:** aquelas que podem ser identificados unicamente com base nos seus próprios atributos.

- **Entidades fracas:** aquelas que, além dos próprios atributos, precisam de um **identificador externo**, atrelado a outra entidade, para tal. Esta possui uma notação que a especifica:

<img src="Imagens/2022-09-08-10-52-02-image.png" title="" alt="" data-align="center">

No mais, nos casos em que a relação identifica entidades pode-se adotar uma notação específica para esta, como a seguinte:

<img src="Imagens/2022-09-08-11-51-43-image.png" title="" alt="" data-align="center">

#### Exemplos de identificação externa

| Parcial                                    | Total                                      |
| ------------------------------------------ | ------------------------------------------ |
| ![](Imagens/2022-09-07-14-18-51-image.png) | ![](Imagens/2022-09-07-14-19-17-image.png) |

A relação empregado-dependente é do tipo **parcial** pois a relação em si não é suficiente para identificar o dependente, já que podem haver mais de um deste (relação 1:N). Por outro lado, a relação empregado-cônjuge é **total** pois a relação (no caso, 1:1) apenas é capaz de identificar a entidade cônjuge, já que cada empregado pode possuir apenas um cônjuge.

## Relacionamentos recursivos

Nalgumas situações uma relação recursiva pode ser estabelecida como, por exemplo, em uma cadeia de comando:

<img src="Imagens/2022-09-08-10-45-16-image.png" title="" alt="" data-align="center">

> Ilustração da relação em *Enterprise Schema*.

![](Imagens/2022-09-08-10-48-20-image.png)

> Ilustração da relação em um diagrama de conjuntos

Neste caso a relação se dá em sentido vertical, de cima para baixo, onde uma sequência de supervisores se estabelece até que seja encontrado o caso base: o empregado aquele que supervisiona à ninguém.

## Exemplos de relacionamento n-ários

| Tipo                                          | Representação                              |
| --------------------------------------------- | ------------------------------------------ |
| Relacionamento 1-para-1                       | ![](Imagens/2022-09-08-12-07-21-image.png) |
| Relacionamento 1-para-1-para-vários           | ![](Imagens/2022-09-08-12-08-29-image.png) |
| Relacionamento vários-para-vários-para-vários | ![](Imagens/2022-09-08-12-09-54-image.png) |

## Hierarquias de generalização

Quando um tipo de entidade $E$ é uma generalização de um conjunto de entidades $E_1, E_2, \dots, E_n$ de mesmo tipo pode-se representar esta relação da seguinte maneira:

<img src="Imagens/2022-09-08-12-40-07-image.png" title="" alt="" data-align="center">

Por exemplo:

<img src="Imagens/2022-09-08-13-01-47-image.png" title="" alt="" data-align="center">

## Subconjuntos

Uma dada entidade $E_2$ constitui um subconjunto de outro tipo de entidade $E_1$ se todo $E_2$ também for $E_1$. Por exemplo:

<img src="Imagens/2022-09-08-13-02-38-image.png" title="" alt="" data-align="center">

Neste caso, "secretária", "engenheiro" e "técnico" são todos subconjuntos de "empregado".

## Herança múltipla

Quando uma dada entidade $E$ constitui um subconjunto de duas, ou mais, outras entidades. Por exemplo:

<img src="Imagens/2022-09-08-13-05-26-image.png" title="" alt="" data-align="center">

Neste caso o Gerente de Engenharia é subconjunto das entidades "engenheiro", "gerente" e "assalariado".