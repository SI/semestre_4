# Introdução à Bancos de Dados

## Dados e informação

**Dados:** Quaisquer conjunto de descrições;

**Informação:** uma aplicação (interpretação ou uso) dos dados para uma finalidade qualquer;

**Conhecimento:** capacidade para resolução de problemas adquirida pelo uso reflexivo da informação de experiências prévias.

## Sistema

Arranjo de componentes que operam em conjunto para uma dada finalidade ou conjunto de finalidades. Um sistema pode ser composto por outros sistemas que, com relação a este, constituem sub-sistemas.

## Sistema de Banco de Dados

Sistema de informação computacional cujo objetivo principal é registrar e manter dados. Possui quatro componentes:

- *Software* (o Sistema Gerenciador de Banco de Dados ‒SGBD)

- Usuários

- Dados

- *Hardware*

## Administração de Banco de Dados

Prática do controle sobre um Banco de Dados, o que se dá usualmente em termos de

- Mudanças na organização física ou lógica destes;

- Controle de acesso por parte dos usuários;

- Estratégias de armazenamento de "Backup", ou redundância.

## Sistema Gerenciado de Banco de Dados (SGBD)

Sistema que conhece a estrutura com que dados são armazenados, fornecendo estes à terceiros (usuários ou outro programas) conforme a necessidade destes e em formato mais conveniente para estes, de tal forma que

- detalhes da implementação do banco de dados não necessitam ser conhecidos pelos terceiros para que estes possam fazer uso do mesmo.

- Mudanças estruturais podem ser feitas pelo SGBD sem que isto modifique a forma de requisição e obtenção de informações por parte dos terceiros.

## Vantagens do uso de Bancos de Dados

- Acesso e controle unificado de arquivos diversos para diversas aplicações;

- Menor redundância em comparação ao uso de arquivos dispersos;

- Controle de acesso, versão e padrões dos arquivos armazenados.

## Desvantagens

- O controle e organização de um banco de dados, feito de maneira centralizada introduz uma vulnerabilidade no sistema: um único ponto de falha.

- Para acesso de um único usuário ou poucos usuários, pode ser uma complicação desnecessária ao acesso e manejamento dos arquivos.

## Conceitos relacionados

### Instâncias e Esquemas

**Esquema:** visão global do banco de dados.

<img src="Imagens/2022-08-26-16-18-53-image.png" title="" alt="" data-align="center">

> Exemplo de esquema para um banco de dados de uma Universidade.

**Instâncias:** a coleção de dados armazenados no BD (valor de variáveis ≈ instâncias de esquema).

### Abstração em Banco de Dados

Denominação dada a característica que determinados bancos de dados apresentam em que os usuários destes não necessitam tratar os dados tal qual estes foram armazenados (ou seja, não necessitam manipular ponteiros, tabelas ou índices).

### Metadados

Dados que descrevem outros dados armazenados no Banco de Dados (como a data de inserção, modificação, tamanho, etc., destes).

### Linguagem de Definição de Dados (LDD)

Conjunto de definições expressas em uma linguagem, a compilação desta dá origem aos metadados.

### Linguagem de Manipulação de Dados (LMD)

Permite aos usuários acessar e manipular dados duma forma pré-definida. Pode ser de dois tipos: procedimental ou não procedimental.

Uma LMD, ou uma parte desta, destinada apenas à consulta de informação constitui uma Linguagem de Consulta.

## Modelo de dados

Conceito utilizado para descrever a organização lógica dos dados, em termos de LDD e LMD, correspondente a uma determinada realidade (universo discurso). Formalmente, um modelo de dados $M$ pode ser definido pelo par $M = (G,O)$, sendo:

- $G$ o LDD;

- $O$ o LMD;

Por extensão, um esquema constitui uma representação com base em um dado modelo de dados.