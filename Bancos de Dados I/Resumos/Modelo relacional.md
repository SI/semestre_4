# Modelo relacional

Modelo para a construção de bancos de dados com base em um conjunto de tabelas. 

![](Imagens/2022-09-30-16-49-08-image.png)

## Terminologia

Neste modelo, se denomina-se

- cada tabela uma **relação**;

- cada linha de uma relação, senão aquela do cabeçalho, uma **tupla**;

- cada cabeçalho um **atributo**;

- o conjunto de valores possíveis para um dado atributo é o **domínio** deste.
  
  > Assim sendo, a definição do domínio é pertinente a definir o tipo de dado a ser armazenado para cada atributo (números inteiros, números de ponto flutuante, strings, etc.)

- uma descrição de uma relação como sendo um **esquema de relação** (*relation schema*). Por exemplo, para a relação ilustrada acima, tem-se o seguinte esquema:

```sql
ALUNO(Nome, SSN, FoneResidencia, Endereco, FoneEscritorio, Idade, MPG)
```

> De maneira mais generalizada, para uma dada relação $R$ contendo $n$ atributos $A$, tem-se: $R(A_1, \dots, A_n)$.

- o número de atributos em uma dada relação é o **grau** desta.

- o número de tuplas em uma dada relação é a **cardinalidade** desta.

- um conjunto de esquemas de relação mais um conjunto de restrições de integridade (IC ‒ *integrity constraints*) é um **esquema relacional do banco de dados**.

- o estado da relação em um determinado momento é uma **instância da relação**.

## Características das Relações

### Ordenação de tuplas em uma relação

É indiferente, para propósito de representação, que as tuplas em uma relação figurem em qualquer ordem em particular. Não obstante, em relações estas frequente figuram ordenadas já que ao serem armazenadas desta forma, fica mais fácil recuperar um dado registro.

### Ordenação de valores em uma tupla

Denomina-se por **n-tupla** uma lista de valores ordenados. Diferentemente da ordenação de tuplas, a ordenação de valores em tuplas é significativo pois estes valores referem-se a atributos respectivos. Por exemplo, considere o esquema de relação apresentado anteriormente:

```sql
ALUNO(Nome, SSN, FoneResidencia, Endereco, FoneEscritorio, Idade, MPG)
```

Se trocarmos os valores de SSN e FoneResidencia de lugar, teremos uma diferente interpretação de "ALUNO".

Entretanto, com uma grafia alternativa onde o nome do atributo figura ao lado do valor atribuído a este, é possível ter descritos valores em qualquer ordem. Vide:

```sql
t = {(Nome, Roberto), (FoneResidencia, 11-5511-3035), ...}
```

### Valores em uma tupla

Cada valor em uma tupla é **atômico**, isto é, não pode ser repartido em suas partes componentes. Logo, neste modelo ná há valores cmpostos ou multivalorados. No mais, valores desconhecidos ou não aplicáveis são considerados **nulos**.

### Interpretação de uma relação

Um esquema de relação pode ser representada como uma declaração ou asserção factual.

## Restrições de modelos relacionais

Podem ser dos seguintes tipos:

- **Inerentes ou implícitos ao modelo:** Oriundos da lógica organizativa do modelo relacional. Por exemplo, o modelo relacional é baseado na teoria dos conjuntos logo:
  
  - tuplas podem figurar em qualquer ordem,
  
  - mas não podem haver tuplas duplicadas (isso é uma restrição inerente ao modelo e, portanto, não necessita ser explicitada).

- **Baseadas no esquema:** são definidas explicitamente pela definição do esquema. Tais quais:
  
  - Restrições de domínio. Por exemplo, a idade de um aluno só pode ser definida em um número inteiro positivo e não nulo que não pode ser maior que um nibble (de 1 à 255).
  
  - Restrições de identificadores. Havendo um atributo identificador ou conjunção de atributos identificadores, isto é, um(uns) atributo(s) capaz(es) de identificar sua tupla individualmente, este(s) não pode(m) se repetir.[^1]
    
    - Quaisquer atributos capazes de identificar uma tupla individualmente são designados *chaves candidatas*;
    
    - Não obstante, aquele que é efetivamente utilizado para tal fim é denominado a *chave primária*. Chaves primárias não podem assumir valores *nulos*.
  
  - Restrições de integridade referencial. Entre duas relações, onde uma tupla em uma referencia uma tupla noutra, a referência se dá à partir de um atributo designado *chave estrangeira* em uma relação que corresponde à *chave primária* da outra relação em termos de:
    
    - Possuir o mesmo domínio;
    
    - a primeira possuir o mesmo conjunto, ou um subconjunto, dos elementos da segunda (o elemento na chave estrangeira ou ocorre na chave primária ou é nulo).
  
  - Restrições de valores nulos. Certos atributos podem ser arbitrariamente designados como sendo não nulos.

- **Baseadas na aplicação:** aquelas expressas e realizadas pela aplicação que faz uso do banco de dados.

[^1]: Por extensão, o conjunto dos atributos de uma tupla constitui um identificador desta.