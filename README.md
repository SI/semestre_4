# Quarto semestre de Sistemas de Informação

> Documentação de trabalhos desenvolvidos

| Disciplinas | Nota |
| --- | --- |
| [Banco de Dados I](#banco-de-dados-I) | 7.0 |
| [Redes de Computadores](#redes-de-computadores) | 5.9 |
| [Métodos Quantitativos para Análise Multivariada](#métodos-quantitativos-para-análise-multivariada) | 9.6 |
| [Sistemas Operacionais](#sistemas-operacionais) | 9.2 |

## Banco de Dados I

### Programa

Conceitos e definições introdutórias: aplicações, vantagens, evolução histórica. Organização de um Sistema de Banco de Dados.
Modelos de representação. Modelagem de dados: Modelo Entidade-Relacionamento. Modelo Relacional: normalização e
álgebra relacional. Linguagem SQL: comandos de definição de dados e manipulação de dados. Uso de ferramentas CASE para
projeto lógico e implementação de bancos de dados.

### Conteúdo

#### Exercício de programação

Neste, modela-se e cria-se um banco de dados usando PostgreSQL, no caso, para a documentação de conflitos bélicos, assim como os agentes, territórios e armamentos nestes envolvidos:

- [Parte 1: Modelo ER+](Bancos de Dados I/EP - Conflitos Bélicos/Parte 1: Modelo ER+)

- [Parte 2: Modelo relacional](Bancos de Dados I/EP - Conflitos Bélicos/Parte 2: Modelo relacional)

- [Parte 3: Implementação do Banco de Dados](Bancos de Dados I/EP - Conflitos Bélicos/Parte 3: Banco de Dados/)

#### Resoluções de listas de exercícios

- [Lista "MER-Exercícios iniciais"](./Bancos de Dados I/Resolução de Exercícios/Resolução da lista de exercícios "MER-Exercícios iniciais".md)

- [Lista "Consultas em SQL"](Bancos de Dados I/Resolução de Exercícios/Resolução "Consultas em SQL".md)

- [Lista de Exercícios "Banco de Dados"](Bancos de Dados I/Resolução de Exercícios/Resposta à lista de Banco de Dados.md)

#### Resumos

- [Introdução à Banco de Dados](Bancos de Dados I/Resumos/Introdução à Bancos de Dados.md)

- [Modelo de entidades e relacionamentos](Bancos de Dados I/Resumos/Modelo de entidades e relacionamentos.md)

- [Modelo relacional](Bancos de Dados I/Resumos/Modelo relacional.md)

- [Algebra relacional](Bancos de Dados I/Resumos/Algebra relacional.pdf)

## Redes de Computadores

### Programa

Apresentar os conceitos básicos em redes de computadores. Exercitar o aluno em técnicas de projeto, instalação e configuração de redes locais.

### Conteúdo

#### Exercício de programação

[White Rabbit](Redes de Computadores/Ep-Redes): uma aplicação de linha de comando para o envio e recebimento de arquivos e pastas via TCP.

#### Resumos

[Vide](Redes de Computadores/Resumos)

## Métodos Quantitativos de Análise Multivariada

### Programa

Apresentar os fundamentos e métodos para análise, modelagem e interpretação de dados multivariados.

### Conteúdo

Nesta disciplina escolhemos um conjunto de dados e, a partir dele, buscamos realizar inferências com base em diferentes métodos estatísticos.

- [Análise preliminar dos dados](Métodos Quantitativos para Análise Multivariada/01 - Análise preliminar do conjunto de variáveis/Predição de preços de aluguéis pela análise estatística de características dos domicílios.pdf)

- [Análise Discriminante e Regressão Logística](Métodos Quantitativos para Análise Multivariada/04 - Análise Discriminante e Regressão Logística/Relatório Análise Discriminante Regressão Logística.md)

- [Análise Fatorial](Métodos Quantitativos para Análise Multivariada/05 - Análise Fatorial/Relatório Análise Fatorial.md)

- [Análise de Clusters](Métodos Quantitativos para Análise Multivariada/06 - Análise de Clusters/Análise de Clusters.md)

- [Escalonamento Multidimensional](./Métodos Quantitativos para Análise Multivariada/07 - Escalonamento multidimensional/Escalonamento Multidimensional.md)

## Sistemas Operacionais
